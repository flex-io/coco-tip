"""add table results

Revision ID: fe8d2dd4d65a
Revises: ccd1a6e8fc97
Create Date: 2021-01-13 13:17:59.926644

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'fe8d2dd4d65a'
down_revision = 'ccd1a6e8fc97'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
