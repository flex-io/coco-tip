"""Initial migration

Revision ID: a9e2f9239aec
Revises: b2988feeefea
Create Date: 2021-01-14 02:25:47.813670

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a9e2f9239aec'
down_revision = 'b2988feeefea'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
