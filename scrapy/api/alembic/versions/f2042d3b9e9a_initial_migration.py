"""Initial migration

Revision ID: f2042d3b9e9a
Revises: 535dd288481f
Create Date: 2021-01-14 03:32:30.377975

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f2042d3b9e9a'
down_revision = '535dd288481f'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
