"""Initial migration

Revision ID: 50e88def864d
Revises: a9e2f9239aec
Create Date: 2021-01-14 03:26:16.687372

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '50e88def864d'
down_revision = 'a9e2f9239aec'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
