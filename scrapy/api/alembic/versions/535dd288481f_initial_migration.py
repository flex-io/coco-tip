"""Initial migration

Revision ID: 535dd288481f
Revises: 50e88def864d
Create Date: 2021-01-14 03:26:18.815242

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '535dd288481f'
down_revision = '50e88def864d'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
