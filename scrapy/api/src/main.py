import os
import uvicorn
from fastapi import FastAPI, BackgroundTasks
from fastapi_sqlalchemy import DBSessionMiddleware
from src.routers import csgo, scheduled, playground
import settings
from src import Schedule
import requests

app = FastAPI()

app.add_middleware(DBSessionMiddleware, db_url=os.environ["DATABASE_URL"])
app.include_router(scheduled.router)
app.include_router(csgo.router)
app.include_router(playground.router)

def write_notification(email: str, message=""):
    print(email)


@app.post("/send-notification/{email}")
async def send_notification(email: str, background_tasks: BackgroundTasks):
    background_tasks.add_task(write_notification, email, message="some notification")
    return {"message": "Notification sent in the background"}

@app.get("/")
async def root():
    print(Schedule.get_jobs('default'))
    return {"message": "Hello World"}

@app.get("/test")
async def test():
    data = {
        "project": "csgo",
        "spider": "upcoming_matches",
        "url": "This is args",
        "setting": "FEED_URI="
    }
    
    res = requests.post('http://scrapy:6800/schedule.json', data=data)
    print(res)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)