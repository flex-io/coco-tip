from fastapi_sqlalchemy import db
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy import MetaData
from sqlalchemy.dialects import postgresql
from src import logger

def get_all(model):
    data = model.query.all()
    return data


def add_instance(model, **kwargs):
    instance = model(**kwargs)
    id = db.session.add(instance)
    commit_changes()
    return instance.id


def delete_instance(model, id):
    model.query.filter_by(id=id).delete()
    commit_changes()


def edit_instance(model, id, **kwargs):
    instance = model.query.filter_by(id=id).all()[0]
    for attr, new_value in kwargs.items():
        setattr(instance, attr, new_value)
    commit_changes()

def compile_query(query):
    compiler = query.compile if not hasattr(query, 'statement') else query.statement.compile
    return compiler(dialect=postgresql.dialect())
def upsert_instance(model, rows, no_update_cols=[]):
    
    # reflect db schema to MetaData
    metadata = MetaData()
    metadata.reflect(db.session.get_bind())

    table = metadata.tables[model.__tablename__] 

    stmt = insert(table).values(rows)

    update_cols = [c.name for c in table.c
                   if c not in list(table.primary_key.columns)
                   and c.name not in no_update_cols]
    
 
    on_conflict_stmt = stmt.on_conflict_do_update(
        index_elements=table.primary_key.columns,
        set_={k: getattr(stmt.excluded, k) for k in update_cols}
        )
        
    db.session.execute(on_conflict_stmt)
    commit_changes()

def truncate_table(model):
    try:
        db.session.execute(f'TRUNCATE TABLE {model.__tablename__} RESTART IDENTITY;')
        # db.session.query(model).delete()
        db.session.commit()
        logger.info(f'{model.__tablename__} has been truncated.')
    except Exception as err:
        logger.error(str(err))
        db.session.rollback()

def commit_changes():
    db.session.commit()