from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey, Float
from sqlalchemy.sql import expression
from sqlalchemy.orm import backref, relationship

Base = declarative_base()

class StringTable(Base):
    __tablename__ = "string_table"
    id = Column(Integer, primary_key=True, index=True)
    value_str = Column(String)
    out_str = Column(String)
    out_int = Column(Integer)
    out_date = Column(DateTime)
    out_bool = Column(Boolean)
    out_float = Column(Float)

class UpcomingMatches(Base):
    __tablename__ = "csgo_upcoming_matches"
    id = Column(Integer, primary_key=True, index=True)
    url = Column(String)
    match_date = Column(DateTime)
    team1_id = Column(Integer)
    team2_id = Column(Integer)
    team1_name = Column(String)
    team2_name = Column(String)
    match_event = Column(String)
    bo = Column(String)

class Match(Base):
    __tablename__ = 'csgo_match'
    id = Column(Integer, primary_key=True)
    team1_logo = Column(String)
    team2_logo = Column(String)
    team1_url = Column(String)
    team2_url = Column(String)
    team1_id = Column(Integer)
    team2_id = Column(Integer)
    team1_rank = Column(Integer)
    team2_rank = Column(Integer)
    match_date = Column(DateTime)
    event_url = Column(String)
    ban_pick = Column(String)
    status_id = Column(Integer)



class MapStat(Base):
    __tablename__ = 'csgo_match_map_stat'
    id = Column(Integer, primary_key=True, autoincrement=True)
    map_stat_id = Column(Integer)
    match_id = Column(Integer, ForeignKey('csgo_match.id', ondelete="CASCADE"))
    pick_by_team_id = Column(Integer)
    map_round = Column(Integer)
    mapstat_name = Column(String)
    team_id = Column(Integer)
    team_name = Column(String)
    round_history = Column(String)
    score = Column(Integer)
    url = Column(String)
    match = relationship(Match, backref=backref("csgo_match", cascade="all,delete"))

class PlayerStat(Base):
    __tablename__ = 'csgo_match_map_stat_player_stat'
    id = Column(Integer, primary_key=True, autoincrement=True)
    parent_id = Column(Integer, ForeignKey('csgo_match_map_stat.id', ondelete="CASCADE"))
    map_stat_id = Column(Integer)
    match_id = Column(Integer)
    player_id = Column(Integer)
    player_name = Column(String)
    player_url = Column(String)
    kills = Column(Integer)
    headshot = Column(Integer)
    assist = Column(Integer)
    flash_assist = Column(Integer)
    deaths = Column(Integer)
    kdratio = Column(Float)
    adr = Column(Float)
    fkdiff = Column(Integer)
    rating = Column(Float)
    match = relationship(MapStat, backref=backref("csgo_match_map_stat", cascade="all,delete"))

class Results(Base):
    __tablename__ = 'csgo_results'
    match_id = Column(Integer, primary_key=True)
    url = Column(String)
    match_date = Column(DateTime)
    is_process = Column(Boolean, server_default=expression.false(), nullable=False)
    status_id = Column(Integer, index=True)