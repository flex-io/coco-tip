from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.schedulers.asyncio import AsyncIOScheduler
import logging
import os, sys
# from apscheduler import schedulers
import settings

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# load_dotenv(os.path.join(BASE_DIR, ".env"))
sys.path.append(BASE_DIR)

logging.basicConfig(filename='example.log', level=logging.INFO)
logger = logging.getLogger(__name__)
jobstores = {
    'default': SQLAlchemyJobStore(url=os.environ["DATABASE_URL"])
}
Schedule = AsyncIOScheduler(jobstores=jobstores)