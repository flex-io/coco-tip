import os
import json
import time
from fastapi import APIRouter
from .. import Schedule, logger
from fastapi_sqlalchemy import db
from ..models import Results, UpcomingMatches
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse

import requests

router = APIRouter(
    prefix="/spider",
    tags=["spider"],
)
schedule_url = os.getenv("SCRAPY_URL")
AWS_API_URL = os.getenv("AWS_API_URL")

UNPOSTED_UPCOMING_MATCHES_URL = f"{AWS_API_URL}/csgo/for_processing"

@router.get('/fetch_results')
def fetch_results():
    data = {
        "project": "csgo",
        "spider": "results",
        #Disable creating file for results
        "setting": "FEED_URI="
    }
    logger.info("Executes spider_get_results")

    requests.post(schedule_url, data=data)

    time.sleep(5)
    fetch_map_stat()

    # time.sleep(10)
    # generate_json_map_stat

@router.get('/fetch_upcoming_matches')
def fetch_upcoming_matches():
    data = {
        "project": "csgo",
        "spider": "upcoming_matches",
        "url": "This is args",
        #Disable creating file for results
        "setting": "FEED_URI="
    }
    logger.info("Executes fetch_upcoming_matches")
    requests.post(schedule_url, data=data)

@router.post("/fetch_map_stat")
def fetch_map_stat():
    try:

        with db():
            res = db.session.query(Results).filter(Results.is_process != True).all()
            # db.session.query(User).all()
        urls = [record.url for record in res]

        if len(urls) > 0:
            data = {
                "project": "csgo",
                "spider": "match_url",
                "url": ",".join(urls),
                #Disable creating file for results
                "setting": "FEED_URI="
            }
            logger.info('executing: spider_match_url')
            requests.post(schedule_url, data=data)
            return 200
        else:
            logger.info('No new record in Results')
    except Exception as e:
        logger.info(str(e))

@router.post("/generate_json_map_stat")
def generate_json_map_stat():
    try:

        res = requests.get(UNPOSTED_UPCOMING_MATCHES_URL, headers={'Content-Type': "application/json", 'Accept': "application/json"})

        data = {}
        logger.info('Executing: generate_json_map_stat')
        with db():
            for record in res.json():
                record_id = record['id']
                
                game_detail = db.session.execute("SELECT json_csgo_game_detail(:match_id) as game_detail", {"match_id": record_id})
                
                for row in game_detail:
                    data = jsonable_encoder(row)
                # match_url_format = 'http://localhost:8080/csgo/match/{}'.format(record.id)
                match_url_format = f'{AWS_API_URL}/csgo/match/{record_id}'
                
                requests.post(match_url_format, json=data, headers={'Content-Type': "application/json", 'Accept': "application/json"} )
                logger.info(f'${record_id} has been processed to s3')

        return 200
    except Exception as err:
        logger.error(str(err))

@router.on_event("startup")
def load_schedule_or_create_blank():
    try:
        Schedule.start()
        
        # cleanup scheduled jobs
        Schedule.remove_all_jobs('default')
 
        # Schedule.add_job(fetch_upcoming_matches, 'interval', minutes=2)
        # Schedule.add_job(fetch_results, 'interval', minutes=3)
        # Schedule.add_job(fetch_map_stat, 'interval', minutes=5)
        # Schedule.add_job(generate_json_map_stat, 'interval', minutes=4)   
        
    except:
        logger.info("Unable to Create Schedule Object")

@router.on_event("shutdown")
def shutdown_event():
    Schedule.shutdown()