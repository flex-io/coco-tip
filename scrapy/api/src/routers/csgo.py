import json
from fastapi import APIRouter, Body
from fastapi.exceptions import HTTPException
from fastapi_sqlalchemy import db
from ..models import UpcomingMatches, Results, Match, MapStat, PlayerStat
from .. import database
import re
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from ..db_helper import NoneIf

router = APIRouter(
    prefix="/csgo",
    tags=["csgo"],
    responses={404: {"description": "Not found"}},
)

def safe_cast(val, to_type, default=None):
    try:
        return to_type(val)
    except (ValueError, TypeError):
        return default

@router.get("/matches")
def get_upcoming_match():
    try:
        with db():
            res = db.session.query(UpcomingMatches).all()
            # db.session.query(Results).filter(Results.is_process == False).all()
            resp = {"matches": jsonable_encoder(res)}
            # json_compatible_item_data = jsonable_encoder(res)
        return JSONResponse(content=resp)
    except Exception as err:
        raise HTTPException(status_code=500, detail=str(err))


@router.get("/game_detail/{match_id}")
def get_game_detail(match_id: int):
    try:
        with db():
            game_detail = db.session.execute("SELECT json_csgo_game_detail(:match_id) as game_detail", {"match_id": match_id})
            
            for row in game_detail:
                data = jsonable_encoder(row)

            return JSONResponse(content=data)
    except Exception as err:
        raise err

@router.post("/upcoming_matches")
def post_upcoming_match(body = Body(...)):
    # data = request.get_json()
    json_obj = json.loads(body)
    
    #TRUNCATE TABLE BEFORE INSERTING RECORDS
    database.truncate_table(UpcomingMatches)

    database.upsert_instance(UpcomingMatches, json_obj['upcoming_matches'], [])

    return 'success', 200

@router.post("/match_url")
def post_match(body=Body(...)):
    def update_results_is_process(match_id, value: bool):
        try:
            db.session.query(Results).filter(Results.match_id == match_id).update({"is_process": value})
            db.session.commit()
        except Exception as e:
            raise e


    def delete_match_result(match_id):
        try:
            update_results_is_process(match_id, False)
            db.session.query(Match).filter(Match.id == match_id).delete()
            db.session.commit()
            # database.delete_instance(Match, match_id)
            # update_match_result(match_id, is_process = False)
        except Exception as e:
            raise e


    data = json.loads(body)

    try:
   
        if 'match' not in data:
            raise HTTPException(status_code=400, detail='match is not defined.')

        # json_obj = json.loads(_encoder.encode(data['match']), object_hook=lambda d: Namespace(**d))
        match = data['match']


        delete_match_result(match['match_id'])
        
        database.add_instance(Match,
            id = match['match_id'],
            team1_logo = match['team1_logo'],
            team2_logo = match['team2_logo'],
            team1_url = match['team1_url'],
            team2_url = match['team2_url'],
            team1_id = match['team1_id'],
            team2_id = match['team2_id'],
            team1_rank = match['team1_rank'],
            team2_rank = match['team2_rank'],
            match_date = match['match_date'],
            event_url = match['event_url'],
            ban_pick = "|".join(match['ban_pick']),
            )

        #dict to GET team_id i.e /team/9183/winstrike
        # teams = {
        #     match['team1_url'].split("/")[3]: match['team1_url'].split("/")[2],
        #     match['team2_url'].split("/")[3]: match['team2_url'].split("/")[2],
        # }

        for cs_map in match['maps']:
            db_map_stat_id = database.add_instance(MapStat,
                map_stat_id = cs_map['mapstat_id'],
                match_id = cs_map['match_id'],
                pick_by_team_id = cs_map['pick_by_team_id'],
                map_round = cs_map['map_round'],
                mapstat_name = cs_map['mapstat_name'],
                team_id = cs_map['team_id'],
                team_name = cs_map['team_name'],
                score = cs_map['score'],
                url = cs_map['url'],
                round_history = "|".join(cs_map['round_history'])
            )
            for player_stat in cs_map['player_stats']:
                database.add_instance(PlayerStat,
                    parent_id = db_map_stat_id,
                    map_stat_id = player_stat['mapstat_id'],
                    match_id = cs_map['match_id'],
                    player_id = player_stat['player_id'],
                    player_name = player_stat['player_name'],
                    player_url = player_stat['player_url'],
                    kills = safe_cast(player_stat['kills'], int),
                    headshot = safe_cast(player_stat['headshot'], int),
                    assist = safe_cast(player_stat['assist'], int),
                    flash_assist = safe_cast(player_stat['flash_assist'], int),
                    deaths = safe_cast(player_stat['deaths'], int),
                    kdratio = re.sub('%', '', player_stat['kdratio']),
                    adr = safe_cast(player_stat['adr'], float),
                    fkdiff = safe_cast(player_stat['fkdiff'], int),
                    rating = safe_cast(player_stat['rating'], float),
                )
        

        match_id = data['match']['match_id']
        update_results_is_process(match_id, True)
        # update_match_result(data['match']['match_id'], is_process = True)
    except Exception as e:
        raise e
        # delete_match_result(data['match']['match_id'])


@router.post("/results")
def post_results_matches(body = Body(...)):
    # data = request.get_json()
    json_obj = json.loads(body)
    
    #TRUNCATE TABLE BEFORE INSERTING RECORDS

    database.upsert_instance(Results, json_obj['results'], ['is_process'])

    return json_obj, 200

@router.get('/results')
def get_results():
    res = db.session.query(Results).filter(NoneIf(Results.is_process, False) == False).all()
    # db.session.query(Results).filter(Results.is_process == False).all()
    resp = {'matches': [record.url for record in res]}
    json_compatible_item_data = jsonable_encoder(resp)
    return JSONResponse(content=json_compatible_item_data)
    # return ",".join(urls)