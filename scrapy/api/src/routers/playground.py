import json
from fastapi import APIRouter, Request, Body
from fastapi_sqlalchemy import db
from src.models import Results
from src import database
import re

router = APIRouter(
    prefix="/playground",
    tags=["playground"],
    responses={404: {"description": "Not found"}},
)

@router.get('/string_table')
def get_string_table(value_str: str = ''):
    res = db.session.query(Results).filter(Results.is_process == False).all()
    urls = [record.url for record in res]
    return urls

@router.patch('/string_table')
def patch_string_table():
    db.session.query(StringTable).update({"out_str": StringTable.value_str})
    return "success", 200 