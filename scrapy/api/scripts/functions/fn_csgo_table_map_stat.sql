DROP FUNCTION IF EXISTS fn_csgo_table_map_stat(int, int);

CREATE OR REPLACE FUNCTION fn_csgo_table_map_stat (
	p_team_id INT,
	p_month_interval INT
) 
RETURNS TABLE (
	id INT,
	match_date TIMESTAMP WITHOUT TIME ZONE,
	map_round INT,
	map_stat_id INT,
	map_name VARCHAR,
	match_id INT,
	team_id INT,
	team_name VARCHAR,
	team_round_history VARCHAR,
	team_rank INT,
	team_score INT,
	opponent_id INT,
	opponent_name VARCHAR,
	opponent_round_history VARCHAR,
	opponent_rank INT,
	opponent_score INT,
	pistol_1 BOOLEAN,
	pistol_16 BOOLEAN,
	is_win BOOLEAN,
	team_first_half_score INT,
	opponent_first_half_score INT
) 
LANGUAGE plpgsql
AS $$
DECLARE
	local_interval TIMESTAMP := CURRENT_DATE - (p_month_interval || ' month')::INTERVAL;
BEGIN
	RETURN query
		SELECT
			m.id,
			m.match_date,
			mms.map_round,
			mms.map_stat_id,
			mms.mapstat_name AS map_name, 
			mms.match_id,
			mms.team_id,
			mms.team_name,
			mms.round_history AS team_round_history,
			CASE WHEN mms.team_id = m.team1_id THEN m.team1_rank
				ELSE m.team2_rank
			END as team_rank,
			mms.score AS team_score,
			mms2.team_id as opponent_id,
			mms2.team_name as opponent_name,
			mms2.round_history as opponent_round_history,
			CASE WHEN mms2.team_id = m.team1_id THEN m.team1_rank
				ELSE m.team2_rank
			END as opponent_rank,
			mms2.score AS opponent_score,
			fn_csgo_bool_win_at_round(mms.round_history, 1) as pistol_1,
			fn_csgo_bool_win_at_round(mms.round_history, 16) as pistol_16,
			CASE WHEN mms.score > mms2.score THEN true ELSE false END as is_win,
			fn_csgo_int_score_by_range(mms.round_history, 1, 15) as team_first_half_score,
			fn_csgo_int_score_by_range(mms2.round_history, 1, 15) as opponent_first_half_score
		FROM csgo_match_map_stat mms
			INNER JOIN csgo_match m ON m.id = mms.match_id
			INNER JOIN csgo_match_map_stat mms2 ON mms2.map_stat_id = mms.map_stat_id
				AND mms2.team_id != mms.team_id
		WHERE mms.team_id = p_team_id
			AND m.match_date BETWEEN local_interval AND CURRENT_DATE;
	
END;$$