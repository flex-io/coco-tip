DROP FUNCTION IF EXISTS fn_csgo_bool_win_at_round(TEXT, INT);

CREATE OR REPLACE FUNCTION fn_csgo_bool_win_at_round(
	p_round_history TEXT,
    p_at_round INT
)
RETURNS BOOLEAN LANGUAGE plpgsql
AS $BODY$
DECLARE
    win_at_round BOOLEAN;
BEGIN win_at_round := CASE
    WHEN (
        string_to_array(REPLACE(p_round_history, '|', ','), ',')::int []
    ) [p_at_round] != 0 THEN true
    ELSE false
END;
RETURN win_at_round;
END $BODY$;