DROP FUNCTION IF EXISTS fn_csgo_table_markets(int);

CREATE OR REPLACE FUNCTION fn_csgo_table_markets (
	p_match_id INT
)
RETURNS TABLE (
    match_id INT,
	map_round INT,
	category VARCHAR,
	bet_type VARCHAR
)
LANGUAGE plpgsql
AS $$
BEGIN
	RETURN query
	SELECT x.map_round, x.category, x.bet_type
	FROM (VALUES
			(0, 'match', 'to_win'),
			(0, 'match', 'total_maps')
		 ) AS x(map_round, category, bet_type)

	UNION ALL

	SELECT DISTINCT mms.map_round, mms.mapstat_name, y.bet_type
	FROM csgo_match_map_stat mms
		CROSS JOIN (VALUES
						('to_win'),
						('first_half')
				   ) AS y(bet_type)
	WHERE mms.match_id = p_match_id;
END;$$