DROP FUNCTION IF EXISTS fn_csgo_match_team_map_result(INT, INT, INT);

CREATE OR REPLACE FUNCTION fn_csgo_match_team_map_result (
	p_team_id INT,
	p_match_id INT DEFAULT 0,
	p_limit INT DEFAULT 99999
)
RETURNS TABLE (
    match_id INT,
	map_stat_id INT,
	is_favourite BOOLEAN,
	map_round INT,
	mapstat_name VARCHAR,

	team_id INT,
	team_rank INT,
	team_name VARCHAR,
	team_win BOOLEAN,
	team_score INT,
	team_round_history VARCHAR,
	team_clutch_score INT,
	team_pick BOOLEAN,
	team_1st_half_winner BOOLEAN,
	team_clutch_1st_half INT,
	team_win_at_pistol_1 BOOLEAN,
	team_win_at_pistol_16 BOOLEAN,
	
	opponent_id INT,
	opponent_rank INT,
	opponent_name VARCHAR,
	opponent_win BOOLEAN,
	opponent_score INT,
	opponent_round_history VARCHAR,
	opponent_clutch_score INT,
	opponent_pick BOOLEAN,
	opponent_1st_half_winner BOOLEAN,
	opponent_clutch_1st_half INT,
	opponent_win_at_pistol_1 BOOLEAN,
	opponent_win_at_pistol_16 BOOLEAN
)
LANGUAGE plpgsql
AS $$
BEGIN
	RETURN query
	SELECT
		tmp.id AS match_id,
		tmp.map_stat_id,
		tmp.is_favourite,
		tmp.map_round,
		tmp.mapstat_name,

		tmp.team_id,
		tmp.team_rank,
		tmp.team_name,
		tmp.team_win,
		tmp.team_score,
		tmp.team_round_history,
		tmp.team_clutch_score,
		tmp.team_pick,
		tmp.team_1st_half_winner,
		tmp.team_clutch_1st_half,
		tmp.team_win_at_pistol_1,
		tmp.team_win_at_pistol_16,

		tmp.opponent_id,
		tmp.opponent_rank,
		tmp.opponent_name,
		NOT tmp.team_win AS opponent_win,
		tmp.opponent_score,
		tmp.opponent_round_history,
		tmp.opponent_clutch_score,
		NOT tmp.team_pick AS opponent_pick,
		NOT tmp.team_1st_half_winner AS opponent_1st_half_winner,
		tmp.opponent_clutch_1st_half,
		NOT tmp.team_win_at_pistol_1 AS opponent_win_at_pistol_1,
		NOT tmp.team_win_at_pistol_16 AS opponent_win_at_pistol_16
	FROM (
		SELECT
			m.id,
			ms.map_stat_id,
			CASE WHEN COALESCE(m.team1_rank, 0) < COALESCE(m.team2_rank, 0) THEN TRUE ELSE FALSE END AS is_favourite,
			ms.map_round,
			ms.mapstat_name,
			-- TEAM FIELDS
			ms.team_id AS team_id,
			m.team1_rank AS team_rank,
			ms.team_name,
			CASE WHEN ms.score > ms_o.score THEN TRUE ELSE FALSE END AS team_win,
			ms.score AS team_score,
			ms.round_history AS team_round_history,
			ms.score - ms_o.score AS team_clutch_score,
			-- [-1 = No one picks]
			-- [ 1 = Pick by team]
			-- [ 0 = Not pick by team]
			CASE
				WHEN ms.pick_by_team_id IS NULL AND ms_o.pick_by_team_id IS NULL
					THEN NULL
				WHEN ms.pick_by_team_id IS NOT NULL
					THEN TRUE
				ELSE FALSE
			END AS team_pick,
			CASE
				WHEN ABS(fn_csgo_int_score_by_range(ms.round_history, 1, 15)) > ABS(fn_csgo_int_score_by_range(ms_o.round_history, 1, 15))
					THEN TRUE
				ELSE FALSE
			END AS team_1st_half_winner,
			ABS(fn_csgo_int_score_by_range(ms.round_history, 1, 15)) - ABS(fn_csgo_int_score_by_range(ms_o.round_history, 1, 15)) AS team_clutch_1st_half,
			fn_csgo_bool_win_at_round(ms.round_history, 1) as team_win_at_pistol_1,
			fn_csgo_bool_win_at_round(ms.round_history, 16) as team_win_at_pistol_16,
			-- OPPONENT FIELDS
			ms_o.team_id AS opponent_id,
			m.team2_rank AS opponent_rank,
			ms_o.team_name AS opponent_name,
			ms_o.score AS opponent_score,
			ms_o.round_history AS opponent_round_history,
			ABS(fn_csgo_int_score_by_range(ms_o.round_history, 1, 15)) - ABS(fn_csgo_int_score_by_range(ms.round_history, 1, 15)) AS opponent_clutch_1st_half,
			ms_o.score - ms.score AS opponent_clutch_score
		
		FROM csgo_match m
			INNER JOIN csgo_match_map_stat ms ON ms.match_id = m.id
			INNER JOIN csgo_match_map_stat ms_o ON ms_o.match_id = m.id
				AND ms_o.team_id != ms.team_id
				AND ms_o.map_stat_id = ms.map_stat_id
		WHERE (ms.team_id = p_team_id)
			AND (m.id = p_match_id OR (p_match_id = 0))
		ORDER BY m.match_date DESC, ms.map_stat_id, ms.map_round ASC
		LIMIT p_limit
	)tmp;

END;$$

