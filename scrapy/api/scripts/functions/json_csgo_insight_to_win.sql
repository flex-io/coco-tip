
CREATE OR REPLACE FUNCTION public.json_csgo_insight_towin(p_team1_id integer, p_team2_id integer, p_limit INT DEFAULT 20)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
DECLARE
    d_json json;
BEGIN
	WITH
		team1 AS (SELECT * FROM fn_csgo_table_match_result(p_team1_id)),
		team2 AS (SELECT * FROM fn_csgo_table_match_result(p_team2_id)),
		h2h AS (
			(SELECT * FROM team1 a WHERE a.opponent_id IN(SELECT team_id FROM team2) LIMIT p_limit)
			
			UNION ALL
			
			(SELECT * FROM team2 a WHERE a.opponent_id IN(SELECT team_id FROM team1) LIMIT p_limit)
		),
		favourite AS (
			(SELECT * FROM team1 WHERE is_favourite = TRUE LIMIT p_limit)
			UNION ALL
			(SELECT * FROM team2 WHERE is_favourite = TRUE LIMIT p_limit)
		),
		underdog AS (
			(SELECT * FROM team1 WHERE is_favourite = FALSE LIMIT p_limit)
			UNION ALL
			(SELECT * FROM team2 WHERE is_favourite = FALSE LIMIT p_limit)
		),
		same_opponent AS (
			(SELECT * FROM team1 WHERE opponent_id IN(SELECT opponent_id FROM team2) LIMIT p_limit)
			UNION ALL
			(SELECT * FROM team2 WHERE opponent_id IN(SELECT opponent_id FROM team1) LIMIT p_limit)
		)
	SELECT row_to_json(t)
	FROM (
	  	SELECT 'Main - To Win' as group_name,
	  	(
			SELECT ARRAY_TO_JSON(
				ARRAY_AGG(
					row_to_json(x)
				)
			)
			FROM (
				SELECT 'H2H' AS title,
					CONCAT(y.team1_win, '/', y.team1_lost) AS leading,
					CONCAT(y.team2_win, '/', y.team2_lost) AS trailing,
					(
						SELECT ARRAY_AGG(row_to_json(a))
						FROM h2h a
						
					)AS breakdown
				FROM (
					SELECT
						-- TEAM 1
						SUM(CASE
								WHEN  a.team_id = p_team1_id THEN a.team_win::INT
								ELSE 0
							END
						) AS team1_win,
						SUM(CASE
								WHEN  a.team_id = p_team1_id THEN (NOT a.team_win)::INT
								ELSE 0
							END
						) AS team1_lost,
						-- TEAM 2 						
						SUM(CASE
								WHEN  a.team_id = p_team2_id THEN a.team_win::INT
								ELSE 0
							END
						) AS team2_win,
						SUM(CASE
								WHEN  a.team_id = p_team2_id THEN (NOT a.team_win)::INT
								ELSE 0
							END
						) AS team2_lost
					FROM h2h a
				)y
				
				UNION ALL
				
				SELECT 'Favourite' AS title,
					CONCAT(y.team1_win, '/', y.team1_lost) AS leading,
					CONCAT(y.team2_win, '/', y.team2_lost) AS trailing,
					(
						SELECT ARRAY_AGG(row_to_json(a))
						FROM favourite a
						
					)AS breakdown
					
				FROM (
					SELECT
						-- TEAM 1
						SUM(CASE
								WHEN  a.team_id = p_team1_id THEN a.team_win::INT
								ELSE 0
							END
						) AS team1_win,
						SUM(CASE
								WHEN  a.team_id = p_team1_id THEN (NOT a.team_win)::INT
								ELSE 0
							END
						) AS team1_lost,
						-- TEAM 2 						
						SUM(CASE
								WHEN  a.team_id = p_team2_id THEN a.team_win::INT
								ELSE 0
							END
						) AS team2_win,
						SUM(CASE
								WHEN  a.team_id = p_team2_id THEN (NOT a.team_win)::INT
								ELSE 0
							END
						) AS team2_lost
					FROM favourite a
				)y	
				
				UNION ALL
				
				SELECT 'Underdog' AS title,
					CONCAT(y.team1_win, '/', y.team1_lost) AS leading,
					CONCAT(y.team2_win, '/', y.team2_lost) AS trailing,
					(
						SELECT ARRAY_AGG(row_to_json(a))
						FROM underdog a
						
					)AS breakdown
					
				FROM (
					SELECT
						-- TEAM 1
						SUM(CASE
								WHEN  a.team_id = p_team1_id THEN a.team_win::INT
								ELSE 0
							END
						) AS team1_win,
						SUM(CASE
								WHEN  a.team_id = p_team1_id THEN (NOT a.team_win)::INT
								ELSE 0
							END
						) AS team1_lost,
						-- TEAM 2 						
						SUM(CASE
								WHEN  a.team_id = p_team2_id THEN a.team_win::INT
								ELSE 0
							END
						) AS team2_win,
						SUM(CASE
								WHEN  a.team_id = p_team2_id THEN (NOT a.team_win)::INT
								ELSE 0
							END
						) AS team2_lost
					FROM underdog a
				)y	
				
				UNION ALL
				
				SELECT 'Same Opponent' AS title,
					CONCAT(y.team1_win, '/', y.team1_lost) AS leading,
					CONCAT(y.team2_win, '/', y.team2_lost) AS trailing,
					(
						SELECT ARRAY_AGG(row_to_json(a))
						FROM same_opponent a
						
					)AS breakdown
					
				FROM (
					SELECT
						-- TEAM 1
						SUM(CASE
								WHEN  a.team_id = p_team1_id THEN a.team_win::INT
								ELSE 0
							END
						) AS team1_win,
						SUM(CASE
								WHEN  a.team_id = p_team1_id THEN (NOT a.team_win)::INT
								ELSE 0
							END
						) AS team1_lost,
						-- TEAM 2 						
						SUM(CASE
								WHEN  a.team_id = p_team2_id THEN a.team_win::INT
								ELSE 0
							END
						) AS team2_win,
						SUM(CASE
								WHEN  a.team_id = p_team2_id THEN (NOT a.team_win)::INT
								ELSE 0
							END
						) AS team2_lost
					FROM same_opponent a
				)y	

				
			)x
		) as header
	)t
	INTO d_json;
    RETURN d_json;
END; $function$
