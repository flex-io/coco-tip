DROP FUNCTION IF EXISTS fn_csgo_table_pistol_rank(int, VARCHAR);

CREATE OR REPLACE FUNCTION fn_csgo_table_pistol_rank (
    p_month_interval INT DEFAULT 4,
	p_map VARCHAR DEFAULT ''
) 
	RETURNS TABLE (
		pistol_rank BIGINT,
		team_id INT,
		team_name VARCHAR,
		total_win BIGINT,
		win_diff BIGINT,
		pistol_count BIGINT
	) 
	LANGUAGE plpgsql
AS $$
DECLARE
	d_month_interval TIMESTAMP := CURRENT_DATE - (p_month_interval || ' month')::INTERVAL;
BEGIN
	RETURN query
	SELECT row_number() OVER (PARTITION BY true) AS pistol_rank,
		tmp3.team_id,
		tmp3.team_name::VARCHAR,
		tmp3.total_win,
		tmp3.win_diff,
		tmp3.pistol_count
	FROM (
			SELECT tmp2.team_id,
				tmp2.team_name,
				tmp2.total_win,
				tmp2.pistol_count - tmp2.total_win as win_diff,
				tmp2.pistol_count
				FROM (
					SELECT tmp.team_id, tmp.team_name, SUM(tmp.pistol_win) as total_win, COUNT(tmp.*) as pistol_count
					FROM
						(
								SELECT mms.team_id, mms.team_name, mms.mapstat_name, 1 as round, CASE WHEN fn_csgo_bool_win_at_round(mms.round_history, 1) THEN 1 ELSE 0 END as pistol_win
								FROM csgo_match m
									INNER JOIN csgo_match_map_stat mms ON mms.match_id = m.id
								WHERE m.match_date BETWEEN d_month_interval AND CURRENT_DATE
									AND (mms.mapstat_name = p_map OR p_map = '')

								UNION ALL

								SELECT mms.team_id, mms.team_name, mms.mapstat_name, 16 as round, CASE WHEN fn_csgo_bool_win_at_round(mms.round_history, 16) THEN 1 ELSE 0 END as pistol_win
								FROM csgo_match m
									INNER JOIN csgo_match_map_stat mms ON mms.match_id = m.id
								WHERE m.match_date BETWEEN d_month_interval AND CURRENT_DATE
									AND (mms.mapstat_name = p_map OR p_map = '')
						)tmp
						GROUP BY tmp.team_id, tmp.team_name
				)tmp2
	)tmp3
	ORDER BY tmp3.total_win DESC, tmp3.win_diff ASC, tmp3.pistol_count DESC;
	
END;$$