DROP FUNCTION IF EXISTS fn_csgo_table_match_result(int);

CREATE OR REPLACE FUNCTION fn_csgo_table_match_result (
	p_match_id INT,
	p_team_id INT
)
RETURNS TABLE (
    match_id INT,
    match_date TIMESTAMP,
    team_id INT,
    team_name VARCHAR,
    opponent_win VARCHAR,
    is_favourite BOOLEAN,
    team_rank VARCHAR,
    team_win BOOLEAN,
    opponent_win_map INT,
    bo INT
)
LANGUAGE plpgsql
AS $$
BEGIN
	RETURN query
	SELECT
		tmp.match_id,
		tmp.match_date,
		tmp.team_id,
		mr.team_name,
		mr.opponent_name,
		mr.is_favourite,
		mr.team_rank,
		CASE
			WHEN SUM(mr.team_win::INT) > SUM(mr.opponent_win::INT)
				THEN TRUE
			ELSE FALSE
		END AS team_win,
		
		SUM(mr.team_win::INT) AS team_win_map, SUM(mr.opponent_win::INT) AS opponent_win_map,
		CASE
			WHEN SUM(mr.team_win::INT) + SUM(mr.opponent_win::INT) IN(2, 3) THEN 3
			WHEN SUM(mr.team_win::INT) + SUM(mr.opponent_win::INT) IN(4, 5) THEN 5
			ELSE 1
		END AS bo
	FROM (
		SELECT m.match_date, m.id AS match_id, 10469 AS team_id
		FROM csgo_match m
		WHERE 10469 IN(m.team1_id, m.team2_id)
		ORDER BY m.match_date DESC
		LIMIT 20
	)tmp
	CROSS JOIN fn_csgo_match_team_map_result(10469, tmp.match_id) mr
	GROUP BY tmp.match_date, tmp.match_id, tmp.team_id, mr.team_name, mr.opponent_name, mr.is_favourite,
		mr.team_rank
	ORDER BY tmp.match_date DESC;
END;$$