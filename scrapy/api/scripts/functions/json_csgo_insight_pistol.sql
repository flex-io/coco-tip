

CREATE OR REPLACE FUNCTION public.json_csgo_insight_pistol(
	p_team1_id integer,
	p_team2_id integer,
	p_team1_name VARCHAR,
	p_team2_name VARCHAR,
	p_limit integer DEFAULT 20)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
DECLARE
    d_json json;
BEGIN
	WITH
		team1 AS (
			SELECT
				match_date,
				match_id,
				team_id,
				team_win_at_pistol_1,
				opponent_id,
				is_favourite,
				team_win_at_pistol_16,
				mapstat_name,
				team_pick,
				opponent_name
			FROM fn_csgo_match_team_map_result(p_team1_id)
		),
		team2 AS (
			SELECT 
				match_date,
				match_id,
				team_id,
				team_win_at_pistol_1,
				opponent_id,
				is_favourite,
				team_win_at_pistol_16,
				mapstat_name,
				team_pick,
				opponent_name
			FROM fn_csgo_match_team_map_result(p_team2_id)
		),
		maps AS (
			SELECT DISTINCT t1.mapstat_name as map FROM team1 t1
			UNION
			SELECT DISTINCT t2.mapstat_name as map FROM team2 t2
		),
-- 		h2h AS (
-- 			(SELECT * FROM team1 a WHERE a.opponent_id IN(SELECT team_id FROM team2) LIMIT p_limit)
-- 			
-- 			UNION ALL
-- 			
-- 			(SELECT * FROM team2 a WHERE a.opponent_id IN(SELECT team_id FROM team1) LIMIT p_limit)
-- 		),
		favourite AS (
			(SELECT * FROM team1 WHERE is_favourite = TRUE LIMIT p_limit)
			UNION ALL
			(SELECT * FROM team2 WHERE is_favourite = TRUE LIMIT p_limit)
		),
		underdog AS (
			(SELECT * FROM team1 WHERE is_favourite = FALSE LIMIT p_limit)
			UNION ALL
			(SELECT * FROM team2 WHERE is_favourite = FALSE LIMIT p_limit)
		),
-- 		pick AS (
-- 			(SELECT * FROM team1 WHERE team_pick = TRUE LIMIT p_limit)
-- 			UNION ALL
-- 			(SELECT * FROM team2 WHERE team_pick = TRUE LIMIT p_limit)
-- 		),
-- 		discard AS (
-- 			(SELECT * FROM team1 WHERE team_pick = FALSE LIMIT p_limit)
-- 			UNION ALL
-- 			(SELECT * FROM team2 WHERE team_pick = FALSE LIMIT p_limit)
-- 		),
		all_tables AS (
			SELECT 'Favourite' AS cat, f.*
			FROM favourite f
			UNION ALL
			SELECT 'Underdog' AS cat, u.*
			FROM underdog u
		),
		tabs AS (
			SELECT * FROM (VALUES(p_team1_id, p_team1_name), (0, 'h2h'), (p_team2_id, p_team2_name)) AS t(tab_id, tab_name)
		)

	SELECT row_to_json(t)
	FROM (
	  	SELECT 'Pistol' as group_name,
	  		(
	  			SELECT row_to_json(v)
	  			FROM (VALUES(p_team1_name, 'vs', p_team2_name)) AS v(left_text, center_text, right_text)
	  		) AS screen,
	  		(
				SELECT ARRAY_AGG(
					json_build_object(
				        'id', t.tab_id,
				        'tab_name', t.tab_name,
				        'details', (
				        	SELECT ARRAY_AGG(
				        		(SELECT (
					        		 json_build_object(
					        			'group_name', CONCAT('map - ', x.map),
					        			'header', json_build_object(
					        						'title', x.cat,
					        						'win', SUM(a.team_win_at_pistol_1::INT + a.team_win_at_pistol_16::INT),
					        						'lost', SUM((NOT a.team_win_at_pistol_1)::INT + (NOT a.team_win_at_pistol_16)::INT) 
					        						), 
					        			'sub_row', ARRAY_AGG(
					        					row_to_json(a)
					        				)
					        			
					        			
				        			)
				        		)
				        		FROM all_tables a
		        				WHERE a.cat = x.cat
		        					AND a.team_id = t.tab_id)
				        	)
				        	FROM (
					        	SELECT m.map, n.cat
					        	FROM maps m
					        		CROSS JOIN (VALUES('Favourite'), ('Underdog'))AS n(cat)
					        	WHERE t.tab_id IN(p_team1_id, p_team2_id)
					        )x
				        )
		        
	    			)
				)			
  				FROM tabs t
	  		) AS tabs




-- 	  		(
-- 				SELECT ARRAY_TO_JSON(
-- 					ARRAY_AGG(
-- 						row_to_json(x)
-- 					)
-- 			)
-- 			FROM (
-- 				SELECT m.map AS title,
-- 					(
-- 						SELECT ARRAY_TO_JSON(
-- 							ARRAY_AGG(
-- 								row_to_json(x)
-- 							)
-- 						)
-- 						FROM (
-- 							

-- 						) x
-- 					) AS breakdown
-- 				FROM maps m
-- 			)x
-- 		) as header
	) AS t
	INTO d_json;
    RETURN d_json;
END; $function$

-- 				SELECT m.map AS title,
-- 					CONCAT(
-- 						SUM(
-- 							(t.team_win_at_pistol_1::INT + t.team_win_at_pistol_16::INT) * (t.team_id = p_team1_id)::INT
-- 						),
-- 						'/',
-- 						SUM(((NOT t.team_win_at_pistol_1)::INT + (NOT t.team_win_at_pistol_16)::INT)  * (t.team_id = p_team1_id)::INT)
-- 					) AS leading,
-- 					CONCAT(
-- 						SUM((t.team_win_at_pistol_1::INT + t.team_win_at_pistol_16::INT) * (t.team_id = p_team2_id)::INT),
-- 						'/',
-- 						SUM(((NOT t.team_win_at_pistol_1)::INT + (NOT t.team_win_at_pistol_16)::INT)  * (t.team_id = p_team2_id)::INT)
-- 					) AS trailing,
-- 					(
-- 						SELECT ARRAY_AGG(row_to_json(a))
-- 						FROM teams a
-- 						WHERE a.mapstat_name = m.map
-- 					) AS breakdown
-- 				FROM maps m
-- 					INNER JOIN teams t ON t.mapstat_name = m.map
-- 				GROUP BY m.map
				
-- 				UNION ALL
-- 			
-- 				SELECT 'H2H' AS title,
-- 					CONCAT(y.team1_win, '/', y.team1_lost) AS leading,
-- 					CONCAT(y.team2_win, '/', y.team2_lost) AS trailing,
-- 					(
-- 						SELECT ARRAY_AGG(row_to_json(a))
-- 						FROM h2h a
-- 						WHERE a.team_id = p_team1_id
-- 						
-- 					)AS breakdown
-- 				FROM (
-- 					SELECT
-- 						-- TEAM 1
-- 						SUM(CASE
-- 								WHEN  a.team_id = p_team1_id THEN (a.team_win_at_pistol_1::INT) + (a.team_win_at_pistol_16::INT)
-- 								ELSE 0
-- 							END
-- 						) AS team1_win,
-- 						SUM(CASE
-- 								WHEN  a.team_id = p_team1_id THEN (NOT a.team_win_at_pistol_1)::INT + (NOT a.team_win_at_pistol_16)::INT
-- 								ELSE 0
-- 							END
-- 						) AS team1_lost,
-- 						-- TEAM 2 						
-- 						SUM(CASE
-- 								WHEN  a.team_id = p_team2_id THEN (a.team_win_at_pistol_1::INT) + (a.team_win_at_pistol_16::INT)
-- 								ELSE 0
-- 							END
-- 						) AS team2_win,
-- 						SUM(CASE
-- 								WHEN  a.team_id = p_team2_id THEN (NOT a.team_win_at_pistol_1)::INT + (NOT a.team_win_at_pistol_16)::INT
-- 								ELSE 0
-- 							END
-- 						) AS team2_lost
-- 					FROM h2h a
-- 				)y
-- 				
-- 				UNION ALL
-- 				
-- 				SELECT 'Favourite' AS title,
-- 					CONCAT(y.team1_win, '/', y.team1_lost) AS leading,
-- 					CONCAT(y.team2_win, '/', y.team2_lost) AS trailing,
-- 					(
-- 						SELECT ARRAY_AGG(row_to_json(a))
-- 						FROM favourite a
-- 						
-- 					)AS breakdown
-- 					
-- 				FROM (
-- 					SELECT
-- 						-- TEAM 1
-- 						SUM(CASE
-- 								WHEN  a.team_id = p_team1_id THEN (a.team_win_at_pistol_1::INT) + (a.team_win_at_pistol_16::INT)
-- 								ELSE 0
-- 							END
-- 						) AS team1_win,
-- 						SUM(CASE
-- 								WHEN  a.team_id = p_team1_id THEN (NOT a.team_win_at_pistol_1)::INT + (NOT a.team_win_at_pistol_16)::INT
-- 								ELSE 0
-- 							END
-- 						) AS team1_lost,
-- 						-- TEAM 2 						
-- 						SUM(CASE
-- 								WHEN  a.team_id = p_team2_id THEN (a.team_win_at_pistol_1::INT) + (a.team_win_at_pistol_16::INT)
-- 								ELSE 0
-- 							END
-- 						) AS team2_win,
-- 						SUM(CASE
-- 								WHEN  a.team_id = p_team2_id THEN (NOT a.team_win_at_pistol_1)::INT + (NOT a.team_win_at_pistol_16)::INT
-- 								ELSE 0
-- 							END
-- 						) AS team2_lost
-- 					FROM favourite a
-- 				)y
-- 				
-- 				UNION ALL
-- 				
-- 				SELECT 'Underdog' AS title,
-- 					CONCAT(y.team1_win, '/', y.team1_lost) AS leading,
-- 					CONCAT(y.team2_win, '/', y.team2_lost) AS trailing,
-- 					(
-- 						SELECT ARRAY_AGG(row_to_json(a))
-- 						FROM underdog a
-- 						
-- 					)AS breakdown
-- 					
-- 				FROM (
-- 					SELECT
-- 						-- TEAM 1
-- 						SUM(CASE
-- 								WHEN  a.team_id = p_team1_id THEN (a.team_win_at_pistol_1::INT) + (a.team_win_at_pistol_16::INT)
-- 								ELSE 0
-- 							END
-- 						) AS team1_win,
-- 						SUM(CASE
-- 								WHEN  a.team_id = p_team1_id THEN (NOT a.team_win_at_pistol_1)::INT + (NOT a.team_win_at_pistol_16)::INT
-- 								ELSE 0
-- 							END
-- 						) AS team1_lost,
-- 						-- TEAM 2 						
-- 						SUM(CASE
-- 								WHEN  a.team_id = p_team2_id THEN (a.team_win_at_pistol_1::INT) + (a.team_win_at_pistol_16::INT)
-- 								ELSE 0
-- 							END
-- 						) AS team2_win,
-- 						SUM(CASE
-- 								WHEN  a.team_id = p_team2_id THEN (NOT a.team_win_at_pistol_1)::INT + (NOT a.team_win_at_pistol_16)::INT
-- 								ELSE 0
-- 							END
-- 						) AS team2_lost
-- 					FROM underdog a
-- 				)y	
	
				
				


