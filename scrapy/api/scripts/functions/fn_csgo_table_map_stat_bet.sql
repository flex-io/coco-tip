SELECT x.bet_type,
	CASE x.bet_type
		WHEN 'match.to_win' THEN
			tmp.map1_to_win::VARCHAR
		WHEN 'match.total_maps' THEN
			CASE WHEN tmp.total_maps = tmp.bo THEN 'over|2.5' ELSE 'under|2.5' END
		WHEN 'map1.to_win' THEN
			tmp.map1_to_win::VARCHAR
	END
FROM (
	SELECT
		CASE WHEN m1_t1.score > m1_t2.score THEN m1_t1.team_id ELSE m1_t2.team_id END AS map1_to_win,
		CASE WHEN m2_t1.score > m2_t2.score THEN m2_t1.team_id ELSE m2_t2.team_id END AS map2_to_win,
		CASE WHEN m3_t1.score > m3_t2.score THEN m3_t1.team_id ELSE m3_t2.team_id END AS map3_to_win,
		GREATEST(m1_t1.map_round, m2_t1.map_round, m3_t1.map_round, NULL) AS total_maps,
		CASE WHEN GREATEST(m1_t1.map_round, m2_t1.map_round, m3_t1.map_round, NULL, NULL) BETWEEN 2 AND 3 THEN 3
			WHEN GREATEST(m1_t1.map_round, m2_t1.map_round, m3_t1.map_round, NULL, NULL) > 3 THEN 5
			ELSE 1
		END AS bo
-- 		CASE WHEN m2_t1.score > m2_t2.score THEN true ELSE false END AS m2_t1_win,
-- 		CASE WHEN m3_t1.score > m3_t2.score THEN true ELSE false END AS m3_t1_win
	FROM csgo_match m
		INNER JOIN csgo_match_map_stat m1_t1 ON m1_t1.match_id = m.id
			AND m1_t1.team_id = m.team1_id
			AND m1_t1.map_round = 1
		INNER JOIN csgo_match_map_stat m1_t2 ON m1_t2.match_id = m.id
			AND m1_t2.team_id = m.team2_id
			AND m1_t2.map_round = 1
		LEFT JOIN csgo_match_map_stat m2_t1 ON m2_t1.match_id = m.id
			AND m2_t1.team_id = m.team1_id
			AND m2_t1.map_round = 2
		LEFT JOIN csgo_match_map_stat m2_t2 ON m2_t2.match_id = m.id
			AND m2_t2.team_id = m.team2_id
			AND m2_t2.map_round = 2
		LEFT JOIN csgo_match_map_stat m3_t1 ON m3_t1.match_id = m.id
			AND m3_t1.team_id = m.team1_id
			AND m3_t1.map_round = 3
		LEFT JOIN csgo_match_map_stat m3_t2 ON m3_t2.match_id = m.id
			AND m3_t2.team_id = m.team2_id
			AND m3_t2.map_round = 3
	WHERE m.id = 2340181
)tmp
CROSS JOIN (VALUES
			('match.to_win'),
			('match.total_maps'),
			('map1.to_win')
		   ) as x (bet_type)


