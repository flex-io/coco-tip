DROP FUNCTION IF EXISTS fn_csgo_bool_win_at_round(text, int);

CREATE OR REPLACE FUNCTION public.fn_csgo_bool_win_at_round(
	text, int)
    RETURNS BOOLEAN
    LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
  win_at_round BOOLEAN;
BEGIN
	win_at_round := CASE WHEN (string_to_array(REPLACE($1, '|', ','), ',' )::int[])[$2] != 0 THEN true ELSE false END;
	RETURN win_at_round;
END
$BODY$;