DROP FUNCTION IF EXISTS fn_csgo_table_pistol_by_round(int, int, int);

CREATE OR REPLACE FUNCTION fn_csgo_table_pistol_by_round (
	p_team_id INT DEFAULT 0,
	p_map_round INT DEFAULT 1,
	p_month_interval INT DEFAULT 4
) 
RETURNS TABLE (
    team_id INT,
	team_name VARCHAR,
	mapstat_name VARCHAR,
	round INT,
	round_score_type INT,
	is_win BOOLEAN,
	is_cs_side BOOLEAN
)
LANGUAGE plpgsql
AS $$
DECLARE
	local_interval TIMESTAMP := CURRENT_DATE - (p_month_interval || ' month')::INTERVAL;
BEGIN
	RETURN query
    SELECT tmp.team_id,
        tmp.team_name::VARCHAR,
        tmp.mapstat_name::VARCHAR,
        tmp.round,
        tmp.round_score_type,
        CASE WHEN tmp.round_score_type != 0 THEN true ELSE false END AS is_win,
        CASE
            WHEN tmp.round_score_type != 0 THEN
                CASE WHEN tmp.round_score_type > 0 THEN true ELSE false END
            ELSE
                CASE WHEN tmp.opponent_round_score_type > 0 THEN false ELSE true END
        END AS is_cs_side
    FROM (
            SELECT mms.team_id,
                mms.team_name,
                mms.mapstat_name,
                p_map_round AS round,
                (string_to_array(REPLACE(mms.round_history, '|', ','), ',' )::int[])[p_map_round] AS round_score_type,
                (string_to_array(REPLACE(mms2.round_history, '|', ','), ',' )::int[])[p_map_round] AS opponent_round_score_type
            FROM csgo_match m
                INNER JOIN csgo_match_map_stat mms ON mms.match_id = m.id
                INNER JOIN csgo_match_map_stat mms2 ON mms2.map_stat_id = mms.map_stat_id
                    AND mms2.team_id != mms.team_id
            WHERE m.match_date BETWEEN local_interval AND CURRENT_DATE
				AND (mms.team_id = p_team_id OR p_team_id = 0)
    )tmp;
END;$$
 
