DROP FUNCTION IF EXISTS fn_csgo_int_score_by_range(text, int, int);

CREATE OR REPLACE FUNCTION public.fn_csgo_int_score_by_range(
        p_round_history text,
        range_from int,
        range_to int
    )
RETURNS INTEGER
LANGUAGE 'plpgsql' AS
$BODY$
    DECLARE
        s INT;
    BEGIN
    SELECT SUM(
        CASE
            WHEN t::int < 0 THEN -1
            WHEN t::int > 0 THEN 1
            ELSE 0
        END) into s
    FROM unnest(
            (string_to_array(REPLACE($1, '|', ','), ',')) [$2:$3]
        ) AS T(num);
    RETURN s;
    END
$BODY$;

ALTER FUNCTION public.fn_csgo_int_score_by_range(text) OWNER TO postgres;