DROP FUNCTION IF EXISTS fn_csgo_json_map_stat(int, int);

CREATE OR REPLACE FUNCTION fn_csgo_json_map_stat(
	p_match_id int,
	p_month_interval int
)
RETURNS json AS $$
DECLARE
    d_json json;
BEGIN
	SELECT row_to_json(t)
	FROM (
	  SELECT id, match_date, bo, team1_id, team2_id,
		(
		  SELECT array_to_json(array_agg(row_to_json(d)))
		  FROM (
			  	SELECT * 
			  	FROM (
						SELECT * FROM fn_csgo_table_map_stat(um.team1_id, p_month_interval)
						UNION
						SELECT * FROM fn_csgo_table_map_stat(um.team2_id, p_month_interval)
				  )c
				ORDER BY c.match_id DESC
		  ) d
		) as map_stats
	  FROM csgo_upcoming_matches um
	  WHERE um.id = p_match_id
	) t
	INTO d_json;
    RETURN d_json;
END; $$
LANGUAGE plpgsql
SECURITY INVOKER;