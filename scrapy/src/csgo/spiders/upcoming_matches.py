# // quote=response.css("div.upcomingMatchesSection div.upcomingMatch a div.matchTeams div.team1 div.matchTeamName::text").getall()
# scrapy crawl quotes -O quotes.json
import scrapy
from .. import items
from ..converter import TimestampUnix13ToDateTime
import logging

logger = logging.getLogger(__name__)

class UpcomingMatchesSpider(scrapy.Spider):

    def __init__(self, *args, **kwargs):
        super(UpcomingMatchesSpider, self).__init__(*args, **kwargs)
        logger.setLevel(logging.ERROR)
    name = "upcoming_matches"
    start_urls = [
        'https://www.hltv.org/matches',
    ]

    def parse(self, response):
        res = items.UpcomingMatches()
        matches = []

        for quote in response.css('div.upcomingMatchesSection div.upcomingMatch'):
            if quote.css('::attr(team1)').get() and quote.css('::attr(team2)').get():
                match_date = quote.css(
                    '::attr(data-zonedgrouping-entry-unix)').get()

                matches.append({
                    'id': quote.css('a.match::attr(href)').get().split("/")[2],
                    'url': quote.css('a.match::attr(href)').get(),
                    # Convert Date-Unix13 to DateTime
                    'match_date': TimestampUnix13ToDateTime(match_date),
                    'team1_id': quote.css('::attr(team1)').get(),
                    'team2_id': quote.css('::attr(team2)').get(),
                    'team1_name': quote.css('a.match div.matchTeams div.team1 div.matchTeamName::text').get(),
                    'team2_name': quote.css('a.match div.matchTeams div.team2 div.matchTeamName::text').get(),
                    'match_event': quote.css('a.match div.matchEvent div.matchEventName::text').get(),
                    'bo': quote.css('a.match div.matchInfo div.matchMeta::text').get()
                })

        res['upcoming_matches'] = matches
        yield res
