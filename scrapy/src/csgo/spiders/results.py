# // quote=response.css("div.upcomingMatchesSection div.upcomingMatch a div.matchTeams div.team1 div.matchTeamName::text").getall()
# scrapy crawl quotes -O quotes.json
import scrapy
from .. import items
from ..converter import TimestampUnix13ToDateTime


class ResultsMatchesSpider(scrapy.Spider):
    name = "results"

    start_urls = [
        'https://www.hltv.org/results',
    ]

    def parse(self, response):
        res = items.Results()
        matches = []

        for quote in response.css('div.results-sublist div.result-con'):
            match_date = quote.css(
                '::attr(data-zonedgrouping-entry-unix)').get()
            if (match_date is not None):
                matches.append({
                    'url': quote.css('a.a-reset::attr(href)').get(),
                    'match_id': quote.css('a.a-reset::attr(href)').get().split("/")[2],
                    'match_date': TimestampUnix13ToDateTime(match_date),
                })

        res['results'] = matches
        yield res

        # next_page = response.css('div.pagination-bottom a.pagination-next::attr(href)').get()

        # if next_page is not None:
        #     yield response.follow('https://hltv.org{}'.format(next_page), callback=self.parse)
