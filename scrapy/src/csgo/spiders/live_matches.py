# extract live matches
#   get the map
#   post to api
#   analyse
#       post to prod
import scrapy
from .. import items
from scrapy.http import Request
from ..converter import TimestampUnix13ToDateTime

class LiveMatchesSpider(scrapy.Spider):

    def __init__(self, *args, **kwargs):
        super(LiveMatchesSpider, self).__init__(*args, **kwargs)
    name = "live_matches"
    start_urls = [
        'https://www.hltv.org/matches',
        # 'https://www.hltv.org/matches/2346045/lowlandlions-vs-apologis-numberone-season-1',
    ]
    

    # def parse(self, response):
    #     item = dict()

    #     live_matches = []
    #     for live_match in response.xpath('//div[contains(@class,"liveMatchesContainer")]'):
    #         live_matches.append(live_match.xpath('//a[contains(@class, "match") and contains(@class, "a-reset")]/@href').get())

    #     if len(live_matches) > 0:
    #         first_live = live_matches.pop(0)
    #         url = f'https://hltv.org{first_live}'
    #         yield Request(url, callback=self.parse_live_match, meta={'live_matches': live_matches})
        
    def parse(self, response):
        
        # ban_pick = response.xpath('//div[contains(@class, "standard-box") and contains(@class, "veto-box")]/div[@class="padding"]/div/text()').extract()
        res = items.LiveMatches()
        matches = []
        for element in response.xpath('//div[@class="liveMatch-container"]'):
            matches.append({
                'id': element.xpath('@data-scorebot-id').get(),
                'url': element.xpath('//a[contains(@class="match")]/@href').extract_first(),
                'team1_id': element.xpath('@data-team1-id').get(),
                'team2_id': element.xpath('@data-team2-id').get(),
                'maps': element.xpath('@data-maps').get(),

                    # 'id': quote.css('div.liveMatch a.liveMatch::attr(href)').get().split("/")[2],
                    # 'url': quote.css('div.liveMatch a.liveMatch::attr(href)').get(),
                    # # Convert Date-Unix13 to DateTime
                    # # 'match_date': TimestampUnix13ToDateTime(match_date),
                    # 'team1_id': quote.css('::attr(data-team1-id)').get(),
                    # 'team2_id': quote.css('::attr(data-team2-id)').get(),
                    # 'maps': quote.css('::attr(data-maps)').get(),
                    # 'team1_name': quote.css('div.liveMatch a.match div.matchTeams div.matchTeam div.matchTeamName::text').get(),
                    # 'team2_name': quote.css('a.liveMatch div.matchTeams div.team2 div.matchTeamName::text').get(),
                    # 'match_event': quote.css('a.liveMatch div.matchEvent div.matchEventName::text').get(),
                    # 'bo': quote.css('a.match div.matchInfo div.matchMeta::text').get()
            })
        # for quote in response.css('div.liveMatches div.liveMatch-container'):
        #     if quote.css('::attr(team1)').get() and quote.css('::attr(team2)').get():
        #         match_date = quote.css(
        #             '::attr(data-zonedgrouping-entry-unix)').get()

        #         matches.append({
        #             'id': quote.css('div.liveMatch a.liveMatch::attr(href)').get().split("/")[2],
        #             'url': quote.css('div.liveMatch a.liveMatch::attr(href)').get(),
        #             # Convert Date-Unix13 to DateTime
        #             # 'match_date': TimestampUnix13ToDateTime(match_date),
        #             'team1_id': quote.css('::attr(data-team1-id)').get(),
        #             'team2_id': quote.css('::attr(data-team2-id)').get(),
        #             'maps': quote.css('::attr(data-maps)').get(),
        #             'team1_name': quote.css('div.liveMatch a.match div.matchTeams div.matchTeam div.matchTeamName::text').get(),
        #             'team2_name': quote.css('a.liveMatch div.matchTeams div.team2 div.matchTeamName::text').get(),
        #             'match_event': quote.css('a.liveMatch div.matchEvent div.matchEventName::text').get(),
        #             'bo': quote.css('a.match div.matchInfo div.matchMeta::text').get()
        #         })

        res['live_matches'] = matches
        print(res)
        yield res
