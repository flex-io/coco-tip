import scrapy
from .. import items
from scrapy.http import Request
from .. import converter
import re

def parse_round(rounds):
    def select_round_type(round_str):
        switcher = {
            "emptyHistory": "0",
            "ct_win": "1",
            "bomb_defused": "2",
            "t_win": "-1",
            "bomb_exploded": "-2",
            "stopwatch": "3"
        }

        return switcher.get(round_str, "-3")

    temp_rounds = rounds
    for index, tmp in enumerate(temp_rounds):
        temp_rounds[index] = select_round_type(tmp.split("/")[5].split(".")[0])

    st = ",".join(temp_rounds)
    return st
   
def parse_team_map_pick_to_listdict(_list):
    return converter.ListToDict(_list)

class MatchUrlSpider(scrapy.Spider):

    def __init__(self, url=None, *args, **kwargs):
        super(MatchUrlSpider, self).__init__(*args, **kwargs)
        mod_urls = [f'https://hltv.org{s}' for s in url.split(",")]
        self.start_urls = mod_urls

    handle_httpstatus_list = [403, 404]
    allowed_domains = ['hltv.org']
    # custom_settings = {
    #     'LOG_FILE': 'logs/demospider.log',
    #     'LOG_LEVEL': 'DEBUG'
    # }
    name = "match_url"

    def parse(self, response):
        self.url = response.request.url

        item = dict()
        teams_rank = response.xpath('//div[contains(@class, "teamRanking")]/a/text()').extract()

        team1_rank = None
        team2_rank = None

        team1_url = response.css('div.team1-gradient a::attr(href)').get()
        team2_url = response.css('div.team2-gradient a::attr(href)').get()
        team1_id = team1_url.split("/")[2]
        team2_id = team2_url.split("/")[2]
        
        if len(teams_rank) > 0 :
            team1_rank = re.sub('#', '', teams_rank[0]).strip()
            team1_rank = int(team1_rank) if team1_rank.isnumeric() else None
        if len(teams_rank) > 1:
            team2_rank = re.sub('#', '', teams_rank[1]).strip()
            team2_rank = int(team2_rank) if team2_rank.isnumeric() else None

        match_date = response.xpath('//div[contains(@class, "timeAndEvent")]/div[contains(@class, "date")]/@data-unix').get()
        item['match_id'] = response.request.url.split("/")[4]
        item['team1_logo'] = response.xpath('//div[contains(@class, "team1-gradient")]/a/img/@src').get()
        item['team2_logo'] = response.xpath('//div[contains(@class, "team2-gradient")]/a/img/@src').get()
        item['team1_url'] = team1_url
        item['team2_url'] = team2_url
        item['team1_id'] = team1_id
        item['team2_id'] = team2_id
        item['team1_rank'] = team1_rank
        item['team2_rank'] = team2_rank
        item['match_date'] = converter.TimestampUnix13ToDateTime(match_date)
        item['event_url'] = response.xpath('//div[contains(@class, "timeAndEvent")]/div[contains(@class, "event")]/a/@href').get()
        item['ban_pick'] = response.xpath('//div[contains(@class, "standard-box") and contains(@class, "veto-box")]/div[@class="padding"]/div/text()').extract()
        item['maps'] = []
        
        map_urls = response.css('a.results-stats::attr(href)').getall()
        map_count = len(map_urls)
        match_item = items.MatchItem()
        
        team_picks = []
        for match_map in response.xpath('//*[@class="mapholder"]'):
            # map_url = match_map.css('a.results-stats::attr(href)').get()
            map_name = match_map.xpath('div[@class = "played"]/div[contains(@class, "map-name-holder")]/div[contains(@class, "mapname")]/text()').get()

            # results_left_team_name = match_map.xpath(
            #     'div[contains(@class, "results")]'
            #     '/*[contains(@class, "results-left")]'
            #     '/*[contains(@class, "results-teamname-container")]'
            #     '/*[contains(@class, "results-teamname")]/text()'
            # ).get()

            # results_right_team_name = match_map.xpath(
            #     'div[contains(@class, "results")]'
            #     '/*[contains(@class, "results-right")]'
            #     '/*[contains(@class, "results-teamname-container")]'
            #     '/*[contains(@class, "results-teamname")]'
            #     '/text()'
            # ).get()

            results_left_classes = match_map.xpath('div[contains(@class, "results")]/*[contains(@class, "results-left")]/@class').get()
            results_right_classes = match_map.xpath('div[contains(@class, "results")]/*[contains(@class, "results-right")]/@class').get()

            if results_left_classes is None:
                results_left_classes = ""
            if results_right_classes is None:
                results_right_classes = ""

            if results_left_classes.find('pick') != -1:
                pick_by_team_id = team1_id
            elif results_right_classes.find('pick') != -1:
                pick_by_team_id = team2_id
            else:
                pick_by_team_id = None

            team_picks.append({map_name: pick_by_team_id})

        listdict_team_picks = converter.ListToDict(team_picks)

        if map_count > 0:
            map_url = map_urls.pop(0)
            match_item['match'] = item

            url = f'https://hltv.org{map_url}'
            yield Request(url, callback=self.parse_map, meta={'item': match_item, 'map_urls': map_urls, 'map_count': map_count, 'listdict_team_picks': listdict_team_picks})
        else:
            match_item['match'] = item
            yield match_item

       

    def parse_map(self, response):
        match_item = response.meta['item']
        # mapinfo = dict()
        #OVERPASS
        map_name = response.css('div.match-info-box::text').getall()[1]
        map_count = response.meta['map_count']

        map_index = map_count - len(response.meta['map_urls'])

        def get_player_stats(mapstat_id):

            def re_sub(value, default = None):
                try:
                    temp = re.sub('[()]', '', value).strip()
                    return temp
                except:
                    return default

            def if_none_raise_error(key, value):
                if value is None:
                    raise Exception('{}: has None value.{}'.format(key, value))
                else:
                    return value

            player_stats = []
            
            for table in response.xpath('//table[@class="stats-table"]'):
                team_name = table.xpath('thead/tr/th[contains(@class, "st-teamname")]/img/@title').get()
                for part in table.xpath('tbody/tr'):
                    player_url = part.xpath('td[contains(@class, "st-player")]/div[contains(@class, "flag-align")]/a/@href').get()
                    headshot = part.xpath('td[contains(@class, "st-kills")]/span/text()').get()
                    flash_assist = part.xpath('td[contains(@class, "st-assists")]/span/text()').get()
                    kills = part.xpath('td[contains(@class, "st-kills")]/text()').get()
                    assist = part.xpath('td[contains(@class, "st-assists")]/text()').get()
                    deaths = part.xpath('td[contains(@class, "st-deaths")]/text()').get()
                    kdratio = part.xpath('td[contains(@class, "st-kdratio")]/text()').get()
                    kddiff = part.xpath('td[contains(@class, "st-kddiff")]/text()').get()
                    adr = part.xpath('td[contains(@class, "st-adr")]/text()').get()
                    fkdiff = part.xpath('td[contains(@class, "st-fkdiff")]/text()').get()
                    rating = part.xpath('td[contains(@class, "st-rating")]/text()').get()
                    player = {
                        'mapstat_id': mapstat_id,
                        'team_name': team_name,
                        'player_id': player_url.split("/")[3],
                        'player_name': player_url.split("/")[4],
                        "player_url": player_url,
                        "kills": if_none_raise_error('kills', kills),
                        #remove parenthesis
                        "headshot": re_sub(headshot),
                        "assist": if_none_raise_error('assist', assist),
                        "flash_assist": re_sub(flash_assist),
                        "deaths": if_none_raise_error('deaths', deaths),
                        #Percentage of rounds in which the player either had a kill, assist, survived or was traded
                        "kdratio": if_none_raise_error('kdratio', kdratio),
                        #Kills deaths difference
                        "kddiff": if_none_raise_error('kddiff', kddiff),
                        #Average damage per round
                        "adr": if_none_raise_error('adr', adr),
                        #first kill, first death per round
                        "fkdiff": if_none_raise_error('fkdiff', fkdiff),
                        "rating": if_none_raise_error('rating', rating),
                    }
                    player_stats.append(player)

            return player_stats

        
        team_left = response.xpath('//div[@class="match-info-box"]/div[@class="team-left"]/a[contains(@class, "block")]/@href').extract_first()
        team_right = response.xpath('//div[@class="match-info-box"]/div[@class="team-right"]/a[contains(@class, "block")]/@href').extract_first()

        team_left_id = team_left.split("/")[3]
        team_right_id = team_right.split("/")[3]

        team_left_name = response.xpath('//div[@class="match-info-box"]/div[@class="team-left"]/a[contains(@class, "block")]/text()').extract_first()
        team_right_name = response.xpath('//div[@class="match-info-box"]/div[@class="team-right"]/a[contains(@class, "block")]/text()').extract_first()
    
        team = {
            team_left_name: team_left_id,
            team_right_name: team_right_id
        }

        team_left_name = response.xpath('//div[@class="match-info-box"]/div[@class="team-left"]/img[contains(@class, "team-logo")]/@title').extract_first()
        team_left_score = response.xpath('//div[@class="match-info-box"]/div[@class="team-left"]/div[contains(@class, "bold")]/text()').extract_first()

        team_right_name = response.xpath('//div[@class="match-info-box"]/div[@class="team-right"]/img[contains(@class, "team-logo")]/@title').extract_first()
        team_right_score = response.xpath('//div[@class="match-info-box"]/div[@class="team-right"]/div[contains(@class, "bold")]/text()').extract_first()

        # print (match_item['match']['team1_id'], team_left_score, team_right_score)
        dict_score = {
            team_left_name: team_left_score,
            team_right_name: team_right_score
        }
        

        listdict_team_picks = response.meta['listdict_team_picks']
        for sel in response.xpath('//div[@class="round-history-team-row"]'):
            half_rounds = []
            for first in sel.xpath('div[@class="round-history-half"]'):
                half_rounds.append(parse_round(first.xpath('img/@src').extract()))

            mapstat_id = response.request.url.split("/")[6]
            xpath_team_name = sel.xpath('img/@title').get()
            team_name = xpath_team_name

            match_item['match']['maps'].append({
                'match_id': match_item['match']['match_id'],
                'mapstat_id': mapstat_id,
                'pick_by_team_id': listdict_team_picks.get(map_name.strip()),
                'map_round': map_index,
                'mapstat_name': map_name.strip(),
                'team_id': team.get(xpath_team_name),
                'team_name':  team_name,
                'round_history': half_rounds,
                'player_stats': get_player_stats(mapstat_id),
                'url': response.request.url,
                'score': dict_score.get(team_name)
            })
            

        map_urls = response.meta['map_urls']
        
        if not map_urls:  # we crawled all of the agents!
            yield match_item
       

        if len(map_urls) <= 0:
            return
       
        url = map_urls.pop(0)
        yield Request(f'https://hltv.org{url}', callback=self.parse_map, meta={'item': match_item, 'map_urls': map_urls, 'map_count': map_count, 'listdict_team_picks': listdict_team_picks})
        