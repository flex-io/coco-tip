# // quote=response.css("div.upcomingMatchesSection div.upcomingMatch a div.matchTeams div.team1 div.matchTeamName::text").getall()
# scrapy crawl quotes -O quotes.json
import scrapy
import json
from .. import items
from scrapy.utils.serialize import ScrapyJSONEncoder

_encoder = ScrapyJSONEncoder()

class GetHtmlSpider(scrapy.Spider):
    name = "get_html"

    def __init__(self, url=None, *args, **kwargs):
        super(GetHtmlSpider, self).__init__(*args, **kwargs)
        self.url = url
        self.start_urls.append(url)

    def parse(self, response):
        with open(f'{response.request.url.split("/")[-1]}.html', 'wb') as f:
            f.write(response.body)
