import datetime

def TimestampUnix13ToDateTime(param:str = None):
    try:
        ts = datetime.datetime.fromtimestamp(int(param)/1000)
        return ts.strftime("%Y-%m-%d %H:%M:%S")
    except:
        return None

def ListToDict(_list):
    return {k: v for d in _list for k, v in d.items()}

