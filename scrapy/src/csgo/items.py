# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class CsgoItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


class UpcomingMatches(scrapy.Item):
    upcoming_matches = scrapy.Field()

class Results(scrapy.Item):
    results = scrapy.Field()

class MatchItem(scrapy.Item):
    match = scrapy.Field()

class LiveMatches(scrapy.Item):
    live_matches = scrapy.Field()