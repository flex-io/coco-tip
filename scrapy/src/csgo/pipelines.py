# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import os
import json
import requests
from pathlib import Path
from itemadapter import ItemAdapter
from scrapy.utils.serialize import ScrapyJSONEncoder
from .items import Results, UpcomingMatches, MatchItem
from itemadapter import ItemAdapter
# from dotenv import load_dotenv

# load_dotenv()
# load_dotenv(verbose=True)

# PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '../../..'))
# env_path = os.path.join(PROJECT_ROOT, ".env")

# load_dotenv(dotenv_path=env_path)


SCRAPY_API_URL = os.getenv("SCRAPY_API_URL")
AWS_API_URL = os.getenv("AWS_API_URL")
SCRAPY_API_URL = 'http://localhost:8000'
# API_URL = 'http://scrapy_api:8000'
# up one folder
THIS_FOLDER = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '../..'))
MATCHES_OUTPUT_HLTV = os.path.join(THIS_FOLDER, "share/csgo/")

# Fix error when encoding item
_encoder = ScrapyJSONEncoder()

class CsgoPipeline:
    def process_item(self, item, spider):
        return item

# class JsonWriterPipeline:

#     def open_spider(self, spider):
#         Path(MATCHES_OUTPUT_HLTV).mkdir(parents=True, exist_ok=True)
#         MATCHES_HLTV_FILE = f'{MATCHES_OUTPUT_HLTV}/{spider.url.split("/")[-1]}.jl'
#         self.file = open(MATCHES_HLTV_FILE, 'w')

    
#     def close_spider(self, spider):
#         self.file.close()
    
#     def process_item(self, item, spider):
        
#         line = json.dumps(ItemAdapter(item).asdict()) + "\n"
#         self.file.write(line)
#         return item

class ApiPostPipeline:
        
    
    def process_item(self, item, spider):


        collection_name = ''
        if isinstance(item, UpcomingMatches):
            collection_name = 'upcoming_matches'

            url = "{host}/{prefix}/{collection_name}".format(
                host=AWS_API_URL,
                prefix="csgo",
                collection_name=collection_name,
            )

            requests.post(url, headers={'Content-Type':'application/json'}, data=json.dumps(_encoder.encode(item)))

        elif isinstance(item, Results):
            collection_name = 'results'
        elif isinstance(item, MatchItem):
            collection_name = 'match_url'
        
        url = "{host}/{prefix}/{collection_name}".format(
            host=SCRAPY_API_URL,
            prefix="csgo",
            collection_name=collection_name,
        )

        requests.post(url, headers={'Content-Type':'application/json'}, data=json.dumps(_encoder.encode(item)))
 
        return item