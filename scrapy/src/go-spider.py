import time
import pathlib
import json
import requests
from scrapy.utils.serialize import ScrapyJSONEncoder

from twisted.internet import reactor, defer
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from scrapy.utils.project import get_project_settings
from types import SimpleNamespace
from csgo.spiders import match_url


upcoming_matches = requests.get('http://localhost:8000/csgo/results')
# urls = upcoming_matches.json()
urls = upcoming_matches.json()['matches']
print(urls)

match_settings = get_project_settings()

match_settings.update({
    "LOG_ENABLED": True,
    "ROBOTSTXT_OBEY": False,
    "COOKIES_ENABLED": False,
    "DOWNLOADER_MIDDLEWARES": {
        'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
        'scrapy_useragents.downloadermiddlewares.useragents.UserAgentsMiddleware': 500,
    },
    "USER_AGENTS": [
        ('Mozilla/5.0 (X11; Linux x86_64) '
        'AppleWebKit/537.36 (KHTML, like Gecko) '
        'Chrome/57.0.2987.110 '
        'Safari/537.36'),  # chrome
        ('Mozilla/5.0 (X11; Linux x86_64) '
        'AppleWebKit/537.36 (KHTML, like Gecko) '
        'Chrome/61.0.3163.79 '
        'Safari/537.36'),  # chrome
        ('Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) '
        'Gecko/20100101 '
        'Firefox/55.0'),  # firefox
        ('Mozilla/5.0 (X11; Linux x86_64) '
        'AppleWebKit/537.36 (KHTML, like Gecko) '
        'Chrome/61.0.3163.91 '
        'Safari/537.36'),  # chrome
        ('Mozilla/5.0 (X11; Linux x86_64) '
        'AppleWebKit/537.36 (KHTML, like Gecko) '
        'Chrome/62.0.3202.89 '
        'Safari/537.36'),  # chrome
        ('Mozilla/5.0 (X11; Linux x86_64) '
        'AppleWebKit/537.36 (KHTML, like Gecko) '
        'Chrome/63.0.3239.108 '
        'Safari/537.36'),  # chrome
    ],
    "DOWNLOAD_DELAY": 2,
    "ITEM_PIPELINES": {
        # 'csgo.pipelines.JsonWriterPipeline': 300,
        'csgo.pipelines.ApiPostPipeline': 400,
    }
})


@defer.inlineCallbacks
def crawl():

    configure_logging()

    match_runner = CrawlerRunner(match_settings)
    count = 1
    # match_runner.crawl(match_url.MatchUrlSpider, url=url)
    for url in urls:
        print(url)
        yield match_runner.crawl(match_url.MatchUrlSpider, url=url)
        count += 1
        print(count)


    reactor.stop()

crawl()
reactor.run() 