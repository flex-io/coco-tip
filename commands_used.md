# scrapy-api

## Backup your databases

docker exec -t your-db-container pg*dumpall -c -U postgres > dump*`date +%d-%m-%Y"_"%H_%M_%S`.sql

## Restore your databases

cat your_dump.sql | docker exec -i your-db-container psql -U postgres

# fastapi

for a detailed guide on how to deploy a fastapi application with AWS API Gateway and AWS Lambda

## building a package

python3 setup.py sdist bdist_wheel

## run to docker

docker build -t example_app_image .
docker run -p 8080:8080 --name example-app-container example_app_image
arn:aws:iam::315847217724:role/fastapilambdarole

## deploy to AWS Lambda

sam validate
sam build --use-container --debug

sam package --s3-bucket coco-deployment-bucket --output-template-file out.yml --region ap-southeast-2
sam deploy --template-file out.yml --stack-name coco-stack-name --region ap-southeast-2 --no-fail-on-empty-changeset --capabilities CAPABILITY_IAM

# Scrapyd

https://hub.docker.com/r/vimagick/scrapyd

docker-compose logs -f scrapyd
