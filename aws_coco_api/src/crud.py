from sqlalchemy import MetaData
from sqlalchemy.orm import Session
from sqlalchemy.dialects.postgresql import insert
from . import models, schemas


def get_point_logs(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.PointEvent).offset(skip).limit(limit).all()


def get_points_by_player(db: Session, player: str):
    return db.query(models.PointEvent).filter(models.PointEvent.player == player).all()


def get_points_by_team(db: Session, team: str):
    return db.query(models.PointEvent).filter(models.PointEvent.team == team).all()


def get_points_by_season(db: Session, season: str):
    return db.query(models.PointEvent).filter(models.PointEvent.season == season).all()


def get_points_by_team_and_season(db: Session, team: str, season: str):
    return (
        db.query(models.PointEvent)
        .filter(models.PointEvent.team == team, models.PointEvent.season == season)
        .all()
    )


def get_points_by_player_and_season(db: Session, player: str, season: str):
    return (
        db.query(models.PointEvent)
        .filter(models.PointEvent.player == player, models.PointEvent.season == season)
        .all()
    )

def upsert_instance(db: Session, model, rows, no_update_cols=[]):
    
    # reflect db schema to MetaData
    metadata = MetaData()
    metadata.reflect(db.get_bind())

    table = metadata.tables[model.__tablename__] 

    stmt = insert(table).values(rows)

    update_cols = [c.name for c in table.c
                   if c not in list(table.primary_key.columns)
                   and c.name not in no_update_cols]
    
 
    on_conflict_stmt = stmt.on_conflict_do_update(
        index_elements=table.primary_key.columns,
        set_={k: getattr(stmt.excluded, k) for k in update_cols}
        )
        
    db.execute(on_conflict_stmt)
    db.commit()

def truncate_table(db: Session, model):
    try:
        db.execute(f'TRUNCATE TABLE {model.__tablename__} RESTART IDENTITY;')
        # db.session.query(model).delete()
        db.commit()
    except:
        db.rollback()