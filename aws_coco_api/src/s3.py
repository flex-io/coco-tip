import json
import boto3
import botocore
from botocore.exceptions import NoCredentialsError
from fastapi import HTTPException

ACCESS_KEY = 'AKIAUTCPWCI6IEHUXLVS'
SECRET_KEY = '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'

path_to_json = 'csgo/upcoming_match/'
bucket = 'coco-deployment-bucket'
s3_client = boto3.client('s3')

# def upload_file(file_name, bucket, object_name=None):
#     """Upload a file to an S3 bucket

#     :param file_name: File to upload
#     :param bucket: Bucket to upload to
#     :param object_name: S3 object name. If not specified then file_name is used
#     :return: True if file was uploaded, else False
#     """

#     # If S3 object_name was not specified, use file_name
#     if object_name is None:
#         object_name = file_name

#     # Upload the file
#     s3_client = boto3.client('s3')
#     myData = {'firstName':'Saravanan','lastName':'Subramanian','title':'Manager', 'empId':'007'}
#     serializedMyData = json.dumps(myData)
#     try:
#         # response = s3_client.upload_file(file_name, bucket, object_name)
#         response = s3.put_object(Bucket=bucket,Key='EmpId007')
#     except ClientError as e:
#         logging.error(e)
#         return False
#     return True

def put_json_to_s3(data, key):
    try:
        # response = s3_client.upload_file(file_name, bucket, object_name)
        serializedJson = json.dumps(data)
        filename = path_to_json + key
        response = s3_client.put_object(Body=serializedJson, Bucket=bucket,Key=filename)
        return filename
    except botocore.exceptions.ClientError as err:
        logging.error(err)
        raise HTTPException(status_code=500, detail=str(ValueError(err)))
    
def local_put_json_to_s3(data, key):
    s3 = boto3.client('s3', aws_access_key_id=ACCESS_KEY,
                      aws_secret_access_key=SECRET_KEY)

    try:
        serializedJson = json.dumps(data)
        filename = path_to_json + key

        s3.put_object(Body=serializedJson, Bucket=bucket,Key=filename)
        return True
    except FileNotFoundError:
        raise HTTPException(status_code=404, detail="The file was not found")
    except NoCredentialsError:
        raise HTTPException(status_code=500, detail="Credentials not available")

def delete_file(key):
    s3 = boto3.client('s3', aws_access_key_id=ACCESS_KEY,
                      aws_secret_access_key=SECRET_KEY)

    try:
        s3.delete_object(Bucket=bucket, Key=key)
        return True
    except FileNotFoundError:
        raise HTTPException(status_code=404, detail="The file was not found")
    except NoCredentialsError:
        raise HTTPException(status_code=500, detail="Credentials not available")

def read_csgo_upcoming_matches(key):

    try:
        filename = path_to_json + key

        s3 = boto3.client('s3', aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY)
        
        if(_key_existing_size__list(s3, bucket, filename) is None):
            raise HTTPException(status_code=404, detail="Item not found: {}".format(filename))
        
        obj = s3.get_object(Bucket=bucket, Key=filename)
        j = json.loads(obj['Body'].read().decode('utf-8'))
        return j

    except botocore.exceptions.ClientError as err:
        raise HTTPException(status_code=500, detail=str(ValueError(err)))
        


def _key_existing_size__list(client, bucket, key):
    """return the key's size if it exist, else None"""
    response = client.list_objects_v2(
        Bucket=bucket,
        Prefix=key,
    )
    for obj in response.get('Contents', []):
        if obj['Key'] == key:
            return obj['Size']