from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date, Float, DateTime, func
from sqlalchemy.orm import relationship

from .database import Base


class PointEvent(Base):
    __tablename__ = "point_log"

    id = Column(Integer, primary_key=True, index=True)
    player = Column(String)
    team = Column(String)
    season = Column(String)
    data_date = Column(Date)
    points = Column(Float)

class UpcomingMatches(Base):
    __tablename__ = "csgo_upcoming_matches"
    id = Column(Integer, primary_key=True, index=True)
    url = Column(String)
    match_date = Column(DateTime)
    team1_id = Column(Integer)
    team2_id = Column(Integer)
    team1_name = Column(String)
    team2_name = Column(String)
    match_event = Column(String)
    bo = Column(String)
    s3_url = Column(String)

class PostedUpcomingMatches(Base):
    __tablename__ = "csgo_posted_upcoming_matches"
    id = Column(Integer, primary_key=True, index=True)
    s3_url = Column(String)
    created_at = Column(DateTime(timezone=True), server_default=func.now())