from fastapi import FastAPI
from .routers import players, teams, seasons, csgo_matches, games
from fastapi.middleware.cors import CORSMiddleware

from mangum import Mangum

app = FastAPI(
    title="FastAPI-PostgreSQL-AWS-Lambda",
    openapi_prefix="/prod"
)

origins = ['*']

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



app.include_router(players.router, tags=["Players"])
app.include_router(teams.router, tags=["Teams"])
app.include_router(seasons.router, tags=["Seasons"])
app.include_router(csgo_matches.router, tags=["csgo"])
app.include_router(games.router, tags=["games"])
handler = Mangum(app, enable_lifespan=False)

app.mount("/prod", app)