import json
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from ..database import SessionLocal
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse

router = APIRouter(
    prefix="/games",
    tags=["games"],
    responses={404: {"description": "Not found"}},
)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.get("/")
def fetch_games(db: Session = Depends(get_db)):
    try:
        res = db.execute("SELECT * FROM fn_fetch_games()")

        json_obj = json.loads(
            json.dumps([dict(r) for r in res], default=jsonable_encoder)
        )

        return JSONResponse(content=jsonable_encoder(json_obj))
    except Exception as err:
        raise HTTPException(status_code=500, detail=str(err))