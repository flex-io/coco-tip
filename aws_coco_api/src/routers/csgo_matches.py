import json
from fastapi import APIRouter, Depends, Body, HTTPException, Request
from sqlalchemy.orm import Session
from .. import crud, models, s3
from ..database import SessionLocal, engine
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse

router = APIRouter(
    prefix="/csgo",
    tags=["csgo"],
    responses={404: {"description": "Not found"}},
)

models.Base.metadata.create_all(bind=engine)

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.get("/for_processing")
def get_for_processing(db: Session = Depends(get_db)):
    try:
        posted_upcoming_matches = db.query(models.PostedUpcomingMatches).all()
        upcoming_matches = (
            db.query(models.UpcomingMatches)
            .filter(
                models.UpcomingMatches.id.notin_(
                    [record.id for record in posted_upcoming_matches]
                )
            )
            .all()
        )
        return JSONResponse(content=jsonable_encoder(upcoming_matches))

    except Exception as err:
        raise HTTPException(status_code=500, detail=str(err))


@router.post("/delete_posted_match")
def post_delete_posted_match(match_id: int, db: Session = Depends(get_db)):
    try:

        qry = db.query(models.PostedUpcomingMatches).filter_by(id = match_id)

        record = qry.first()
        if record is not None:
            is_deleted = s3.delete_file(key=record.s3_url)
            if (is_deleted):
                qry.delete()
            db.commit()
        else:
            raise HTTPException(status_code=404, detail="Record not found")
    except Exception as err:
        db.rollback()
        raise HTTPException(status_code=404, detail=str(err))
    

@router.get("/cleanup")
def post_cleanup(db: Session = Depends(get_db)):
    try:
        upcoming_matches = db.query(models.UpcomingMatches).all()
        posted_upcoming_matches = (
            db.query(models.PostedUpcomingMatches)
            .filter(
                models.PostedUpcomingMatches.id.notin_(
                    [record.id for record in upcoming_matches]
                )
            )
            .all()
        )

        for record in posted_upcoming_matches:
            is_deleted = s3.delete_file(key=record.s3_url)
            if is_deleted:
                db.query(models.PostedUpcomingMatches).filter(
                    models.PostedUpcomingMatches.id == record.id
                ).delete()

        db.commit()
        return JSONResponse(content=jsonable_encoder(posted_upcoming_matches))
    except Exception as err:
        db.rollback()
        raise HTTPException(status_code=500, detail=str(err))


@router.get("/posted_upcoming_matches")
def get_posted_upcoming_matches(db: Session = Depends(get_db)):
    try:
        res = db.query(models.PostedUpcomingMatches).all()

        return JSONResponse(content=jsonable_encoder(res))
    except Exception as err:
        raise HTTPException(status_code=500, detail=str(err))


@router.post("/upcoming_matches")
def post_upcoming_match(body=Body(...), db: Session = Depends(get_db)):
    try:
        json_obj = json.loads(body)

        # TRUNCATE TABLE BEFORE INSERTING RECORDS
        crud.truncate_table(db, models.UpcomingMatches)

        crud.upsert_instance(
            db, models.UpcomingMatches, json_obj["upcoming_matches"], []
        )
    except Exception as err:
        raise HTTPException(status_code=500, detail=str(err))


@router.get("/matches")
def get_upcoming_match(db: Session = Depends(get_db)):
    try:
        res = db.query(models.UpcomingMatches).all()
        # db.session.query(Results).filter(Results.is_process == False).all()
        resp = {"matches": jsonable_encoder(res)}
        # json_compatible_item_data = jsonable_encoder(res)
        return JSONResponse(content=resp)
    except Exception as err:
        raise HTTPException(status_code=500, detail=str(err))


@router.get("/match/{match_id}")
def get_matches_by_id(match_id: int):
    return s3.read_csgo_upcoming_matches(str(match_id))


@router.post("/match/{match_id}")
async def post_match(match_id: int, request: Request, db: Session = Depends(get_db)):
    body = await request.body()
    json_body = json.loads(body)

    s3_filename = s3.put_json_to_s3(json_body, str(match_id))

    if s3_filename is not None:
        r = {"id": match_id, "s3_url": s3_filename}
        r = json.dumps(r)
        loaded_r = json.loads(r)
        crud.upsert_instance(db, models.PostedUpcomingMatches, loaded_r, [])

    return JSONResponse(content={"s3_filename": s3_filename})
