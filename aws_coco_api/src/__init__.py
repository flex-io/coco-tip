# settings.py
import os
from dotenv import load_dotenv

load_dotenv()

# OR, the same with increased verbosity
load_dotenv(verbose=True)

# OR, explicitly providing path to '.env'
from pathlib import Path  # Python 3.6+ only

# BASE_DIR = os.path.dirname(os.path.abspath(__file__))
# env_path = os.path.join(BASE_DIR, ".env")
PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '../..'))
env_path = os.path.join(PROJECT_ROOT, ".env")

load_dotenv(dotenv_path=env_path)
