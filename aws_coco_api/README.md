# Check

**init**.py

# fastapi-aws-lambda-example

for a detailed guide on how to deploy a fastapi application with AWS API Gateway and AWS Lambda

# building a package

python3 setup.py sdist bdist_wheel

# run to docker

docker build -t example_app_image .
docker run -p 8080:8080 --name example-app-container example_app_image
arn:aws:iam::315847217724:role/fastapilambdarole

# deploy to AWS Lambda

Uncomment AWS Database Credentials in src/database.py
Uncomment openapi_prefix="/prod" in src/main.py

sam validate
sam build --use-container --debug

sam package --s3-bucket coco-deployment-bucket --output-template-file out.yml --region ap-southeast-2
sam deploy --template-file out.yml --stack-name coco-stack-name --region ap-southeast-2 --no-fail-on-empty-changeset --capabilities CAPABILITY_IAM

# Run to Local

Uncomment Local Database Credentials in src/database.py
Comment openapi_prefix="/prod" in src/main.py

uvicorn src.main:app --host 0.0.0.0 --port 8080 --reload
