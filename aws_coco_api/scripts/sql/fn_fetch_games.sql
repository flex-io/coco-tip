DROP FUNCTION IF EXISTS fn_fetch_games();
CREATE OR REPLACE FUNCTION fn_fetch_games () RETURNS TABLE (
        id INT,
        match_date TIMESTAMP WITHOUT TIME ZONE,
        team1_name VARCHAR,
        team2_name VARCHAR,
        title TEXT,
        sub_title VARCHAR,
        is_live BOOLEAN,
        league VARCHAR,
        team1_id INT,
        team2_id INT,
        league_id INT,
        game_type INT
    ) LANGUAGE plpgsql AS $$
DECLARE local_interval TIMESTAMP := NOW() + (3 || ' days')::INTERVAL;
BEGIN RETURN query
SELECT *
FROM (
        SELECT um.id,
            um.match_date AT TIME ZONE 'UTC' AT TIME ZONE 'AEDT' as match_date,
            um.team1_name,
            um.team2_name,
            CONCAT(um.team1_name, ' vs ', um.team2_name) as title,
            um.match_event as sub_title,
            False as is_live,
            um.match_event as league,
            um.team1_id,
            um.team2_id,
            COALESCE (NULL, -1) as league_id,
            1 as game_type
        FROM csgo_upcoming_matches as um
    ) tmp
WHERE tmp.match_date BETWEEN NOW() AND local_interval;
END;
$$