cd aws_coco_api
sam validate
sam build --use-container --debug

sam package --s3-bucket coco-deployment-bucket --output-template-file out.yml --region ap-southeast-2
sam deploy --template-file out.yml --stack-name coco-stack-name --region ap-southeast-2 --no-fail-on-empty-changeset --capabilities CAPABILITY_IAM