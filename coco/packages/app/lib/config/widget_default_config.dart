import 'package:flutter/material.dart';

class TextHeaderData {
  AlignmentGeometry? alignment = AlignmentDirectional.centerStart;
  EdgeInsetsGeometry? padding = EdgeInsets.all(5);

  TextHeaderData();
}

class WidgetDefault {
  static EdgeInsetsGeometry paddingAll = EdgeInsets.all(5);
  static TextHeaderData get textHeader => TextHeaderData();
}
