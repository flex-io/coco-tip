import 'dart:convert';

// ignore: import_of_legacy_library_into_null_safe
import 'package:app/models/models.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:http/http.dart' as http;

class GameRepo {
  // static const baseUrl = 'http://10.1.1.24:8000';
  static const baseUrl = 'https://e5grmu5o47.execute-api.ap-southeast-2.amazonaws.com/prod';

  const GameRepo();

  Future<Game> fetchGameDetail(int match_id) async {
    final url = '$baseUrl/csgo/match/$match_id';
    final resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw Exception('Error getting matches');
    }
    final data = jsonDecode(resp.body);
    Game _game = Game.fromJson(data['game_detail']);
    return _game;
  }
}
