import 'dart:convert';

// ignore: import_of_legacy_library_into_null_safe
import 'package:http/http.dart' as http;
import 'package:app/models/models.dart';

class MatchesRepo {
  // static const baseUrl = 'http://10.1.1.24:8000';
  static const baseUrl = 'https://e5grmu5o47.execute-api.ap-southeast-2.amazonaws.com/prod';

  const MatchesRepo();

  Future<List<Match>> fetchMatchList() async {
    final url = '$baseUrl/csgo/matches';
    final resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw Exception('Error getting matches');
    }
    final t = jsonDecode(resp.body)['matches'] as List;
    return t.map((e) => Match.fromJson(e)).toList();
  }
}
