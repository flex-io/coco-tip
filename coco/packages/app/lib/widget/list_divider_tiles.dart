import 'package:app/config/config.dart';
import 'package:flutter/material.dart';

class ListDivideTiles extends StatelessWidget {
  final Widget? header;
  final Iterable<Widget> children;

  const ListDivideTiles({required this.children, this.header});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        header ?? SizedBox.shrink(),
        Container(
          color: Theme.of(context).primaryColor,
          padding: WidgetDefault.paddingAll,
          child: Column(
            children: divideTiles(
              context: context,
              tiles: children,
            ).toList(),
          ),
        )
      ],
    );
  }

  static Iterable<Widget> divideTiles({
    required BuildContext context,
    required Iterable<Widget> tiles,
  }) sync* {
    final Iterator<Widget> iterator = tiles.iterator;
    final bool isNotEmpty = iterator.moveNext();

    final Decoration decoration = BoxDecoration(
      border: Border(
        bottom: Divider.createBorderSide(
          context,
          color: Theme.of(context).dividerColor,
        ),
      ),
    );

    Widget tile = iterator.current;
    while (iterator.moveNext()) {
      yield DecoratedBox(
        position: DecorationPosition.foreground,
        decoration: decoration,
        child: tile,
      );
      tile = iterator.current;
    }
    if (isNotEmpty) yield tile;
  }
}
