import 'package:app/widget/_config.dart';
import 'package:flutter/material.dart';
import 'package:functional_widget_annotation/functional_widget_annotation.dart';

part 'function.g.dart';

@swidget
Widget error(String err) => Text(err);

@swidget
Widget orElse() => Text('Developer is lazy to create this widget.');

@swidget
Widget loading() => Center(
      child: CircularProgressIndicator(),
    );

@swidget
Widget textHeader(String data, {AlignmentGeometry? alignment}) => Container(
      alignment: alignment ?? WidgetConfig.textHeader.alignment,
      padding: WidgetConfig.paddingAll,
      child: Text(data),
    );
