// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'function.dart';

// **************************************************************************
// FunctionalWidgetGenerator
// **************************************************************************

class Error extends StatelessWidget {
  const Error(this.err, {Key? key}) : super(key: key);

  final String err;

  @override
  Widget build(BuildContext _context) => error(err);
}

class OrElse extends StatelessWidget {
  const OrElse({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext _context) => orElse();
}

class Loading extends StatelessWidget {
  const Loading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext _context) => loading();
}

class TextHeader extends StatelessWidget {
  const TextHeader(this.data, {Key? key, this.alignment}) : super(key: key);

  final String data;

  final AlignmentGeometry? alignment;

  @override
  Widget build(BuildContext _context) => textHeader(data, alignment: alignment);
}
