part of 'matches_cubit.dart';

@freezed
class MatchesState with _$MatchesState {
  const factory MatchesState.loading() = _Loading;
  const factory MatchesState.loaded(List<Match> matches) = _Loaded;
  const factory MatchesState.error(String error) = _Error;
}
