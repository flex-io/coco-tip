import 'package:app/repos/repos.dart';
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:app/models/models.dart';

part 'matches_state.dart';

part 'matches_cubit.freezed.dart';

class MatchesCubit extends Cubit<MatchesState> {
  final MatchesRepo repository;

  MatchesCubit({required this.repository}) : super(MatchesState.loading());

  Future<void> loadMatches() async {
    try {
      emit(MatchesState.loading());
      final List<Match> matches = await repository.fetchMatchList();
      emit(MatchesState.loaded(matches));
    } catch (e) {
      emit(MatchesState.error(e.toString()));
    }
  }
}
