part of 'game.dart';

@freezed
class GameStatPistolState with _$GameStatPistolState {
  const factory GameStatPistolState.loading() = _GameStatPistolInit;

  const factory GameStatPistolState.loaded({
    required FGameStatPistol stat,
    required String mapstat_name,
  }) = _GameStatPistolLoaded;

  const factory GameStatPistolState.error({required String err}) = _GameStatPistolLoadInProgress;
}

class GameStatPistolCubit extends Cubit<GameStatPistolState> {
  final GameCubit gameCubit;
  StreamSubscription? gameSubscription;

  GameStatPistolCubit({required this.gameCubit})
      : super(gameCubit.state.maybeWhen(
            loaded: (data) => GameStatPistolState.loaded(
                stat: FGameStatPistol(
                  team1: GameStatPistol(
                    team_name: data.team1_name,
                    details: data.team1_details,
                  ),
                  team2: GameStatPistol(
                    team_name: data.team2_name,
                    details: data.team2_details,
                  ),
                  h2h: data.h2h,
                  sameOpponent: data.same_opponent,
                ),
                mapstat_name: 'All'),
            orElse: () => GameStatPistolState.loading())) {
    gameSubscription = gameCubit.listen((state) {
      state.maybeWhen(
          loaded: (data) => emit(GameStatPistolState.loaded(
              stat: FGameStatPistol(
                team1: GameStatPistol(
                  team_name: data.team1_name,
                  details: data.team1_details,
                ),
                team2: GameStatPistol(
                  team_name: data.team2_name,
                  details: data.team2_details,
                ),
                h2h: data.h2h,
                sameOpponent: data.same_opponent,
              ),
              mapstat_name: 'All')),
          orElse: () {
            emit(GameStatPistolState.loading());
          });
    });
  }

  Future<void> filterStat(String mapstat_name) async {
    try {
      gameCubit.state.maybeWhen(
          loaded: (data) {
            final List<GameDetail>? team1 = data.team1_details
                ?.where((element) => element.mapstat_name == mapstat_name || mapstat_name == 'All')
                .toList();
            final List<GameDetail>? team2 = data.team2_details
                ?.where((element) => element.mapstat_name == mapstat_name || mapstat_name == 'All')
                .toList();
            final List<GameDetail>? h2h = data.h2h
                ?.where((element) => element.mapstat_name == mapstat_name || mapstat_name == 'All')
                .toList();
            final List<GameDetail>? sameOpponent = data.same_opponent
                ?.where((element) => element.mapstat_name == mapstat_name || mapstat_name == 'All')
                .toList();
            // GameStatPistol.fromData(team1: team1, team2: team2);

            emit(GameStatPistolState.loaded(
                stat: FGameStatPistol(
                  team1: GameStatPistol(
                    team_name: data.team1_name,
                    details: team1,
                  ),
                  team2: GameStatPistol(
                    team_name: data.team2_name,
                    details: team2,
                  ),
                  h2h: h2h,
                  sameOpponent: sameOpponent,
                ),
                mapstat_name: mapstat_name));
          },
          orElse: () => emit(GameStatPistolState.loading()));
    } catch (e) {
      emit(GameStatPistolState.error(err: e.toString()));
    }
  }

  @override
  Future<void> close() {
    gameSubscription?.cancel();
    return super.close();
  }
}
