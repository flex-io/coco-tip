part of 'game.dart';

@freezed
class GameState with _$GameState {
  const factory GameState.loading() = _Loading;

  const factory GameState.loaded({required Game game}) = _Loaded;

  const factory GameState.error(String msg) = _Error;
}

class GameCubit extends Cubit<GameState> {
  GameCubit() : super(GameState.loading());

  Future<void> loadWithSample(int id) async {
    try {
      emit(GameState.loading());

      final Game _game = await GameRepo().fetchGameDetail(id);

      emit(GameState.loaded(game: _game));
    } catch (e) {
      emit(
        GameState.error(e.toString()),
      );
    }
  }
}
