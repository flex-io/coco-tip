// part of 'game.dart';

// @freezed
// class GameStatSeriesState with _$GameStatSeriesState {
//   const factory GameStatSeriesState.initial() = _SeriesInitial;
//   const factory GameStatSeriesState.loaded(GameStat gameStat) = _SeriesLoaded;
//   const factory GameStatSeriesState.error() = _SeriesError;
// }

// class GameStatSeriesCubit extends Cubit<GameStatSeriesState> {
//   final GameCubit gameCubit;
//   StreamSubscription? gameSubscription;

//   GameStatSeriesCubit({required this.gameCubit})
//       : super(gameCubit.state.maybeWhen(
//             loaded: (data) => GameStatSeriesState.loaded(
//                   GameStat.series(
//                     team1Series: data.team1_details?.groupBy(
//                         (element) => element.match_id.toString(),
//                         valueTransform: (item) => item),
//                     team2Series: data.team1_details?.groupBy(
//                         (element) => element.match_id.toString(),
//                         valueTransform: (item) => item),
//                     h2h: data.h2h?.groupBy((element) => element.match_id.toString(),
//                         valueTransform: (item) => item),
//                     sameOpponent: data.same_opponent?.groupBy(
//                         (element) => element.match_id.toString(),
//                         valueTransform: (item) => item),
//                   ),
//                 ),
//             orElse: () => GameStatSeriesState.error())) {
//     gameSubscription = gameCubit.listen((state) {
//       state.maybeWhen(orElse: () => {});
//     });
//   }

//   @override
//   Future<void> close() {
//     gameSubscription?.cancel();
//     return super.close();
//   }
// }
