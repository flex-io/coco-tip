// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'game.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$GameStateTearOff {
  const _$GameStateTearOff();

  _Loading loading() {
    return const _Loading();
  }

  _Loaded loaded({required Game game}) {
    return _Loaded(
      game: game,
    );
  }

  _Error error(String msg) {
    return _Error(
      msg,
    );
  }
}

/// @nodoc
const $GameState = _$GameStateTearOff();

/// @nodoc
mixin _$GameState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(Game game) loaded,
    required TResult Function(String msg) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(Game game)? loaded,
    TResult Function(String msg)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GameStateCopyWith<$Res> {
  factory $GameStateCopyWith(GameState value, $Res Function(GameState) then) =
      _$GameStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$GameStateCopyWithImpl<$Res> implements $GameStateCopyWith<$Res> {
  _$GameStateCopyWithImpl(this._value, this._then);

  final GameState _value;
  // ignore: unused_field
  final $Res Function(GameState) _then;
}

/// @nodoc
abstract class _$LoadingCopyWith<$Res> {
  factory _$LoadingCopyWith(_Loading value, $Res Function(_Loading) then) =
      __$LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadingCopyWithImpl<$Res> extends _$GameStateCopyWithImpl<$Res>
    implements _$LoadingCopyWith<$Res> {
  __$LoadingCopyWithImpl(_Loading _value, $Res Function(_Loading) _then)
      : super(_value, (v) => _then(v as _Loading));

  @override
  _Loading get _value => super._value as _Loading;
}

/// @nodoc
class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'GameState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(Game game) loaded,
    required TResult Function(String msg) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(Game game)? loaded,
    TResult Function(String msg)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Error value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements GameState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$LoadedCopyWith<$Res> {
  factory _$LoadedCopyWith(_Loaded value, $Res Function(_Loaded) then) =
      __$LoadedCopyWithImpl<$Res>;
  $Res call({Game game});

  $GameCopyWith<$Res> get game;
}

/// @nodoc
class __$LoadedCopyWithImpl<$Res> extends _$GameStateCopyWithImpl<$Res>
    implements _$LoadedCopyWith<$Res> {
  __$LoadedCopyWithImpl(_Loaded _value, $Res Function(_Loaded) _then)
      : super(_value, (v) => _then(v as _Loaded));

  @override
  _Loaded get _value => super._value as _Loaded;

  @override
  $Res call({
    Object? game = freezed,
  }) {
    return _then(_Loaded(
      game: game == freezed
          ? _value.game
          : game // ignore: cast_nullable_to_non_nullable
              as Game,
    ));
  }

  @override
  $GameCopyWith<$Res> get game {
    return $GameCopyWith<$Res>(_value.game, (value) {
      return _then(_value.copyWith(game: value));
    });
  }
}

/// @nodoc
class _$_Loaded implements _Loaded {
  const _$_Loaded({required this.game});

  @override
  final Game game;

  @override
  String toString() {
    return 'GameState.loaded(game: $game)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Loaded &&
            (identical(other.game, game) ||
                const DeepCollectionEquality().equals(other.game, game)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(game);

  @JsonKey(ignore: true)
  @override
  _$LoadedCopyWith<_Loaded> get copyWith =>
      __$LoadedCopyWithImpl<_Loaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(Game game) loaded,
    required TResult Function(String msg) error,
  }) {
    return loaded(game);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(Game game)? loaded,
    TResult Function(String msg)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(game);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Error value) error,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class _Loaded implements GameState {
  const factory _Loaded({required Game game}) = _$_Loaded;

  Game get game => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$LoadedCopyWith<_Loaded> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$ErrorCopyWith<$Res> {
  factory _$ErrorCopyWith(_Error value, $Res Function(_Error) then) =
      __$ErrorCopyWithImpl<$Res>;
  $Res call({String msg});
}

/// @nodoc
class __$ErrorCopyWithImpl<$Res> extends _$GameStateCopyWithImpl<$Res>
    implements _$ErrorCopyWith<$Res> {
  __$ErrorCopyWithImpl(_Error _value, $Res Function(_Error) _then)
      : super(_value, (v) => _then(v as _Error));

  @override
  _Error get _value => super._value as _Error;

  @override
  $Res call({
    Object? msg = freezed,
  }) {
    return _then(_Error(
      msg == freezed
          ? _value.msg
          : msg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
class _$_Error implements _Error {
  const _$_Error(this.msg);

  @override
  final String msg;

  @override
  String toString() {
    return 'GameState.error(msg: $msg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Error &&
            (identical(other.msg, msg) ||
                const DeepCollectionEquality().equals(other.msg, msg)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(msg);

  @JsonKey(ignore: true)
  @override
  _$ErrorCopyWith<_Error> get copyWith =>
      __$ErrorCopyWithImpl<_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(Game game) loaded,
    required TResult Function(String msg) error,
  }) {
    return error(msg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(Game game)? loaded,
    TResult Function(String msg)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(msg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements GameState {
  const factory _Error(String msg) = _$_Error;

  String get msg => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$ErrorCopyWith<_Error> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
class _$GameStatPistolStateTearOff {
  const _$GameStatPistolStateTearOff();

  _GameStatPistolInit loading() {
    return const _GameStatPistolInit();
  }

  _GameStatPistolLoaded loaded(
      {required FGameStatPistol stat, required String mapstat_name}) {
    return _GameStatPistolLoaded(
      stat: stat,
      mapstat_name: mapstat_name,
    );
  }

  _GameStatPistolLoadInProgress error({required String err}) {
    return _GameStatPistolLoadInProgress(
      err: err,
    );
  }
}

/// @nodoc
const $GameStatPistolState = _$GameStatPistolStateTearOff();

/// @nodoc
mixin _$GameStatPistolState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(FGameStatPistol stat, String mapstat_name) loaded,
    required TResult Function(String err) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(FGameStatPistol stat, String mapstat_name)? loaded,
    TResult Function(String err)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GameStatPistolInit value) loading,
    required TResult Function(_GameStatPistolLoaded value) loaded,
    required TResult Function(_GameStatPistolLoadInProgress value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GameStatPistolInit value)? loading,
    TResult Function(_GameStatPistolLoaded value)? loaded,
    TResult Function(_GameStatPistolLoadInProgress value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GameStatPistolStateCopyWith<$Res> {
  factory $GameStatPistolStateCopyWith(
          GameStatPistolState value, $Res Function(GameStatPistolState) then) =
      _$GameStatPistolStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$GameStatPistolStateCopyWithImpl<$Res>
    implements $GameStatPistolStateCopyWith<$Res> {
  _$GameStatPistolStateCopyWithImpl(this._value, this._then);

  final GameStatPistolState _value;
  // ignore: unused_field
  final $Res Function(GameStatPistolState) _then;
}

/// @nodoc
abstract class _$GameStatPistolInitCopyWith<$Res> {
  factory _$GameStatPistolInitCopyWith(
          _GameStatPistolInit value, $Res Function(_GameStatPistolInit) then) =
      __$GameStatPistolInitCopyWithImpl<$Res>;
}

/// @nodoc
class __$GameStatPistolInitCopyWithImpl<$Res>
    extends _$GameStatPistolStateCopyWithImpl<$Res>
    implements _$GameStatPistolInitCopyWith<$Res> {
  __$GameStatPistolInitCopyWithImpl(
      _GameStatPistolInit _value, $Res Function(_GameStatPistolInit) _then)
      : super(_value, (v) => _then(v as _GameStatPistolInit));

  @override
  _GameStatPistolInit get _value => super._value as _GameStatPistolInit;
}

/// @nodoc
class _$_GameStatPistolInit implements _GameStatPistolInit {
  const _$_GameStatPistolInit();

  @override
  String toString() {
    return 'GameStatPistolState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _GameStatPistolInit);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(FGameStatPistol stat, String mapstat_name) loaded,
    required TResult Function(String err) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(FGameStatPistol stat, String mapstat_name)? loaded,
    TResult Function(String err)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GameStatPistolInit value) loading,
    required TResult Function(_GameStatPistolLoaded value) loaded,
    required TResult Function(_GameStatPistolLoadInProgress value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GameStatPistolInit value)? loading,
    TResult Function(_GameStatPistolLoaded value)? loaded,
    TResult Function(_GameStatPistolLoadInProgress value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _GameStatPistolInit implements GameStatPistolState {
  const factory _GameStatPistolInit() = _$_GameStatPistolInit;
}

/// @nodoc
abstract class _$GameStatPistolLoadedCopyWith<$Res> {
  factory _$GameStatPistolLoadedCopyWith(_GameStatPistolLoaded value,
          $Res Function(_GameStatPistolLoaded) then) =
      __$GameStatPistolLoadedCopyWithImpl<$Res>;
  $Res call({FGameStatPistol stat, String mapstat_name});
}

/// @nodoc
class __$GameStatPistolLoadedCopyWithImpl<$Res>
    extends _$GameStatPistolStateCopyWithImpl<$Res>
    implements _$GameStatPistolLoadedCopyWith<$Res> {
  __$GameStatPistolLoadedCopyWithImpl(
      _GameStatPistolLoaded _value, $Res Function(_GameStatPistolLoaded) _then)
      : super(_value, (v) => _then(v as _GameStatPistolLoaded));

  @override
  _GameStatPistolLoaded get _value => super._value as _GameStatPistolLoaded;

  @override
  $Res call({
    Object? stat = freezed,
    Object? mapstat_name = freezed,
  }) {
    return _then(_GameStatPistolLoaded(
      stat: stat == freezed
          ? _value.stat
          : stat // ignore: cast_nullable_to_non_nullable
              as FGameStatPistol,
      mapstat_name: mapstat_name == freezed
          ? _value.mapstat_name
          : mapstat_name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
class _$_GameStatPistolLoaded implements _GameStatPistolLoaded {
  const _$_GameStatPistolLoaded(
      {required this.stat, required this.mapstat_name});

  @override
  final FGameStatPistol stat;
  @override
  final String mapstat_name;

  @override
  String toString() {
    return 'GameStatPistolState.loaded(stat: $stat, mapstat_name: $mapstat_name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _GameStatPistolLoaded &&
            (identical(other.stat, stat) ||
                const DeepCollectionEquality().equals(other.stat, stat)) &&
            (identical(other.mapstat_name, mapstat_name) ||
                const DeepCollectionEquality()
                    .equals(other.mapstat_name, mapstat_name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(stat) ^
      const DeepCollectionEquality().hash(mapstat_name);

  @JsonKey(ignore: true)
  @override
  _$GameStatPistolLoadedCopyWith<_GameStatPistolLoaded> get copyWith =>
      __$GameStatPistolLoadedCopyWithImpl<_GameStatPistolLoaded>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(FGameStatPistol stat, String mapstat_name) loaded,
    required TResult Function(String err) error,
  }) {
    return loaded(stat, mapstat_name);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(FGameStatPistol stat, String mapstat_name)? loaded,
    TResult Function(String err)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(stat, mapstat_name);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GameStatPistolInit value) loading,
    required TResult Function(_GameStatPistolLoaded value) loaded,
    required TResult Function(_GameStatPistolLoadInProgress value) error,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GameStatPistolInit value)? loading,
    TResult Function(_GameStatPistolLoaded value)? loaded,
    TResult Function(_GameStatPistolLoadInProgress value)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class _GameStatPistolLoaded implements GameStatPistolState {
  const factory _GameStatPistolLoaded(
      {required FGameStatPistol stat,
      required String mapstat_name}) = _$_GameStatPistolLoaded;

  FGameStatPistol get stat => throw _privateConstructorUsedError;
  String get mapstat_name => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$GameStatPistolLoadedCopyWith<_GameStatPistolLoaded> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$GameStatPistolLoadInProgressCopyWith<$Res> {
  factory _$GameStatPistolLoadInProgressCopyWith(
          _GameStatPistolLoadInProgress value,
          $Res Function(_GameStatPistolLoadInProgress) then) =
      __$GameStatPistolLoadInProgressCopyWithImpl<$Res>;
  $Res call({String err});
}

/// @nodoc
class __$GameStatPistolLoadInProgressCopyWithImpl<$Res>
    extends _$GameStatPistolStateCopyWithImpl<$Res>
    implements _$GameStatPistolLoadInProgressCopyWith<$Res> {
  __$GameStatPistolLoadInProgressCopyWithImpl(
      _GameStatPistolLoadInProgress _value,
      $Res Function(_GameStatPistolLoadInProgress) _then)
      : super(_value, (v) => _then(v as _GameStatPistolLoadInProgress));

  @override
  _GameStatPistolLoadInProgress get _value =>
      super._value as _GameStatPistolLoadInProgress;

  @override
  $Res call({
    Object? err = freezed,
  }) {
    return _then(_GameStatPistolLoadInProgress(
      err: err == freezed
          ? _value.err
          : err // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
class _$_GameStatPistolLoadInProgress implements _GameStatPistolLoadInProgress {
  const _$_GameStatPistolLoadInProgress({required this.err});

  @override
  final String err;

  @override
  String toString() {
    return 'GameStatPistolState.error(err: $err)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _GameStatPistolLoadInProgress &&
            (identical(other.err, err) ||
                const DeepCollectionEquality().equals(other.err, err)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(err);

  @JsonKey(ignore: true)
  @override
  _$GameStatPistolLoadInProgressCopyWith<_GameStatPistolLoadInProgress>
      get copyWith => __$GameStatPistolLoadInProgressCopyWithImpl<
          _GameStatPistolLoadInProgress>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(FGameStatPistol stat, String mapstat_name) loaded,
    required TResult Function(String err) error,
  }) {
    return error(err);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(FGameStatPistol stat, String mapstat_name)? loaded,
    TResult Function(String err)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(err);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GameStatPistolInit value) loading,
    required TResult Function(_GameStatPistolLoaded value) loaded,
    required TResult Function(_GameStatPistolLoadInProgress value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GameStatPistolInit value)? loading,
    TResult Function(_GameStatPistolLoaded value)? loaded,
    TResult Function(_GameStatPistolLoadInProgress value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _GameStatPistolLoadInProgress implements GameStatPistolState {
  const factory _GameStatPistolLoadInProgress({required String err}) =
      _$_GameStatPistolLoadInProgress;

  String get err => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$GameStatPistolLoadInProgressCopyWith<_GameStatPistolLoadInProgress>
      get copyWith => throw _privateConstructorUsedError;
}
