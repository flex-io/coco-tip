import 'package:app/bloc/bloc.dart';
import 'package:app/screens/game/stat/pistol/view.dart';
import 'package:app/screens/game/stat/series/series.dart';

// ignore: import_of_legacy_library_into_null_safe
import 'package:app/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GameScreen extends StatelessWidget {
  // Widget popupActions(
  //         {BuildContext? context,
  //         required List<String> items,
  //         required void Function(String) onSelected}) =>
  //     PopupMenuButton<String>(
  //         onSelected: (choice) => onSelected(choice),
  //         itemBuilder: (_) => items.map((String choice) {
  //               return PopupMenuItem<String>(
  //                 value: choice,
  //                 child: Text(choice),
  //               );
  //             }).toList());

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GameCubit, GameState>(
      builder: (context, state) => state.when(
        loading: loading,
        loaded: (game) => DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: AppBar(
              title: Text('Game'),
              bottom: TabBar(
                tabs: [
                  Tab(text: 'Series'),
                  Tab(text: 'Pistol'),
                ],
              ),
            ),
            body: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              children: [
                GameStatSeriesView(
                  team1_series: game.team1_result,
                  h2h: game.h2h_result,
                  team2_series: game.team2_result,
                ),
                GameStatPistolView(),
              ],
            ),
          ),
        ),
        error: error,
      ),
    );
  }
}


// import 'package:app/bloc/bloc.dart';
// import 'package:app/models/models.dart';
// import 'package:app/screens/game/stat/t_game_stat.dart';

// // ignore: import_of_legacy_library_into_null_safe
// import 'package:app/widget/widget.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';

// class GameScreen extends StatelessWidget {
//   Widget popupActions(
//           {BuildContext? context,
//           required List<String> items,
//           required void Function(String) onSelected}) =>
//       PopupMenuButton<String>(
//           onSelected: (choice) => onSelected(choice),
//           itemBuilder: (_) => items.map((String choice) {
//                 return PopupMenuItem<String>(
//                   value: choice,
//                   child: Text(choice),
//                 );
//               }).toList());

//   @override
//   Widget build(BuildContext context) {
//     return BlocBuilder<GameCubit, GameState>(
//       builder: (context, state) => state.when(
//         loading: loading,
//         loaded: (game) => BlocBuilder<GameStatCubit, GameStatState>(
//             builder: (context, state2) => state2.when(
//                 loading: loading,
//                 loaded: (List<GameStat> stats, mapstat_name) => DefaultTabController(
//                       length: stats.length,
//                       child: Scaffold(
//                         appBar: AppBar(
//                           title: Text('Game'),
//                           actions: <Widget>[
//                             popupActions(
//                               items: ['All']..addAll(game.maps.toList()),
//                               onSelected: (map) =>
//                                   BlocProvider.of<GameStatCubit>(context).filterStat(map),
//                             ),
//                           ],
//                           bottom: TabBar(
//                             tabs: stats.map((e) => Tab(text: e.name)).toList(),
//                           ),
//                         ),
//                         body: TabBarView(
//                           physics: NeverScrollableScrollPhysics(),
//                           children: stats
//                               .map((stat) => TGameStat(
//                                     context,
//                                     stat,
//                                     mapstat_name,
//                                   ))
//                               .toList(),
//                         ),
//                       ),
//                     ),
//                 error: error)),
//         error: error,
//       ),
//     );
//   }
// }
