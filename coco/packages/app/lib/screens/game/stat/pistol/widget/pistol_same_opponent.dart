part of 'widget.dart';

class PistolSameOpponent extends StatelessWidget {
  final List<GameDetail>? gameDetails, team2;
  PistolSameOpponent({this.gameDetails, this.team2});

  @override
  Widget build(BuildContext context) {
    if (gameDetails == null || (gameDetails ?? []).isEmpty) return SizedBox.shrink();

    List<Widget> widgetGroups = [];
    Map<String, List<GameDetail>>? detailGroup = gameDetails
        ?.pickSome(5)
        .groupBy((element) => element.opponent_name, valueTransform: (item) => item);

    detailGroup?.forEach((key, value) {
      widgetGroups.add(ListTile(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              flex: 4,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: value
                      .map((e) => PistolRound(
                          r1: e.team_round_1,
                          r16: e.team_round_16,
                          r1IsCSide: e.team_round_1_ct_side,
                          r16IsCSide: e.team_round_16_ct_side))
                      .toList()),
            ),
            Expanded(
                flex: 2,
                child: Text(
                  key,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 11),
                )),
            Expanded(
              flex: 4,
              child: Row(
                  children: (team2 ?? [])
                      .where((element) => element.opponent_name == key)
                      .pickSome(5)
                      .map((e) => PistolRound(
                          r1: e.team_round_1,
                          r16: e.team_round_16,
                          r1IsCSide: e.team_round_1_ct_side,
                          r16IsCSide: e.team_round_16_ct_side))
                      .toList()),
            )
          ],
        ),
      ));
    });

    return Column(
      children: [
        TextHeader('Same Opponent'),
        ListDivideTiles(children: widgetGroups),
      ],
    );
  }
}
