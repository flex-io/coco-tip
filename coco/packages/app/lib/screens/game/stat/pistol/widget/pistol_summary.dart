part of 'widget.dart';

class PistolSummary extends StatelessWidget {
  final GameStatPistol? team1Pistol, team2Pistol;

  PistolSummary({required this.team1Pistol, required this.team2Pistol});

  @override
  Widget build(BuildContext context) {
    return ListDivideTiles(
      header: TextHeader('Summary'),
      children: [
        Tile1(
          lead: (team1Pistol?.wins ?? 0).toString() + '/' + (team1Pistol?.loses ?? 0).toString(),
          title: 'Total\nWins/Loses',
          trail: (team2Pistol?.wins ?? 0).toString() + '/' + (team2Pistol?.loses ?? 0).toString(),
        ),
        Tile1(
          lead: (team1Pistol?.ctSide ?? []).where((e) => e != 0).length.toString() +
              '/' +
              (team1Pistol?.ctSide ?? []).where((e) => e == 0).length.toString(),
          title: 'C-Side',
          trail: (team2Pistol?.ctSide ?? []).where((e) => e != 0).length.toString() +
              '/' +
              (team2Pistol?.ctSide ?? []).where((e) => e == 0).length.toString(),
        ),
        Tile1(
          lead: (team1Pistol?.tSide ?? []).where((e) => e != 0).length.toString() +
              '/' +
              (team1Pistol?.tSide ?? []).where((e) => e == 0).length.toString(),
          title: 'T-Side',
          trail: (team2Pistol?.tSide ?? []).where((e) => e != 0).length.toString() +
              '/' +
              (team2Pistol?.tSide ?? []).where((e) => e == 0).length.toString(),
        ),
        Tile1(
          lead: (team1Pistol?.againstFavourite ?? [])
                  .map((e) => (e.team_round_1 != 0 ? 1 : 0) + (e.team_round_16 != 0 ? 1 : 0))
                  .fold(0, sum)
                  .toString() +
              '/' +
              (team1Pistol?.againstFavourite ?? [])
                  .map((e) => (e.team_round_1 == 0 ? 1 : 0) + (e.team_round_16 == 0 ? 1 : 0))
                  .fold(0, sum)
                  .toString(),
          title: 'Against Favourite\n(R1 + R16)',
          trail: (team2Pistol?.againstFavourite ?? [])
                  .map((e) => (e.team_round_1 != 0 ? 1 : 0) + (e.team_round_16 != 0 ? 1 : 0))
                  .fold(0, sum)
                  .toString() +
              '/' +
              (team2Pistol?.againstFavourite ?? [])
                  .map((e) => (e.team_round_1 == 0 ? 1 : 0) + (e.team_round_16 == 0 ? 1 : 0))
                  .fold(0, sum)
                  .toString(),
        ),
        Tile1(
          lead: (team1Pistol?.againstUnderdog ?? [])
                  .map((e) => (e.team_round_1 != 0 ? 1 : 0) + (e.team_round_16 != 0 ? 1 : 0))
                  .fold(0, sum)
                  .toString() +
              '/' +
              (team1Pistol?.againstUnderdog ?? [])
                  .map((e) => (e.team_round_1 == 0 ? 1 : 0) + (e.team_round_16 == 0 ? 1 : 0))
                  .fold(0, sum)
                  .toString(),
          title: 'Against Underdog\n(R1 + R16)',
          trail: (team2Pistol?.againstUnderdog ?? [])
                  .map((e) => (e.team_round_1 != 0 ? 1 : 0) + (e.team_round_16 != 0 ? 1 : 0))
                  .fold(0, sum)
                  .toString() +
              '/' +
              (team2Pistol?.againstUnderdog ?? [])
                  .map((e) => (e.team_round_1 == 0 ? 1 : 0) + (e.team_round_16 == 0 ? 1 : 0))
                  .fold(0, sum)
                  .toString(),
        ),
      ],
    );
  }
}
