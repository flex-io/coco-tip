part of 'widget.dart';

class PistolLastMatches extends StatelessWidget {
  final GameStatPistol? team1Pistol, team2Pistol;

  PistolLastMatches({required this.team1Pistol, required this.team2Pistol});

  Widget LastMatches(GameStatPistol? stat) {
    return ListTile(
      title: Row(
        children: [
          Expanded(flex: 3, child: Text(stat!.team_name, textAlign: TextAlign.left)),
          Expanded(
            flex: 7,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ...(stat.details ?? [])
                      .take(5)
                      .map((e) => PistolRound(
                          r1: e.team_round_1,
                          r16: e.team_round_16,
                          r1IsCSide: e.team_round_1_ct_side,
                          r16IsCSide: e.team_round_16_ct_side))
                      .toList(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('R1', style: TextStyle(fontSize: 11)),
                      Text('R16', style: TextStyle(fontSize: 11)),
                    ],
                  ),
                ].toList()),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          TextHeader('Last 5 Matches'),
          Container(
            color: Theme.of(context).primaryColor,
            child: ListDivideTiles(
              children: [
                LastMatches(team1Pistol),
                LastMatches(team2Pistol),
              ],
            ),
          )
        ],
      ),
    );
  }
}
