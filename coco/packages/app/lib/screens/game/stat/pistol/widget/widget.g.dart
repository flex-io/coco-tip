// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'widget.dart';

// **************************************************************************
// FunctionalWidgetGenerator
// **************************************************************************

class Tile1 extends StatelessWidget {
  const Tile1(
      {Key? key, required this.lead, required this.title, required this.trail})
      : super(key: key);

  final String lead;

  final String title;

  final String trail;

  @override
  Widget build(BuildContext _context) =>
      tile1(lead: lead, title: title, trail: trail);
}

class TextHeader extends StatelessWidget {
  const TextHeader(this.data, {Key? key, this.alignment}) : super(key: key);

  final String data;

  final AlignmentGeometry? alignment;

  @override
  Widget build(BuildContext _context) => textHeader(data, alignment: alignment);
}
