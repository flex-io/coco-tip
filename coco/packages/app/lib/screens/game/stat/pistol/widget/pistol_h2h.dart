part of 'widget.dart';

class PistolH2h extends StatelessWidget {
  final List<GameDetail>? h2h;

  PistolH2h({this.h2h});

  @override
  Widget build(BuildContext context) {
    if ((h2h != null))
      return Padding(
        padding: const EdgeInsets.only(top: 12, bottom: 12),
        child: ListTile(
          tileColor: Theme.of(context).primaryColor,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                children: h2h!
                    .map((e) => PistolRound(
                        r1: e.team_round_1,
                        r16: e.team_round_16,
                        r1IsCSide: e.team_round_1_ct_side,
                        r16IsCSide: e.team_round_16_ct_side))
                    .toList(),
              ),
              Text('H2H'),
              Row(
                children: h2h!
                    .map((e) => PistolRound(
                        r1: e.opponent_round_1,
                        r16: e.opponent_round_16,
                        r1IsCSide: !e.team_round_1_ct_side,
                        r16IsCSide: !e.team_round_16_ct_side))
                    .toList(),
              ),
            ],
          ),
        ),
      );
    else
      return SizedBox.shrink();
  }
}
