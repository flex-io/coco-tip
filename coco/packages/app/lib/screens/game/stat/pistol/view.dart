import 'package:app/bloc/bloc.dart';
import 'package:app/models/models.dart';
import 'package:app/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'widget/widget.dart';

class GameStatPistolView extends StatelessWidget {
  // final GameStatPistol? team1, team2;
  // final String mapstatName;
  // final List<GameDetail>? h2h, sameOpponent;
  // late final GameStatPistol team1Pistol;
  // late final GameStatPistol team2Pistol;

  @override
  Widget build(BuildContext context) {
    // print(sameOpponent);
    return BlocBuilder<GameStatPistolCubit, GameStatPistolState>(
      builder: (context, state) => state.maybeWhen(
          orElse: orElse,
          loading: loading,
          loaded: (stat, mapstat_name) => ListView(
                children: [
                  ListTile(
                    title: Text(
                      mapstat_name,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  PistolSummary(
                    team1Pistol: stat.team1,
                    team2Pistol: stat.team2,
                  ),
                  PistolLastMatches(
                    team1Pistol: stat.team1,
                    team2Pistol: stat.team2,
                  ),
                  PistolH2h(h2h: stat.h2h),
                  PistolSameOpponent(
                    gameDetails: stat.sameOpponent,
                    team2: stat.team2?.details,
                  ),
                ],
              )),
    );
  }
}
