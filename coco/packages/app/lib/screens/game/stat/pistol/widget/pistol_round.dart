part of 'widget.dart';

class PistolRound extends StatelessWidget {
  const PistolRound({
    Key? key,
    required this.r1,
    required this.r16,
    required this.r1IsCSide,
    required this.r16IsCSide,
  }) : super(key: key);

  final int r1;
  final int r16;
  final bool r1IsCSide;
  final bool r16IsCSide;

  @override
  Widget build(BuildContext context) {
    IconData r1Icon = r1 != 0 ? Icons.check : Icons.remove;
    IconData r16Icon = r16 != 0 ? Icons.check : Icons.remove;
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Column(
        children: [
          Icon(
            r1Icon,
            color: r1IsCSide ? Colors.blue : Colors.orange,
            size: 16,
          ),
          Icon(
            r16Icon,
            color: r16IsCSide ? Colors.blue : Colors.orange,
            size: 16,
          )
        ],
      ),
    );
  }
}
