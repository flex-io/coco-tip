import 'package:app/config/config.dart';
import 'package:app/models/models.dart';
import 'package:app/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:functional_widget_annotation/functional_widget_annotation.dart';
import 'package:supercharged/supercharged.dart';

part 'widget.g.dart';
part 'pistol_match.dart';
part 'pistol_last_matches.dart';
part 'pistol_same_opponent.dart';
part 'pistol_round.dart';
part 'pistol_h2h.dart';
part 'pistol_summary.dart';

@swidget
Widget tile1({required String lead, required String title, required String trail}) {
  const TextStyle textStyle = TextStyle(fontSize: 13);
  return Container(
    height: 40,
    // padding: EdgeInsets.only(top: 15, bottom: 15),
    child: Row(
      children: [
        Expanded(flex: 3, child: Text(lead, textAlign: TextAlign.left, style: textStyle)),
        Expanded(
            flex: 4,
            child: Text(title, textAlign: TextAlign.center, style: TextStyle(fontSize: 12))),
        Expanded(flex: 3, child: Text(trail, textAlign: TextAlign.right, style: textStyle)),
      ],
    ),
  );
}

@swidget
Widget textHeader(String data, {AlignmentGeometry? alignment}) => Container(
      alignment: alignment ?? WidgetDefault.textHeader.alignment,
      padding: WidgetDefault.paddingAll,
      child: Text(data),
    );
