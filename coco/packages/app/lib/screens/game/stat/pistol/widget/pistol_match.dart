part of 'widget.dart';

class PistolMatch extends StatelessWidget {
  final GameStatPistol team1Pistol, team2Pistol;

  PistolMatch({required this.team1Pistol, required this.team2Pistol});

  Widget PistolRounds(int r1, int r16, bool r1IsCSide, bool r16IsCSide) {
    IconData r1Icon = r1 > 0 ? Icons.check : Icons.remove;
    IconData r16Icon = r16 > 0 ? Icons.check : Icons.remove;
    return Column(
      children: [
        Icon(
          r1Icon,
          color: r1IsCSide ? Colors.blue : Colors.orange,
          size: 16,
        ),
        Icon(
          r16Icon,
          color: r16IsCSide ? Colors.blue : Colors.orange,
          size: 16,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 4,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: team1Pistol.details!
                .take(5)
                .map((e) => PistolRounds(
                      e.team_round_1,
                      e.team_round_16,
                      e.team_round_1_ct_side,
                      e.team_round_16_ct_side,
                    ))
                .toList()
                .reversed
                .toList(),
          ),
        ),
        Expanded(
          flex: 3,
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Icon(Icons.arrow_left),
            Column(
              children: [
                Text('R1', style: TextStyle(fontSize: 11)),
                Text('R16', style: TextStyle(fontSize: 11)),
              ],
            ),
            Icon(Icons.arrow_right),
          ]),
        ),
        Expanded(
            flex: 4,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: team2Pistol.details!
                    .take(5)
                    .map((e) => PistolRounds(
                          e.team_round_1,
                          e.team_round_16,
                          e.team_round_1_ct_side,
                          e.team_round_16_ct_side,
                        ))
                    .toList())),
      ],
    );
  }
}
