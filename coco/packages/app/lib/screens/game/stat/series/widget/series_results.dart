part of 'widget.dart';

class TeamResult {
  final String name;
  const TeamResult({required this.name});
}

class SeriesResults extends StatefulWidget {
  final GameResultGroup team1Result, h2h, team2Result;
  const SeriesResults({
    required this.team1Result,
    required this.h2h,
    required this.team2Result,
  });
  @override
  _SeriesResultsState createState() => _SeriesResultsState();
}

class _SeriesResultsState extends State<SeriesResults> {
  List<bool> isSelected = [false, true, false];
  int _indexSelected = 1;
  late List<GameResultGroup> results = [widget.team1Result, widget.h2h, widget.team2Result];

  double iconSize = 9;
  Widget TRoundIcon(int score) {
    if (score == 0)
      return Padding(
        padding: const EdgeInsets.all(0.5),
        child: Icon(Icons.remove, color: Colors.grey, size: iconSize),
      );
    if (score > 0)
      return Padding(
        padding: const EdgeInsets.all(0.5),
        child: Icon(
          Icons.check_circle,
          color: Colors.blue,
          size: iconSize,
        ),
      );
    else
      return Padding(
        padding: const EdgeInsets.all(0.5),
        child: Icon(Icons.check_circle, color: Colors.orange, size: iconSize),
      );
  }

  Widget TRoundHistory(List<int> rounds) => Row(
        children: rounds.map((e) => TRoundIcon(e)).toList(),
      );

  Widget TGameMapDetail(GameDetail gameDetail) => Padding(
        padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Align(
                alignment: Alignment.centerLeft,
                child: Text(gameDetail.mapstat_name,
                    style: TextStyle(fontSize: 11), textAlign: TextAlign.left)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TRoundHistory(gameDetail.team_round_history_array
                    .getRange(
                        0, min(30, (gameDetail.team_score ?? 0) + (gameDetail.opponent_score ?? 0)))
                    .toList()),
                Text(gameDetail.team_score.toString())
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TRoundHistory(gameDetail.opponent_round_history_array
                    .getRange(
                        0, min(30, (gameDetail.team_score ?? 0) + (gameDetail.opponent_score ?? 0)))
                    .toList()),
                Text(gameDetail.opponent_score.toString())
              ],
            ),
          ],
        ),
      );

  Widget TGameResult(GameResult gameResult) => ExpansionTile(
        childrenPadding: EdgeInsets.only(left: 5, right: 5),
        subtitle: Text(timeago.format(gameResult.match_date)),
        children: [
          ListDivideTiles(
              children:
                  (gameResult.details?.map((e) => TGameMapDetail(e)).toList() ?? [Text('No Data')]))
        ],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(gameResult.opponent_name),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(gameResult.team_final_score.toString()),
                Text(' - '),
                Text(gameResult.opponent_final_score.toString()),
                gameResult.team_win
                    ? Text(
                        '  W',
                        style: TextStyle(color: Colors.green),
                      )
                    : Text('  L', style: TextStyle(color: Colors.red)),
              ],
            )
          ],
        ),
      );

  Widget TGameResultGroup(GameResultGroup gameResultGroup) => Column(
        children: gameResultGroup.results?.map((e) => TGameResult(e)).toList() ?? [Text('No data')],
      );

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          TextHeader('Results'),
          Card(
            child: Column(
              children: [
                ToggleButtons(
                  renderBorder: false,
                  children: <Widget>[
                    Container(
                        width: 150,
                        child: Text(
                          widget.team1Result.name,
                          textAlign: TextAlign.center,
                        )),
                    Container(child: Text(widget.h2h.name, textAlign: TextAlign.center)),
                    Container(
                        width: 150,
                        child: Text(widget.team2Result.name, textAlign: TextAlign.center)),
                  ],
                  onPressed: (int index) {
                    setState(() {
                      _indexSelected = index;
                      for (int buttonIndex = 0; buttonIndex < isSelected.length; buttonIndex++) {
                        if (buttonIndex == index) {
                          isSelected[buttonIndex] = true;
                        } else {
                          isSelected[buttonIndex] = false;
                        }
                      }
                    });
                  },
                  isSelected: isSelected,
                ),
                TGameResultGroup(results[_indexSelected]),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class LabeledCheckbox extends StatelessWidget {
  const LabeledCheckbox({
    Key? key,
    required this.label,
    required this.padding,
    required this.value,
    required this.onChanged,
  }) : super(key: key);

  final String label;
  final EdgeInsets padding;
  final bool value;
  final Function onChanged;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onChanged(!value);
      },
      child: Padding(
        padding: padding,
        child: Row(
          children: <Widget>[
            Expanded(child: Text(label)),
            Checkbox(
              value: value,
              onChanged: (bool? newValue) {
                onChanged(newValue);
              },
            ),
          ],
        ),
      ),
    );
  }
}
