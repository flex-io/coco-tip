import 'package:app/models/models.dart';
import 'package:app/screens/game/stat/series/series.dart';
import 'package:flutter/material.dart';

class GameStatSeriesView extends StatelessWidget {
  final GameResultGroup team1_series, h2h, team2_series;
  final Map<String, List<GameDetail>>? sameOpponent;

  const GameStatSeriesView({
    required this.team1_series,
    required this.team2_series,
    required this.h2h,
    this.sameOpponent,
  });

  @override
  Widget build(BuildContext context) {
    // print(team1_series?.map((e) => e.team_win));
    return ListView(
      children: [
        SeriesResults(
          team1Result: team1_series,
          h2h: h2h,
          team2Result: team2_series,
        )
      ],
    );
  }
}
