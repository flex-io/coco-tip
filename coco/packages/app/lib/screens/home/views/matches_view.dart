import 'dart:async';
import 'package:app/bloc/bloc.dart';
import 'package:app/config/config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app/models/models.dart';
import 'package:intl/intl.dart';

class GameItem extends StatelessWidget {
  late final Match game;
  final GestureTapCallback onTap;

  GameItem({
    Key? key,
    required this.game,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Text(
        'CS:GO',
        style: TextStyle(fontSize: 10.0),
      ),
      trailing: Text(
        DateFormat('h:mm a\ndd MMM').format(game.match_date.add(Duration(hours: 11))),
        textAlign: TextAlign.right,
      ),
      title: Text(game.title),
      isThreeLine: true,
      subtitle: Text(game.match_event),
      dense: true,
      // trailing: Text(game.matchDate.toIso8601String()),
      // trailing: Text(game.timeAgo),
      onTap: onTap,
    );
  }
}

class MatchesView extends StatefulWidget {
  @override
  _MatchesViewState createState() => _MatchesViewState();
}

class _MatchesViewState extends State<MatchesView> {
  late Completer<void> _refreshCompleter;

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(title: Text('Match List')),
      body: BlocBuilder<MatchesCubit, MatchesState>(
        builder: (context, state) {
          return state.when(
              loading: () => Center(
                    child: CircularProgressIndicator(),
                  ),
              loaded: (matches) => RefreshIndicator(
                  child: ListView.builder(
                    key: const Key('__games__'),
                    itemCount: matches.length,
                    itemBuilder: (BuildContext context, int index) {
                      final game = matches[index];
                      return GameItem(
                        game: game,
                        onTap: () async {
                          await Navigator.pushNamed(
                            context,
                            Routes.game,
                            arguments: ScreenArguments(id: game.id),
                          );
                        },
                      );
                    },
                  ),
                  onRefresh: () {
                    BlocProvider.of<MatchesCubit>(context)..loadMatches();
                    return _refreshCompleter.future;
                  }),
              error: (error) => Text(error));
        },
      ),
    );
  }
}
