library coco_app;

import 'package:app/repos/repos.dart';
import 'package:app/screens/screens.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/bloc.dart';
import 'config/config.dart';
import 'models/models.dart';

Widget cocoApp = CocoApp();

class CocoApp extends StatefulWidget {
  @override
  _CocoAppState createState() => _CocoAppState();
}

class _CocoAppState extends State<CocoApp> {
  @override
  void initState() {
    super.initState();
    // setup();
  }

  // Future<void> setup() async {
  //   await tz.initializeTimeZone();
  //   var australia = tz.getLocation('Australia/Sydney');
  //   var now = tz.TZDateTime.now(australia);
  //   print(now.toString());
  // }

  @override
  Widget build(BuildContext context) {
    // return app.build(context);
    return MaterialApp(
      title: 'Coco',
      theme: Themes.theme,
      routes: {
        Routes.home: (context) {
          return MultiBlocProvider(providers: [
            BlocProvider(
              create: (context) => MatchesCubit(repository: const MatchesRepo())..loadMatches(),
            ),
          ], child: HomeScreen());
        },
        Routes.game: (context) {
          final ScreenArguments args =
              ModalRoute.of(context)?.settings.arguments as ScreenArguments;
          return MultiBlocProvider(providers: [
            BlocProvider<GameCubit>(
              create: (context) => GameCubit()..loadWithSample(args.id),
            ),
            BlocProvider<GameStatPistolCubit>(
              create: (context) =>
                  GameStatPistolCubit(gameCubit: BlocProvider.of<GameCubit>(context)),
            ),
          ], child: GameScreen());
        },
      },
    );
  }
}
