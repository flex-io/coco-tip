import 'package:freezed_annotation/freezed_annotation.dart';

part 'match.freezed.dart';

part 'match.g.dart';

@freezed
class Match with _$Match {
  // ignore: unused_element
  const Match._();

  factory Match(
      DateTime match_date,
      int id,
      String team1_name,
      String team2_name,
      int team1_id,
      int team2_id,
      String match_event,
      String s3_url,
      ) = _Match;

  String get title => '$team1_name v $team2_name';

  factory Match.fromJson(Map<String, dynamic> json) => _$MatchFromJson(json);
}
