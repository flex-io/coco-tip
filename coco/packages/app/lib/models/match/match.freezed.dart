// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'match.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Match _$MatchFromJson(Map<String, dynamic> json) {
  return _Match.fromJson(json);
}

/// @nodoc
class _$MatchTearOff {
  const _$MatchTearOff();

  _Match call(DateTime match_date, int id, String team1_name, String team2_name,
      int team1_id, int team2_id, String match_event, String s3_url) {
    return _Match(
      match_date,
      id,
      team1_name,
      team2_name,
      team1_id,
      team2_id,
      match_event,
      s3_url,
    );
  }

  Match fromJson(Map<String, Object> json) {
    return Match.fromJson(json);
  }
}

/// @nodoc
const $Match = _$MatchTearOff();

/// @nodoc
mixin _$Match {
  DateTime get match_date => throw _privateConstructorUsedError;
  int get id => throw _privateConstructorUsedError;
  String get team1_name => throw _privateConstructorUsedError;
  String get team2_name => throw _privateConstructorUsedError;
  int get team1_id => throw _privateConstructorUsedError;
  int get team2_id => throw _privateConstructorUsedError;
  String get match_event => throw _privateConstructorUsedError;
  String get s3_url => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MatchCopyWith<Match> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MatchCopyWith<$Res> {
  factory $MatchCopyWith(Match value, $Res Function(Match) then) =
      _$MatchCopyWithImpl<$Res>;
  $Res call(
      {DateTime match_date,
      int id,
      String team1_name,
      String team2_name,
      int team1_id,
      int team2_id,
      String match_event,
      String s3_url});
}

/// @nodoc
class _$MatchCopyWithImpl<$Res> implements $MatchCopyWith<$Res> {
  _$MatchCopyWithImpl(this._value, this._then);

  final Match _value;
  // ignore: unused_field
  final $Res Function(Match) _then;

  @override
  $Res call({
    Object? match_date = freezed,
    Object? id = freezed,
    Object? team1_name = freezed,
    Object? team2_name = freezed,
    Object? team1_id = freezed,
    Object? team2_id = freezed,
    Object? match_event = freezed,
    Object? s3_url = freezed,
  }) {
    return _then(_value.copyWith(
      match_date: match_date == freezed
          ? _value.match_date
          : match_date // ignore: cast_nullable_to_non_nullable
              as DateTime,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      team1_name: team1_name == freezed
          ? _value.team1_name
          : team1_name // ignore: cast_nullable_to_non_nullable
              as String,
      team2_name: team2_name == freezed
          ? _value.team2_name
          : team2_name // ignore: cast_nullable_to_non_nullable
              as String,
      team1_id: team1_id == freezed
          ? _value.team1_id
          : team1_id // ignore: cast_nullable_to_non_nullable
              as int,
      team2_id: team2_id == freezed
          ? _value.team2_id
          : team2_id // ignore: cast_nullable_to_non_nullable
              as int,
      match_event: match_event == freezed
          ? _value.match_event
          : match_event // ignore: cast_nullable_to_non_nullable
              as String,
      s3_url: s3_url == freezed
          ? _value.s3_url
          : s3_url // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$MatchCopyWith<$Res> implements $MatchCopyWith<$Res> {
  factory _$MatchCopyWith(_Match value, $Res Function(_Match) then) =
      __$MatchCopyWithImpl<$Res>;
  @override
  $Res call(
      {DateTime match_date,
      int id,
      String team1_name,
      String team2_name,
      int team1_id,
      int team2_id,
      String match_event,
      String s3_url});
}

/// @nodoc
class __$MatchCopyWithImpl<$Res> extends _$MatchCopyWithImpl<$Res>
    implements _$MatchCopyWith<$Res> {
  __$MatchCopyWithImpl(_Match _value, $Res Function(_Match) _then)
      : super(_value, (v) => _then(v as _Match));

  @override
  _Match get _value => super._value as _Match;

  @override
  $Res call({
    Object? match_date = freezed,
    Object? id = freezed,
    Object? team1_name = freezed,
    Object? team2_name = freezed,
    Object? team1_id = freezed,
    Object? team2_id = freezed,
    Object? match_event = freezed,
    Object? s3_url = freezed,
  }) {
    return _then(_Match(
      match_date == freezed
          ? _value.match_date
          : match_date // ignore: cast_nullable_to_non_nullable
              as DateTime,
      id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      team1_name == freezed
          ? _value.team1_name
          : team1_name // ignore: cast_nullable_to_non_nullable
              as String,
      team2_name == freezed
          ? _value.team2_name
          : team2_name // ignore: cast_nullable_to_non_nullable
              as String,
      team1_id == freezed
          ? _value.team1_id
          : team1_id // ignore: cast_nullable_to_non_nullable
              as int,
      team2_id == freezed
          ? _value.team2_id
          : team2_id // ignore: cast_nullable_to_non_nullable
              as int,
      match_event == freezed
          ? _value.match_event
          : match_event // ignore: cast_nullable_to_non_nullable
              as String,
      s3_url == freezed
          ? _value.s3_url
          : s3_url // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Match extends _Match {
  _$_Match(this.match_date, this.id, this.team1_name, this.team2_name,
      this.team1_id, this.team2_id, this.match_event, this.s3_url)
      : super._();

  factory _$_Match.fromJson(Map<String, dynamic> json) =>
      _$_$_MatchFromJson(json);

  @override
  final DateTime match_date;
  @override
  final int id;
  @override
  final String team1_name;
  @override
  final String team2_name;
  @override
  final int team1_id;
  @override
  final int team2_id;
  @override
  final String match_event;
  @override
  final String s3_url;

  @override
  String toString() {
    return 'Match(match_date: $match_date, id: $id, team1_name: $team1_name, team2_name: $team2_name, team1_id: $team1_id, team2_id: $team2_id, match_event: $match_event, s3_url: $s3_url)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Match &&
            (identical(other.match_date, match_date) ||
                const DeepCollectionEquality()
                    .equals(other.match_date, match_date)) &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.team1_name, team1_name) ||
                const DeepCollectionEquality()
                    .equals(other.team1_name, team1_name)) &&
            (identical(other.team2_name, team2_name) ||
                const DeepCollectionEquality()
                    .equals(other.team2_name, team2_name)) &&
            (identical(other.team1_id, team1_id) ||
                const DeepCollectionEquality()
                    .equals(other.team1_id, team1_id)) &&
            (identical(other.team2_id, team2_id) ||
                const DeepCollectionEquality()
                    .equals(other.team2_id, team2_id)) &&
            (identical(other.match_event, match_event) ||
                const DeepCollectionEquality()
                    .equals(other.match_event, match_event)) &&
            (identical(other.s3_url, s3_url) ||
                const DeepCollectionEquality().equals(other.s3_url, s3_url)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(match_date) ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(team1_name) ^
      const DeepCollectionEquality().hash(team2_name) ^
      const DeepCollectionEquality().hash(team1_id) ^
      const DeepCollectionEquality().hash(team2_id) ^
      const DeepCollectionEquality().hash(match_event) ^
      const DeepCollectionEquality().hash(s3_url);

  @JsonKey(ignore: true)
  @override
  _$MatchCopyWith<_Match> get copyWith =>
      __$MatchCopyWithImpl<_Match>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_MatchToJson(this);
  }
}

abstract class _Match extends Match {
  factory _Match(
      DateTime match_date,
      int id,
      String team1_name,
      String team2_name,
      int team1_id,
      int team2_id,
      String match_event,
      String s3_url) = _$_Match;
  _Match._() : super._();

  factory _Match.fromJson(Map<String, dynamic> json) = _$_Match.fromJson;

  @override
  DateTime get match_date => throw _privateConstructorUsedError;
  @override
  int get id => throw _privateConstructorUsedError;
  @override
  String get team1_name => throw _privateConstructorUsedError;
  @override
  String get team2_name => throw _privateConstructorUsedError;
  @override
  int get team1_id => throw _privateConstructorUsedError;
  @override
  int get team2_id => throw _privateConstructorUsedError;
  @override
  String get match_event => throw _privateConstructorUsedError;
  @override
  String get s3_url => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$MatchCopyWith<_Match> get copyWith => throw _privateConstructorUsedError;
}
