// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'match.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Match _$_$_MatchFromJson(Map<String, dynamic> json) {
  return _$_Match(
    DateTime.parse(json['match_date'] as String),
    json['id'] as int,
    json['team1_name'] as String,
    json['team2_name'] as String,
    json['team1_id'] as int,
    json['team2_id'] as int,
    json['match_event'] as String,
    json['s3_url'] as String,
  );
}

Map<String, dynamic> _$_$_MatchToJson(_$_Match instance) => <String, dynamic>{
      'match_date': instance.match_date.toIso8601String(),
      'id': instance.id,
      'team1_name': instance.team1_name,
      'team2_name': instance.team2_name,
      'team1_id': instance.team1_id,
      'team2_id': instance.team2_id,
      'match_event': instance.match_event,
      's3_url': instance.s3_url,
    };
