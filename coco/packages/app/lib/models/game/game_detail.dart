part of 'game.dart';

@freezed
class GameDetail with _$GameDetail {
  GameDetail._();

  factory GameDetail({
    required DateTime match_date,
    required int match_id,
    required int map_stat_id,
    bool? is_favourite,
    required int map_round,
    required String mapstat_name,
    required int team_id,
    int? team_rank,
    required String team_name,
    required bool team_win,
    @Default(0) int? team_score,
    required String team_round_history,
    int? team_clutch_score,
    bool? team_pick,
    required bool team_1st_half_winner,
    int? team_clutch_1st_half,
    required bool team_win_at_pistol_1,
    required bool team_win_at_pistol_16,
    required int opponent_id,
    required int opponent_rank,
    required String opponent_name,
    required bool opponent_win,
    @Default(0) int? opponent_score,
    required String opponent_round_history,
    required int opponent_clutch_score,
    bool? opponent_pick,
    required bool opponent_1st_half_winner,
    int? opponent_clutch_1st_half,
    required bool opponent_win_at_pistol_1,
    required bool opponent_win_at_pistol_16,
    required int team_round_1,
    required int team_round_16,
    required int opponent_round_1,
    required int opponent_round_16,
    required bool team_round_1_ct_side,
    required bool team_round_16_ct_side,
  }) = _GameDetail;

  late final List<int> team_round_history_array =
      team_round_history.replaceAll('|', ',').split(',').map((e) => int.parse(e)).toList();

  late final List<int> opponent_round_history_array =
      opponent_round_history.replaceAll('|', ',').split(',').map((e) => int.parse(e)).toList();

  factory GameDetail.fromJson(Map<String, dynamic> json) => _$GameDetailFromJson(json);
}
