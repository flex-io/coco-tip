part of 'game.dart';

@freezed
class GameStat with _$GameStat {
  const factory GameStat.series({
    @Default('Series') String? name,
    List<GameResult>? team1Series,
    List<GameResult>? team2Series,
    Map<String, List<GameDetail>>? h2h,
    Map<String, List<GameDetail>>? sameOpponent,
  }) = FGameStatSeries;

  const factory GameStat.pistol({
    @Default('Pistol') String? name,
    GameStatPistol? team1,
    GameStatPistol? team2,
    List<GameDetail>? h2h,
    List<GameDetail>? sameOpponent,
  }) = FGameStatPistol;
}
