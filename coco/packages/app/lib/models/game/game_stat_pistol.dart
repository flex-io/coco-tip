part of 'game.dart';

T sum<T extends num>(T? lhs, T? rhs) => (lhs ?? 0) + (rhs ?? 0) as T;

class GameStatPistol {
  final String team_name;
  final List<GameDetail>? details;
  late int wins = 0;
  late int loses = 0;
  late List<int> round1 = [];
  late List<int> round16 = [];
  late List<int> ctSide = [];
  late List<int> tSide = [];
  late List<GameDetail> againstFavourite = [];
  late List<GameDetail> againstUnderdog = [];

  GameStatPistol({
    required this.team_name,
    this.details,
  }) : super() {
    round1 = (details ?? []).map((e) => e.team_round_1).toList();
    round16 = (details ?? []).map((e) => e.team_round_16).toList();
    wins = [...round1, ...round16].where((element) => element != 0).length;
    loses = [...round1, ...round16].where((element) => element == 0).length;
    List<int> ctSideR1 = (details ?? [])
        .where((element) => element.team_round_1_ct_side)
        .map((e) => e.team_round_1)
        .toList();
    List<int> ctSideR16 = (details ?? [])
        .where((element) => element.team_round_16_ct_side)
        .map((e) => e.team_round_16)
        .toList();
    List<int> tSideR1 = (details ?? [])
        .where((element) => !element.team_round_1_ct_side)
        .map((e) => e.team_round_1)
        .toList();
    List<int> tSideR16 = (details ?? [])
        .where((element) => !element.team_round_16_ct_side)
        .map((e) => e.team_round_16)
        .toList();
    ctSide = [...ctSideR1, ...ctSideR16];
    tSide = [...tSideR1, ...tSideR16];
    againstFavourite = (details ?? []).where((element) => element.is_favourite == false).toList();
    againstUnderdog = (details ?? []).where((element) => element.is_favourite == true).toList();
  }

  factory GameStatPistol.defaultValue() {
    return GameStatPistol(team_name: '', details: []);
  }
}
