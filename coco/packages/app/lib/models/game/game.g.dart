// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Game _$_$_GameFromJson(Map<String, dynamic> json) {
  return _$_Game(
    id: json['id'] as int,
    match_date: DateTime.parse(json['match_date'] as String),
    team1_id: json['team1_id'] as int,
    team1_name: json['team1_name'] as String,
    team2_id: json['team2_id'] as int,
    team2_name: json['team2_name'] as String,
    bo: json['bo'] as String?,
    match_event: json['match_event'] as String?,
    team1_details: (json['team1_details'] as List<dynamic>?)
        ?.map((e) => GameDetail.fromJson(e as Map<String, dynamic>))
        .toList(),
    team2_details: (json['team2_details'] as List<dynamic>?)
        ?.map((e) => GameDetail.fromJson(e as Map<String, dynamic>))
        .toList(),
    h2h: (json['h2h'] as List<dynamic>?)
        ?.map((e) => GameDetail.fromJson(e as Map<String, dynamic>))
        .toList(),
    same_opponent: (json['same_opponent'] as List<dynamic>?)
        ?.map((e) => GameDetail.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$_$_GameToJson(_$_Game instance) => <String, dynamic>{
      'id': instance.id,
      'match_date': instance.match_date.toIso8601String(),
      'team1_id': instance.team1_id,
      'team1_name': instance.team1_name,
      'team2_id': instance.team2_id,
      'team2_name': instance.team2_name,
      'bo': instance.bo,
      'match_event': instance.match_event,
      'team1_details': instance.team1_details,
      'team2_details': instance.team2_details,
      'h2h': instance.h2h,
      'same_opponent': instance.same_opponent,
    };

_$_GameDetail _$_$_GameDetailFromJson(Map<String, dynamic> json) {
  return _$_GameDetail(
    match_date: DateTime.parse(json['match_date'] as String),
    match_id: json['match_id'] as int,
    map_stat_id: json['map_stat_id'] as int,
    is_favourite: json['is_favourite'] as bool?,
    map_round: json['map_round'] as int,
    mapstat_name: json['mapstat_name'] as String,
    team_id: json['team_id'] as int,
    team_rank: json['team_rank'] as int?,
    team_name: json['team_name'] as String,
    team_win: json['team_win'] as bool,
    team_score: json['team_score'] as int? ?? 0,
    team_round_history: json['team_round_history'] as String,
    team_clutch_score: json['team_clutch_score'] as int?,
    team_pick: json['team_pick'] as bool?,
    team_1st_half_winner: json['team_1st_half_winner'] as bool,
    team_clutch_1st_half: json['team_clutch_1st_half'] as int?,
    team_win_at_pistol_1: json['team_win_at_pistol_1'] as bool,
    team_win_at_pistol_16: json['team_win_at_pistol_16'] as bool,
    opponent_id: json['opponent_id'] as int,
    opponent_rank: json['opponent_rank'] as int,
    opponent_name: json['opponent_name'] as String,
    opponent_win: json['opponent_win'] as bool,
    opponent_score: json['opponent_score'] as int? ?? 0,
    opponent_round_history: json['opponent_round_history'] as String,
    opponent_clutch_score: json['opponent_clutch_score'] as int,
    opponent_pick: json['opponent_pick'] as bool?,
    opponent_1st_half_winner: json['opponent_1st_half_winner'] as bool,
    opponent_clutch_1st_half: json['opponent_clutch_1st_half'] as int?,
    opponent_win_at_pistol_1: json['opponent_win_at_pistol_1'] as bool,
    opponent_win_at_pistol_16: json['opponent_win_at_pistol_16'] as bool,
    team_round_1: json['team_round_1'] as int,
    team_round_16: json['team_round_16'] as int,
    opponent_round_1: json['opponent_round_1'] as int,
    opponent_round_16: json['opponent_round_16'] as int,
    team_round_1_ct_side: json['team_round_1_ct_side'] as bool,
    team_round_16_ct_side: json['team_round_16_ct_side'] as bool,
  );
}

Map<String, dynamic> _$_$_GameDetailToJson(_$_GameDetail instance) =>
    <String, dynamic>{
      'match_date': instance.match_date.toIso8601String(),
      'match_id': instance.match_id,
      'map_stat_id': instance.map_stat_id,
      'is_favourite': instance.is_favourite,
      'map_round': instance.map_round,
      'mapstat_name': instance.mapstat_name,
      'team_id': instance.team_id,
      'team_rank': instance.team_rank,
      'team_name': instance.team_name,
      'team_win': instance.team_win,
      'team_score': instance.team_score,
      'team_round_history': instance.team_round_history,
      'team_clutch_score': instance.team_clutch_score,
      'team_pick': instance.team_pick,
      'team_1st_half_winner': instance.team_1st_half_winner,
      'team_clutch_1st_half': instance.team_clutch_1st_half,
      'team_win_at_pistol_1': instance.team_win_at_pistol_1,
      'team_win_at_pistol_16': instance.team_win_at_pistol_16,
      'opponent_id': instance.opponent_id,
      'opponent_rank': instance.opponent_rank,
      'opponent_name': instance.opponent_name,
      'opponent_win': instance.opponent_win,
      'opponent_score': instance.opponent_score,
      'opponent_round_history': instance.opponent_round_history,
      'opponent_clutch_score': instance.opponent_clutch_score,
      'opponent_pick': instance.opponent_pick,
      'opponent_1st_half_winner': instance.opponent_1st_half_winner,
      'opponent_clutch_1st_half': instance.opponent_clutch_1st_half,
      'opponent_win_at_pistol_1': instance.opponent_win_at_pistol_1,
      'opponent_win_at_pistol_16': instance.opponent_win_at_pistol_16,
      'team_round_1': instance.team_round_1,
      'team_round_16': instance.team_round_16,
      'opponent_round_1': instance.opponent_round_1,
      'opponent_round_16': instance.opponent_round_16,
      'team_round_1_ct_side': instance.team_round_1_ct_side,
      'team_round_16_ct_side': instance.team_round_16_ct_side,
    };
