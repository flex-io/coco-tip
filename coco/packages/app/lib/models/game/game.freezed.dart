// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'game.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Game _$GameFromJson(Map<String, dynamic> json) {
  return _Game.fromJson(json);
}

/// @nodoc
class _$GameTearOff {
  const _$GameTearOff();

  _Game call(
      {required int id,
      required DateTime match_date,
      required int team1_id,
      required String team1_name,
      required int team2_id,
      required String team2_name,
      String? bo,
      String? match_event,
      List<GameDetail>? team1_details,
      List<GameDetail>? team2_details,
      List<GameDetail>? h2h,
      List<GameDetail>? same_opponent}) {
    return _Game(
      id: id,
      match_date: match_date,
      team1_id: team1_id,
      team1_name: team1_name,
      team2_id: team2_id,
      team2_name: team2_name,
      bo: bo,
      match_event: match_event,
      team1_details: team1_details,
      team2_details: team2_details,
      h2h: h2h,
      same_opponent: same_opponent,
    );
  }

  Game fromJson(Map<String, Object> json) {
    return Game.fromJson(json);
  }
}

/// @nodoc
const $Game = _$GameTearOff();

/// @nodoc
mixin _$Game {
  int get id => throw _privateConstructorUsedError;
  DateTime get match_date => throw _privateConstructorUsedError;
  int get team1_id => throw _privateConstructorUsedError;
  String get team1_name => throw _privateConstructorUsedError;
  int get team2_id => throw _privateConstructorUsedError;
  String get team2_name => throw _privateConstructorUsedError;
  String? get bo => throw _privateConstructorUsedError;
  String? get match_event => throw _privateConstructorUsedError;
  List<GameDetail>? get team1_details => throw _privateConstructorUsedError;
  List<GameDetail>? get team2_details => throw _privateConstructorUsedError;
  List<GameDetail>? get h2h => throw _privateConstructorUsedError;
  List<GameDetail>? get same_opponent => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $GameCopyWith<Game> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GameCopyWith<$Res> {
  factory $GameCopyWith(Game value, $Res Function(Game) then) =
      _$GameCopyWithImpl<$Res>;
  $Res call(
      {int id,
      DateTime match_date,
      int team1_id,
      String team1_name,
      int team2_id,
      String team2_name,
      String? bo,
      String? match_event,
      List<GameDetail>? team1_details,
      List<GameDetail>? team2_details,
      List<GameDetail>? h2h,
      List<GameDetail>? same_opponent});
}

/// @nodoc
class _$GameCopyWithImpl<$Res> implements $GameCopyWith<$Res> {
  _$GameCopyWithImpl(this._value, this._then);

  final Game _value;
  // ignore: unused_field
  final $Res Function(Game) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? match_date = freezed,
    Object? team1_id = freezed,
    Object? team1_name = freezed,
    Object? team2_id = freezed,
    Object? team2_name = freezed,
    Object? bo = freezed,
    Object? match_event = freezed,
    Object? team1_details = freezed,
    Object? team2_details = freezed,
    Object? h2h = freezed,
    Object? same_opponent = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      match_date: match_date == freezed
          ? _value.match_date
          : match_date // ignore: cast_nullable_to_non_nullable
              as DateTime,
      team1_id: team1_id == freezed
          ? _value.team1_id
          : team1_id // ignore: cast_nullable_to_non_nullable
              as int,
      team1_name: team1_name == freezed
          ? _value.team1_name
          : team1_name // ignore: cast_nullable_to_non_nullable
              as String,
      team2_id: team2_id == freezed
          ? _value.team2_id
          : team2_id // ignore: cast_nullable_to_non_nullable
              as int,
      team2_name: team2_name == freezed
          ? _value.team2_name
          : team2_name // ignore: cast_nullable_to_non_nullable
              as String,
      bo: bo == freezed
          ? _value.bo
          : bo // ignore: cast_nullable_to_non_nullable
              as String?,
      match_event: match_event == freezed
          ? _value.match_event
          : match_event // ignore: cast_nullable_to_non_nullable
              as String?,
      team1_details: team1_details == freezed
          ? _value.team1_details
          : team1_details // ignore: cast_nullable_to_non_nullable
              as List<GameDetail>?,
      team2_details: team2_details == freezed
          ? _value.team2_details
          : team2_details // ignore: cast_nullable_to_non_nullable
              as List<GameDetail>?,
      h2h: h2h == freezed
          ? _value.h2h
          : h2h // ignore: cast_nullable_to_non_nullable
              as List<GameDetail>?,
      same_opponent: same_opponent == freezed
          ? _value.same_opponent
          : same_opponent // ignore: cast_nullable_to_non_nullable
              as List<GameDetail>?,
    ));
  }
}

/// @nodoc
abstract class _$GameCopyWith<$Res> implements $GameCopyWith<$Res> {
  factory _$GameCopyWith(_Game value, $Res Function(_Game) then) =
      __$GameCopyWithImpl<$Res>;
  @override
  $Res call(
      {int id,
      DateTime match_date,
      int team1_id,
      String team1_name,
      int team2_id,
      String team2_name,
      String? bo,
      String? match_event,
      List<GameDetail>? team1_details,
      List<GameDetail>? team2_details,
      List<GameDetail>? h2h,
      List<GameDetail>? same_opponent});
}

/// @nodoc
class __$GameCopyWithImpl<$Res> extends _$GameCopyWithImpl<$Res>
    implements _$GameCopyWith<$Res> {
  __$GameCopyWithImpl(_Game _value, $Res Function(_Game) _then)
      : super(_value, (v) => _then(v as _Game));

  @override
  _Game get _value => super._value as _Game;

  @override
  $Res call({
    Object? id = freezed,
    Object? match_date = freezed,
    Object? team1_id = freezed,
    Object? team1_name = freezed,
    Object? team2_id = freezed,
    Object? team2_name = freezed,
    Object? bo = freezed,
    Object? match_event = freezed,
    Object? team1_details = freezed,
    Object? team2_details = freezed,
    Object? h2h = freezed,
    Object? same_opponent = freezed,
  }) {
    return _then(_Game(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      match_date: match_date == freezed
          ? _value.match_date
          : match_date // ignore: cast_nullable_to_non_nullable
              as DateTime,
      team1_id: team1_id == freezed
          ? _value.team1_id
          : team1_id // ignore: cast_nullable_to_non_nullable
              as int,
      team1_name: team1_name == freezed
          ? _value.team1_name
          : team1_name // ignore: cast_nullable_to_non_nullable
              as String,
      team2_id: team2_id == freezed
          ? _value.team2_id
          : team2_id // ignore: cast_nullable_to_non_nullable
              as int,
      team2_name: team2_name == freezed
          ? _value.team2_name
          : team2_name // ignore: cast_nullable_to_non_nullable
              as String,
      bo: bo == freezed
          ? _value.bo
          : bo // ignore: cast_nullable_to_non_nullable
              as String?,
      match_event: match_event == freezed
          ? _value.match_event
          : match_event // ignore: cast_nullable_to_non_nullable
              as String?,
      team1_details: team1_details == freezed
          ? _value.team1_details
          : team1_details // ignore: cast_nullable_to_non_nullable
              as List<GameDetail>?,
      team2_details: team2_details == freezed
          ? _value.team2_details
          : team2_details // ignore: cast_nullable_to_non_nullable
              as List<GameDetail>?,
      h2h: h2h == freezed
          ? _value.h2h
          : h2h // ignore: cast_nullable_to_non_nullable
              as List<GameDetail>?,
      same_opponent: same_opponent == freezed
          ? _value.same_opponent
          : same_opponent // ignore: cast_nullable_to_non_nullable
              as List<GameDetail>?,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Game extends _Game {
  _$_Game(
      {required this.id,
      required this.match_date,
      required this.team1_id,
      required this.team1_name,
      required this.team2_id,
      required this.team2_name,
      this.bo,
      this.match_event,
      this.team1_details,
      this.team2_details,
      this.h2h,
      this.same_opponent})
      : super._();

  factory _$_Game.fromJson(Map<String, dynamic> json) =>
      _$_$_GameFromJson(json);

  @override
  final int id;
  @override
  final DateTime match_date;
  @override
  final int team1_id;
  @override
  final String team1_name;
  @override
  final int team2_id;
  @override
  final String team2_name;
  @override
  final String? bo;
  @override
  final String? match_event;
  @override
  final List<GameDetail>? team1_details;
  @override
  final List<GameDetail>? team2_details;
  @override
  final List<GameDetail>? h2h;
  @override
  final List<GameDetail>? same_opponent;

  @override
  String toString() {
    return 'Game(id: $id, match_date: $match_date, team1_id: $team1_id, team1_name: $team1_name, team2_id: $team2_id, team2_name: $team2_name, bo: $bo, match_event: $match_event, team1_details: $team1_details, team2_details: $team2_details, h2h: $h2h, same_opponent: $same_opponent)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Game &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.match_date, match_date) ||
                const DeepCollectionEquality()
                    .equals(other.match_date, match_date)) &&
            (identical(other.team1_id, team1_id) ||
                const DeepCollectionEquality()
                    .equals(other.team1_id, team1_id)) &&
            (identical(other.team1_name, team1_name) ||
                const DeepCollectionEquality()
                    .equals(other.team1_name, team1_name)) &&
            (identical(other.team2_id, team2_id) ||
                const DeepCollectionEquality()
                    .equals(other.team2_id, team2_id)) &&
            (identical(other.team2_name, team2_name) ||
                const DeepCollectionEquality()
                    .equals(other.team2_name, team2_name)) &&
            (identical(other.bo, bo) ||
                const DeepCollectionEquality().equals(other.bo, bo)) &&
            (identical(other.match_event, match_event) ||
                const DeepCollectionEquality()
                    .equals(other.match_event, match_event)) &&
            (identical(other.team1_details, team1_details) ||
                const DeepCollectionEquality()
                    .equals(other.team1_details, team1_details)) &&
            (identical(other.team2_details, team2_details) ||
                const DeepCollectionEquality()
                    .equals(other.team2_details, team2_details)) &&
            (identical(other.h2h, h2h) ||
                const DeepCollectionEquality().equals(other.h2h, h2h)) &&
            (identical(other.same_opponent, same_opponent) ||
                const DeepCollectionEquality()
                    .equals(other.same_opponent, same_opponent)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(match_date) ^
      const DeepCollectionEquality().hash(team1_id) ^
      const DeepCollectionEquality().hash(team1_name) ^
      const DeepCollectionEquality().hash(team2_id) ^
      const DeepCollectionEquality().hash(team2_name) ^
      const DeepCollectionEquality().hash(bo) ^
      const DeepCollectionEquality().hash(match_event) ^
      const DeepCollectionEquality().hash(team1_details) ^
      const DeepCollectionEquality().hash(team2_details) ^
      const DeepCollectionEquality().hash(h2h) ^
      const DeepCollectionEquality().hash(same_opponent);

  @JsonKey(ignore: true)
  @override
  _$GameCopyWith<_Game> get copyWith =>
      __$GameCopyWithImpl<_Game>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_GameToJson(this);
  }
}

abstract class _Game extends Game {
  factory _Game(
      {required int id,
      required DateTime match_date,
      required int team1_id,
      required String team1_name,
      required int team2_id,
      required String team2_name,
      String? bo,
      String? match_event,
      List<GameDetail>? team1_details,
      List<GameDetail>? team2_details,
      List<GameDetail>? h2h,
      List<GameDetail>? same_opponent}) = _$_Game;
  _Game._() : super._();

  factory _Game.fromJson(Map<String, dynamic> json) = _$_Game.fromJson;

  @override
  int get id => throw _privateConstructorUsedError;
  @override
  DateTime get match_date => throw _privateConstructorUsedError;
  @override
  int get team1_id => throw _privateConstructorUsedError;
  @override
  String get team1_name => throw _privateConstructorUsedError;
  @override
  int get team2_id => throw _privateConstructorUsedError;
  @override
  String get team2_name => throw _privateConstructorUsedError;
  @override
  String? get bo => throw _privateConstructorUsedError;
  @override
  String? get match_event => throw _privateConstructorUsedError;
  @override
  List<GameDetail>? get team1_details => throw _privateConstructorUsedError;
  @override
  List<GameDetail>? get team2_details => throw _privateConstructorUsedError;
  @override
  List<GameDetail>? get h2h => throw _privateConstructorUsedError;
  @override
  List<GameDetail>? get same_opponent => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$GameCopyWith<_Game> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
class _$GameStatTearOff {
  const _$GameStatTearOff();

  FGameStatSeries series(
      {String? name = 'Series',
      List<GameResult>? team1Series,
      List<GameResult>? team2Series,
      Map<String, List<GameDetail>>? h2h,
      Map<String, List<GameDetail>>? sameOpponent}) {
    return FGameStatSeries(
      name: name,
      team1Series: team1Series,
      team2Series: team2Series,
      h2h: h2h,
      sameOpponent: sameOpponent,
    );
  }

  FGameStatPistol pistol(
      {String? name = 'Pistol',
      GameStatPistol? team1,
      GameStatPistol? team2,
      List<GameDetail>? h2h,
      List<GameDetail>? sameOpponent}) {
    return FGameStatPistol(
      name: name,
      team1: team1,
      team2: team2,
      h2h: h2h,
      sameOpponent: sameOpponent,
    );
  }
}

/// @nodoc
const $GameStat = _$GameStatTearOff();

/// @nodoc
mixin _$GameStat {
  String? get name => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            String? name,
            List<GameResult>? team1Series,
            List<GameResult>? team2Series,
            Map<String, List<GameDetail>>? h2h,
            Map<String, List<GameDetail>>? sameOpponent)
        series,
    required TResult Function(
            String? name,
            GameStatPistol? team1,
            GameStatPistol? team2,
            List<GameDetail>? h2h,
            List<GameDetail>? sameOpponent)
        pistol,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            String? name,
            List<GameResult>? team1Series,
            List<GameResult>? team2Series,
            Map<String, List<GameDetail>>? h2h,
            Map<String, List<GameDetail>>? sameOpponent)?
        series,
    TResult Function(String? name, GameStatPistol? team1, GameStatPistol? team2,
            List<GameDetail>? h2h, List<GameDetail>? sameOpponent)?
        pistol,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FGameStatSeries value) series,
    required TResult Function(FGameStatPistol value) pistol,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FGameStatSeries value)? series,
    TResult Function(FGameStatPistol value)? pistol,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $GameStatCopyWith<GameStat> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GameStatCopyWith<$Res> {
  factory $GameStatCopyWith(GameStat value, $Res Function(GameStat) then) =
      _$GameStatCopyWithImpl<$Res>;
  $Res call({String? name});
}

/// @nodoc
class _$GameStatCopyWithImpl<$Res> implements $GameStatCopyWith<$Res> {
  _$GameStatCopyWithImpl(this._value, this._then);

  final GameStat _value;
  // ignore: unused_field
  final $Res Function(GameStat) _then;

  @override
  $Res call({
    Object? name = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class $FGameStatSeriesCopyWith<$Res>
    implements $GameStatCopyWith<$Res> {
  factory $FGameStatSeriesCopyWith(
          FGameStatSeries value, $Res Function(FGameStatSeries) then) =
      _$FGameStatSeriesCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? name,
      List<GameResult>? team1Series,
      List<GameResult>? team2Series,
      Map<String, List<GameDetail>>? h2h,
      Map<String, List<GameDetail>>? sameOpponent});
}

/// @nodoc
class _$FGameStatSeriesCopyWithImpl<$Res> extends _$GameStatCopyWithImpl<$Res>
    implements $FGameStatSeriesCopyWith<$Res> {
  _$FGameStatSeriesCopyWithImpl(
      FGameStatSeries _value, $Res Function(FGameStatSeries) _then)
      : super(_value, (v) => _then(v as FGameStatSeries));

  @override
  FGameStatSeries get _value => super._value as FGameStatSeries;

  @override
  $Res call({
    Object? name = freezed,
    Object? team1Series = freezed,
    Object? team2Series = freezed,
    Object? h2h = freezed,
    Object? sameOpponent = freezed,
  }) {
    return _then(FGameStatSeries(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      team1Series: team1Series == freezed
          ? _value.team1Series
          : team1Series // ignore: cast_nullable_to_non_nullable
              as List<GameResult>?,
      team2Series: team2Series == freezed
          ? _value.team2Series
          : team2Series // ignore: cast_nullable_to_non_nullable
              as List<GameResult>?,
      h2h: h2h == freezed
          ? _value.h2h
          : h2h // ignore: cast_nullable_to_non_nullable
              as Map<String, List<GameDetail>>?,
      sameOpponent: sameOpponent == freezed
          ? _value.sameOpponent
          : sameOpponent // ignore: cast_nullable_to_non_nullable
              as Map<String, List<GameDetail>>?,
    ));
  }
}

/// @nodoc
class _$FGameStatSeries implements FGameStatSeries {
  const _$FGameStatSeries(
      {this.name = 'Series',
      this.team1Series,
      this.team2Series,
      this.h2h,
      this.sameOpponent});

  @JsonKey(defaultValue: 'Series')
  @override
  final String? name;
  @override
  final List<GameResult>? team1Series;
  @override
  final List<GameResult>? team2Series;
  @override
  final Map<String, List<GameDetail>>? h2h;
  @override
  final Map<String, List<GameDetail>>? sameOpponent;

  @override
  String toString() {
    return 'GameStat.series(name: $name, team1Series: $team1Series, team2Series: $team2Series, h2h: $h2h, sameOpponent: $sameOpponent)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is FGameStatSeries &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.team1Series, team1Series) ||
                const DeepCollectionEquality()
                    .equals(other.team1Series, team1Series)) &&
            (identical(other.team2Series, team2Series) ||
                const DeepCollectionEquality()
                    .equals(other.team2Series, team2Series)) &&
            (identical(other.h2h, h2h) ||
                const DeepCollectionEquality().equals(other.h2h, h2h)) &&
            (identical(other.sameOpponent, sameOpponent) ||
                const DeepCollectionEquality()
                    .equals(other.sameOpponent, sameOpponent)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(team1Series) ^
      const DeepCollectionEquality().hash(team2Series) ^
      const DeepCollectionEquality().hash(h2h) ^
      const DeepCollectionEquality().hash(sameOpponent);

  @JsonKey(ignore: true)
  @override
  $FGameStatSeriesCopyWith<FGameStatSeries> get copyWith =>
      _$FGameStatSeriesCopyWithImpl<FGameStatSeries>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            String? name,
            List<GameResult>? team1Series,
            List<GameResult>? team2Series,
            Map<String, List<GameDetail>>? h2h,
            Map<String, List<GameDetail>>? sameOpponent)
        series,
    required TResult Function(
            String? name,
            GameStatPistol? team1,
            GameStatPistol? team2,
            List<GameDetail>? h2h,
            List<GameDetail>? sameOpponent)
        pistol,
  }) {
    return series(name, team1Series, team2Series, h2h, sameOpponent);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            String? name,
            List<GameResult>? team1Series,
            List<GameResult>? team2Series,
            Map<String, List<GameDetail>>? h2h,
            Map<String, List<GameDetail>>? sameOpponent)?
        series,
    TResult Function(String? name, GameStatPistol? team1, GameStatPistol? team2,
            List<GameDetail>? h2h, List<GameDetail>? sameOpponent)?
        pistol,
    required TResult orElse(),
  }) {
    if (series != null) {
      return series(name, team1Series, team2Series, h2h, sameOpponent);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FGameStatSeries value) series,
    required TResult Function(FGameStatPistol value) pistol,
  }) {
    return series(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FGameStatSeries value)? series,
    TResult Function(FGameStatPistol value)? pistol,
    required TResult orElse(),
  }) {
    if (series != null) {
      return series(this);
    }
    return orElse();
  }
}

abstract class FGameStatSeries implements GameStat {
  const factory FGameStatSeries(
      {String? name,
      List<GameResult>? team1Series,
      List<GameResult>? team2Series,
      Map<String, List<GameDetail>>? h2h,
      Map<String, List<GameDetail>>? sameOpponent}) = _$FGameStatSeries;

  @override
  String? get name => throw _privateConstructorUsedError;
  List<GameResult>? get team1Series => throw _privateConstructorUsedError;
  List<GameResult>? get team2Series => throw _privateConstructorUsedError;
  Map<String, List<GameDetail>>? get h2h => throw _privateConstructorUsedError;
  Map<String, List<GameDetail>>? get sameOpponent =>
      throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $FGameStatSeriesCopyWith<FGameStatSeries> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FGameStatPistolCopyWith<$Res>
    implements $GameStatCopyWith<$Res> {
  factory $FGameStatPistolCopyWith(
          FGameStatPistol value, $Res Function(FGameStatPistol) then) =
      _$FGameStatPistolCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? name,
      GameStatPistol? team1,
      GameStatPistol? team2,
      List<GameDetail>? h2h,
      List<GameDetail>? sameOpponent});
}

/// @nodoc
class _$FGameStatPistolCopyWithImpl<$Res> extends _$GameStatCopyWithImpl<$Res>
    implements $FGameStatPistolCopyWith<$Res> {
  _$FGameStatPistolCopyWithImpl(
      FGameStatPistol _value, $Res Function(FGameStatPistol) _then)
      : super(_value, (v) => _then(v as FGameStatPistol));

  @override
  FGameStatPistol get _value => super._value as FGameStatPistol;

  @override
  $Res call({
    Object? name = freezed,
    Object? team1 = freezed,
    Object? team2 = freezed,
    Object? h2h = freezed,
    Object? sameOpponent = freezed,
  }) {
    return _then(FGameStatPistol(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      team1: team1 == freezed
          ? _value.team1
          : team1 // ignore: cast_nullable_to_non_nullable
              as GameStatPistol?,
      team2: team2 == freezed
          ? _value.team2
          : team2 // ignore: cast_nullable_to_non_nullable
              as GameStatPistol?,
      h2h: h2h == freezed
          ? _value.h2h
          : h2h // ignore: cast_nullable_to_non_nullable
              as List<GameDetail>?,
      sameOpponent: sameOpponent == freezed
          ? _value.sameOpponent
          : sameOpponent // ignore: cast_nullable_to_non_nullable
              as List<GameDetail>?,
    ));
  }
}

/// @nodoc
class _$FGameStatPistol implements FGameStatPistol {
  const _$FGameStatPistol(
      {this.name = 'Pistol',
      this.team1,
      this.team2,
      this.h2h,
      this.sameOpponent});

  @JsonKey(defaultValue: 'Pistol')
  @override
  final String? name;
  @override
  final GameStatPistol? team1;
  @override
  final GameStatPistol? team2;
  @override
  final List<GameDetail>? h2h;
  @override
  final List<GameDetail>? sameOpponent;

  @override
  String toString() {
    return 'GameStat.pistol(name: $name, team1: $team1, team2: $team2, h2h: $h2h, sameOpponent: $sameOpponent)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is FGameStatPistol &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.team1, team1) ||
                const DeepCollectionEquality().equals(other.team1, team1)) &&
            (identical(other.team2, team2) ||
                const DeepCollectionEquality().equals(other.team2, team2)) &&
            (identical(other.h2h, h2h) ||
                const DeepCollectionEquality().equals(other.h2h, h2h)) &&
            (identical(other.sameOpponent, sameOpponent) ||
                const DeepCollectionEquality()
                    .equals(other.sameOpponent, sameOpponent)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(team1) ^
      const DeepCollectionEquality().hash(team2) ^
      const DeepCollectionEquality().hash(h2h) ^
      const DeepCollectionEquality().hash(sameOpponent);

  @JsonKey(ignore: true)
  @override
  $FGameStatPistolCopyWith<FGameStatPistol> get copyWith =>
      _$FGameStatPistolCopyWithImpl<FGameStatPistol>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            String? name,
            List<GameResult>? team1Series,
            List<GameResult>? team2Series,
            Map<String, List<GameDetail>>? h2h,
            Map<String, List<GameDetail>>? sameOpponent)
        series,
    required TResult Function(
            String? name,
            GameStatPistol? team1,
            GameStatPistol? team2,
            List<GameDetail>? h2h,
            List<GameDetail>? sameOpponent)
        pistol,
  }) {
    return pistol(name, team1, team2, h2h, sameOpponent);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            String? name,
            List<GameResult>? team1Series,
            List<GameResult>? team2Series,
            Map<String, List<GameDetail>>? h2h,
            Map<String, List<GameDetail>>? sameOpponent)?
        series,
    TResult Function(String? name, GameStatPistol? team1, GameStatPistol? team2,
            List<GameDetail>? h2h, List<GameDetail>? sameOpponent)?
        pistol,
    required TResult orElse(),
  }) {
    if (pistol != null) {
      return pistol(name, team1, team2, h2h, sameOpponent);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FGameStatSeries value) series,
    required TResult Function(FGameStatPistol value) pistol,
  }) {
    return pistol(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FGameStatSeries value)? series,
    TResult Function(FGameStatPistol value)? pistol,
    required TResult orElse(),
  }) {
    if (pistol != null) {
      return pistol(this);
    }
    return orElse();
  }
}

abstract class FGameStatPistol implements GameStat {
  const factory FGameStatPistol(
      {String? name,
      GameStatPistol? team1,
      GameStatPistol? team2,
      List<GameDetail>? h2h,
      List<GameDetail>? sameOpponent}) = _$FGameStatPistol;

  @override
  String? get name => throw _privateConstructorUsedError;
  GameStatPistol? get team1 => throw _privateConstructorUsedError;
  GameStatPistol? get team2 => throw _privateConstructorUsedError;
  List<GameDetail>? get h2h => throw _privateConstructorUsedError;
  List<GameDetail>? get sameOpponent => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $FGameStatPistolCopyWith<FGameStatPistol> get copyWith =>
      throw _privateConstructorUsedError;
}

GameDetail _$GameDetailFromJson(Map<String, dynamic> json) {
  return _GameDetail.fromJson(json);
}

/// @nodoc
class _$GameDetailTearOff {
  const _$GameDetailTearOff();

  _GameDetail call(
      {required DateTime match_date,
      required int match_id,
      required int map_stat_id,
      bool? is_favourite,
      required int map_round,
      required String mapstat_name,
      required int team_id,
      int? team_rank,
      required String team_name,
      required bool team_win,
      int? team_score = 0,
      required String team_round_history,
      int? team_clutch_score,
      bool? team_pick,
      required bool team_1st_half_winner,
      int? team_clutch_1st_half,
      required bool team_win_at_pistol_1,
      required bool team_win_at_pistol_16,
      required int opponent_id,
      required int opponent_rank,
      required String opponent_name,
      required bool opponent_win,
      int? opponent_score = 0,
      required String opponent_round_history,
      required int opponent_clutch_score,
      bool? opponent_pick,
      required bool opponent_1st_half_winner,
      int? opponent_clutch_1st_half,
      required bool opponent_win_at_pistol_1,
      required bool opponent_win_at_pistol_16,
      required int team_round_1,
      required int team_round_16,
      required int opponent_round_1,
      required int opponent_round_16,
      required bool team_round_1_ct_side,
      required bool team_round_16_ct_side}) {
    return _GameDetail(
      match_date: match_date,
      match_id: match_id,
      map_stat_id: map_stat_id,
      is_favourite: is_favourite,
      map_round: map_round,
      mapstat_name: mapstat_name,
      team_id: team_id,
      team_rank: team_rank,
      team_name: team_name,
      team_win: team_win,
      team_score: team_score,
      team_round_history: team_round_history,
      team_clutch_score: team_clutch_score,
      team_pick: team_pick,
      team_1st_half_winner: team_1st_half_winner,
      team_clutch_1st_half: team_clutch_1st_half,
      team_win_at_pistol_1: team_win_at_pistol_1,
      team_win_at_pistol_16: team_win_at_pistol_16,
      opponent_id: opponent_id,
      opponent_rank: opponent_rank,
      opponent_name: opponent_name,
      opponent_win: opponent_win,
      opponent_score: opponent_score,
      opponent_round_history: opponent_round_history,
      opponent_clutch_score: opponent_clutch_score,
      opponent_pick: opponent_pick,
      opponent_1st_half_winner: opponent_1st_half_winner,
      opponent_clutch_1st_half: opponent_clutch_1st_half,
      opponent_win_at_pistol_1: opponent_win_at_pistol_1,
      opponent_win_at_pistol_16: opponent_win_at_pistol_16,
      team_round_1: team_round_1,
      team_round_16: team_round_16,
      opponent_round_1: opponent_round_1,
      opponent_round_16: opponent_round_16,
      team_round_1_ct_side: team_round_1_ct_side,
      team_round_16_ct_side: team_round_16_ct_side,
    );
  }

  GameDetail fromJson(Map<String, Object> json) {
    return GameDetail.fromJson(json);
  }
}

/// @nodoc
const $GameDetail = _$GameDetailTearOff();

/// @nodoc
mixin _$GameDetail {
  DateTime get match_date => throw _privateConstructorUsedError;
  int get match_id => throw _privateConstructorUsedError;
  int get map_stat_id => throw _privateConstructorUsedError;
  bool? get is_favourite => throw _privateConstructorUsedError;
  int get map_round => throw _privateConstructorUsedError;
  String get mapstat_name => throw _privateConstructorUsedError;
  int get team_id => throw _privateConstructorUsedError;
  int? get team_rank => throw _privateConstructorUsedError;
  String get team_name => throw _privateConstructorUsedError;
  bool get team_win => throw _privateConstructorUsedError;
  int? get team_score => throw _privateConstructorUsedError;
  String get team_round_history => throw _privateConstructorUsedError;
  int? get team_clutch_score => throw _privateConstructorUsedError;
  bool? get team_pick => throw _privateConstructorUsedError;
  bool get team_1st_half_winner => throw _privateConstructorUsedError;
  int? get team_clutch_1st_half => throw _privateConstructorUsedError;
  bool get team_win_at_pistol_1 => throw _privateConstructorUsedError;
  bool get team_win_at_pistol_16 => throw _privateConstructorUsedError;
  int get opponent_id => throw _privateConstructorUsedError;
  int get opponent_rank => throw _privateConstructorUsedError;
  String get opponent_name => throw _privateConstructorUsedError;
  bool get opponent_win => throw _privateConstructorUsedError;
  int? get opponent_score => throw _privateConstructorUsedError;
  String get opponent_round_history => throw _privateConstructorUsedError;
  int get opponent_clutch_score => throw _privateConstructorUsedError;
  bool? get opponent_pick => throw _privateConstructorUsedError;
  bool get opponent_1st_half_winner => throw _privateConstructorUsedError;
  int? get opponent_clutch_1st_half => throw _privateConstructorUsedError;
  bool get opponent_win_at_pistol_1 => throw _privateConstructorUsedError;
  bool get opponent_win_at_pistol_16 => throw _privateConstructorUsedError;
  int get team_round_1 => throw _privateConstructorUsedError;
  int get team_round_16 => throw _privateConstructorUsedError;
  int get opponent_round_1 => throw _privateConstructorUsedError;
  int get opponent_round_16 => throw _privateConstructorUsedError;
  bool get team_round_1_ct_side => throw _privateConstructorUsedError;
  bool get team_round_16_ct_side => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $GameDetailCopyWith<GameDetail> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GameDetailCopyWith<$Res> {
  factory $GameDetailCopyWith(
          GameDetail value, $Res Function(GameDetail) then) =
      _$GameDetailCopyWithImpl<$Res>;
  $Res call(
      {DateTime match_date,
      int match_id,
      int map_stat_id,
      bool? is_favourite,
      int map_round,
      String mapstat_name,
      int team_id,
      int? team_rank,
      String team_name,
      bool team_win,
      int? team_score,
      String team_round_history,
      int? team_clutch_score,
      bool? team_pick,
      bool team_1st_half_winner,
      int? team_clutch_1st_half,
      bool team_win_at_pistol_1,
      bool team_win_at_pistol_16,
      int opponent_id,
      int opponent_rank,
      String opponent_name,
      bool opponent_win,
      int? opponent_score,
      String opponent_round_history,
      int opponent_clutch_score,
      bool? opponent_pick,
      bool opponent_1st_half_winner,
      int? opponent_clutch_1st_half,
      bool opponent_win_at_pistol_1,
      bool opponent_win_at_pistol_16,
      int team_round_1,
      int team_round_16,
      int opponent_round_1,
      int opponent_round_16,
      bool team_round_1_ct_side,
      bool team_round_16_ct_side});
}

/// @nodoc
class _$GameDetailCopyWithImpl<$Res> implements $GameDetailCopyWith<$Res> {
  _$GameDetailCopyWithImpl(this._value, this._then);

  final GameDetail _value;
  // ignore: unused_field
  final $Res Function(GameDetail) _then;

  @override
  $Res call({
    Object? match_date = freezed,
    Object? match_id = freezed,
    Object? map_stat_id = freezed,
    Object? is_favourite = freezed,
    Object? map_round = freezed,
    Object? mapstat_name = freezed,
    Object? team_id = freezed,
    Object? team_rank = freezed,
    Object? team_name = freezed,
    Object? team_win = freezed,
    Object? team_score = freezed,
    Object? team_round_history = freezed,
    Object? team_clutch_score = freezed,
    Object? team_pick = freezed,
    Object? team_1st_half_winner = freezed,
    Object? team_clutch_1st_half = freezed,
    Object? team_win_at_pistol_1 = freezed,
    Object? team_win_at_pistol_16 = freezed,
    Object? opponent_id = freezed,
    Object? opponent_rank = freezed,
    Object? opponent_name = freezed,
    Object? opponent_win = freezed,
    Object? opponent_score = freezed,
    Object? opponent_round_history = freezed,
    Object? opponent_clutch_score = freezed,
    Object? opponent_pick = freezed,
    Object? opponent_1st_half_winner = freezed,
    Object? opponent_clutch_1st_half = freezed,
    Object? opponent_win_at_pistol_1 = freezed,
    Object? opponent_win_at_pistol_16 = freezed,
    Object? team_round_1 = freezed,
    Object? team_round_16 = freezed,
    Object? opponent_round_1 = freezed,
    Object? opponent_round_16 = freezed,
    Object? team_round_1_ct_side = freezed,
    Object? team_round_16_ct_side = freezed,
  }) {
    return _then(_value.copyWith(
      match_date: match_date == freezed
          ? _value.match_date
          : match_date // ignore: cast_nullable_to_non_nullable
              as DateTime,
      match_id: match_id == freezed
          ? _value.match_id
          : match_id // ignore: cast_nullable_to_non_nullable
              as int,
      map_stat_id: map_stat_id == freezed
          ? _value.map_stat_id
          : map_stat_id // ignore: cast_nullable_to_non_nullable
              as int,
      is_favourite: is_favourite == freezed
          ? _value.is_favourite
          : is_favourite // ignore: cast_nullable_to_non_nullable
              as bool?,
      map_round: map_round == freezed
          ? _value.map_round
          : map_round // ignore: cast_nullable_to_non_nullable
              as int,
      mapstat_name: mapstat_name == freezed
          ? _value.mapstat_name
          : mapstat_name // ignore: cast_nullable_to_non_nullable
              as String,
      team_id: team_id == freezed
          ? _value.team_id
          : team_id // ignore: cast_nullable_to_non_nullable
              as int,
      team_rank: team_rank == freezed
          ? _value.team_rank
          : team_rank // ignore: cast_nullable_to_non_nullable
              as int?,
      team_name: team_name == freezed
          ? _value.team_name
          : team_name // ignore: cast_nullable_to_non_nullable
              as String,
      team_win: team_win == freezed
          ? _value.team_win
          : team_win // ignore: cast_nullable_to_non_nullable
              as bool,
      team_score: team_score == freezed
          ? _value.team_score
          : team_score // ignore: cast_nullable_to_non_nullable
              as int?,
      team_round_history: team_round_history == freezed
          ? _value.team_round_history
          : team_round_history // ignore: cast_nullable_to_non_nullable
              as String,
      team_clutch_score: team_clutch_score == freezed
          ? _value.team_clutch_score
          : team_clutch_score // ignore: cast_nullable_to_non_nullable
              as int?,
      team_pick: team_pick == freezed
          ? _value.team_pick
          : team_pick // ignore: cast_nullable_to_non_nullable
              as bool?,
      team_1st_half_winner: team_1st_half_winner == freezed
          ? _value.team_1st_half_winner
          : team_1st_half_winner // ignore: cast_nullable_to_non_nullable
              as bool,
      team_clutch_1st_half: team_clutch_1st_half == freezed
          ? _value.team_clutch_1st_half
          : team_clutch_1st_half // ignore: cast_nullable_to_non_nullable
              as int?,
      team_win_at_pistol_1: team_win_at_pistol_1 == freezed
          ? _value.team_win_at_pistol_1
          : team_win_at_pistol_1 // ignore: cast_nullable_to_non_nullable
              as bool,
      team_win_at_pistol_16: team_win_at_pistol_16 == freezed
          ? _value.team_win_at_pistol_16
          : team_win_at_pistol_16 // ignore: cast_nullable_to_non_nullable
              as bool,
      opponent_id: opponent_id == freezed
          ? _value.opponent_id
          : opponent_id // ignore: cast_nullable_to_non_nullable
              as int,
      opponent_rank: opponent_rank == freezed
          ? _value.opponent_rank
          : opponent_rank // ignore: cast_nullable_to_non_nullable
              as int,
      opponent_name: opponent_name == freezed
          ? _value.opponent_name
          : opponent_name // ignore: cast_nullable_to_non_nullable
              as String,
      opponent_win: opponent_win == freezed
          ? _value.opponent_win
          : opponent_win // ignore: cast_nullable_to_non_nullable
              as bool,
      opponent_score: opponent_score == freezed
          ? _value.opponent_score
          : opponent_score // ignore: cast_nullable_to_non_nullable
              as int?,
      opponent_round_history: opponent_round_history == freezed
          ? _value.opponent_round_history
          : opponent_round_history // ignore: cast_nullable_to_non_nullable
              as String,
      opponent_clutch_score: opponent_clutch_score == freezed
          ? _value.opponent_clutch_score
          : opponent_clutch_score // ignore: cast_nullable_to_non_nullable
              as int,
      opponent_pick: opponent_pick == freezed
          ? _value.opponent_pick
          : opponent_pick // ignore: cast_nullable_to_non_nullable
              as bool?,
      opponent_1st_half_winner: opponent_1st_half_winner == freezed
          ? _value.opponent_1st_half_winner
          : opponent_1st_half_winner // ignore: cast_nullable_to_non_nullable
              as bool,
      opponent_clutch_1st_half: opponent_clutch_1st_half == freezed
          ? _value.opponent_clutch_1st_half
          : opponent_clutch_1st_half // ignore: cast_nullable_to_non_nullable
              as int?,
      opponent_win_at_pistol_1: opponent_win_at_pistol_1 == freezed
          ? _value.opponent_win_at_pistol_1
          : opponent_win_at_pistol_1 // ignore: cast_nullable_to_non_nullable
              as bool,
      opponent_win_at_pistol_16: opponent_win_at_pistol_16 == freezed
          ? _value.opponent_win_at_pistol_16
          : opponent_win_at_pistol_16 // ignore: cast_nullable_to_non_nullable
              as bool,
      team_round_1: team_round_1 == freezed
          ? _value.team_round_1
          : team_round_1 // ignore: cast_nullable_to_non_nullable
              as int,
      team_round_16: team_round_16 == freezed
          ? _value.team_round_16
          : team_round_16 // ignore: cast_nullable_to_non_nullable
              as int,
      opponent_round_1: opponent_round_1 == freezed
          ? _value.opponent_round_1
          : opponent_round_1 // ignore: cast_nullable_to_non_nullable
              as int,
      opponent_round_16: opponent_round_16 == freezed
          ? _value.opponent_round_16
          : opponent_round_16 // ignore: cast_nullable_to_non_nullable
              as int,
      team_round_1_ct_side: team_round_1_ct_side == freezed
          ? _value.team_round_1_ct_side
          : team_round_1_ct_side // ignore: cast_nullable_to_non_nullable
              as bool,
      team_round_16_ct_side: team_round_16_ct_side == freezed
          ? _value.team_round_16_ct_side
          : team_round_16_ct_side // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$GameDetailCopyWith<$Res> implements $GameDetailCopyWith<$Res> {
  factory _$GameDetailCopyWith(
          _GameDetail value, $Res Function(_GameDetail) then) =
      __$GameDetailCopyWithImpl<$Res>;
  @override
  $Res call(
      {DateTime match_date,
      int match_id,
      int map_stat_id,
      bool? is_favourite,
      int map_round,
      String mapstat_name,
      int team_id,
      int? team_rank,
      String team_name,
      bool team_win,
      int? team_score,
      String team_round_history,
      int? team_clutch_score,
      bool? team_pick,
      bool team_1st_half_winner,
      int? team_clutch_1st_half,
      bool team_win_at_pistol_1,
      bool team_win_at_pistol_16,
      int opponent_id,
      int opponent_rank,
      String opponent_name,
      bool opponent_win,
      int? opponent_score,
      String opponent_round_history,
      int opponent_clutch_score,
      bool? opponent_pick,
      bool opponent_1st_half_winner,
      int? opponent_clutch_1st_half,
      bool opponent_win_at_pistol_1,
      bool opponent_win_at_pistol_16,
      int team_round_1,
      int team_round_16,
      int opponent_round_1,
      int opponent_round_16,
      bool team_round_1_ct_side,
      bool team_round_16_ct_side});
}

/// @nodoc
class __$GameDetailCopyWithImpl<$Res> extends _$GameDetailCopyWithImpl<$Res>
    implements _$GameDetailCopyWith<$Res> {
  __$GameDetailCopyWithImpl(
      _GameDetail _value, $Res Function(_GameDetail) _then)
      : super(_value, (v) => _then(v as _GameDetail));

  @override
  _GameDetail get _value => super._value as _GameDetail;

  @override
  $Res call({
    Object? match_date = freezed,
    Object? match_id = freezed,
    Object? map_stat_id = freezed,
    Object? is_favourite = freezed,
    Object? map_round = freezed,
    Object? mapstat_name = freezed,
    Object? team_id = freezed,
    Object? team_rank = freezed,
    Object? team_name = freezed,
    Object? team_win = freezed,
    Object? team_score = freezed,
    Object? team_round_history = freezed,
    Object? team_clutch_score = freezed,
    Object? team_pick = freezed,
    Object? team_1st_half_winner = freezed,
    Object? team_clutch_1st_half = freezed,
    Object? team_win_at_pistol_1 = freezed,
    Object? team_win_at_pistol_16 = freezed,
    Object? opponent_id = freezed,
    Object? opponent_rank = freezed,
    Object? opponent_name = freezed,
    Object? opponent_win = freezed,
    Object? opponent_score = freezed,
    Object? opponent_round_history = freezed,
    Object? opponent_clutch_score = freezed,
    Object? opponent_pick = freezed,
    Object? opponent_1st_half_winner = freezed,
    Object? opponent_clutch_1st_half = freezed,
    Object? opponent_win_at_pistol_1 = freezed,
    Object? opponent_win_at_pistol_16 = freezed,
    Object? team_round_1 = freezed,
    Object? team_round_16 = freezed,
    Object? opponent_round_1 = freezed,
    Object? opponent_round_16 = freezed,
    Object? team_round_1_ct_side = freezed,
    Object? team_round_16_ct_side = freezed,
  }) {
    return _then(_GameDetail(
      match_date: match_date == freezed
          ? _value.match_date
          : match_date // ignore: cast_nullable_to_non_nullable
              as DateTime,
      match_id: match_id == freezed
          ? _value.match_id
          : match_id // ignore: cast_nullable_to_non_nullable
              as int,
      map_stat_id: map_stat_id == freezed
          ? _value.map_stat_id
          : map_stat_id // ignore: cast_nullable_to_non_nullable
              as int,
      is_favourite: is_favourite == freezed
          ? _value.is_favourite
          : is_favourite // ignore: cast_nullable_to_non_nullable
              as bool?,
      map_round: map_round == freezed
          ? _value.map_round
          : map_round // ignore: cast_nullable_to_non_nullable
              as int,
      mapstat_name: mapstat_name == freezed
          ? _value.mapstat_name
          : mapstat_name // ignore: cast_nullable_to_non_nullable
              as String,
      team_id: team_id == freezed
          ? _value.team_id
          : team_id // ignore: cast_nullable_to_non_nullable
              as int,
      team_rank: team_rank == freezed
          ? _value.team_rank
          : team_rank // ignore: cast_nullable_to_non_nullable
              as int?,
      team_name: team_name == freezed
          ? _value.team_name
          : team_name // ignore: cast_nullable_to_non_nullable
              as String,
      team_win: team_win == freezed
          ? _value.team_win
          : team_win // ignore: cast_nullable_to_non_nullable
              as bool,
      team_score: team_score == freezed
          ? _value.team_score
          : team_score // ignore: cast_nullable_to_non_nullable
              as int?,
      team_round_history: team_round_history == freezed
          ? _value.team_round_history
          : team_round_history // ignore: cast_nullable_to_non_nullable
              as String,
      team_clutch_score: team_clutch_score == freezed
          ? _value.team_clutch_score
          : team_clutch_score // ignore: cast_nullable_to_non_nullable
              as int?,
      team_pick: team_pick == freezed
          ? _value.team_pick
          : team_pick // ignore: cast_nullable_to_non_nullable
              as bool?,
      team_1st_half_winner: team_1st_half_winner == freezed
          ? _value.team_1st_half_winner
          : team_1st_half_winner // ignore: cast_nullable_to_non_nullable
              as bool,
      team_clutch_1st_half: team_clutch_1st_half == freezed
          ? _value.team_clutch_1st_half
          : team_clutch_1st_half // ignore: cast_nullable_to_non_nullable
              as int?,
      team_win_at_pistol_1: team_win_at_pistol_1 == freezed
          ? _value.team_win_at_pistol_1
          : team_win_at_pistol_1 // ignore: cast_nullable_to_non_nullable
              as bool,
      team_win_at_pistol_16: team_win_at_pistol_16 == freezed
          ? _value.team_win_at_pistol_16
          : team_win_at_pistol_16 // ignore: cast_nullable_to_non_nullable
              as bool,
      opponent_id: opponent_id == freezed
          ? _value.opponent_id
          : opponent_id // ignore: cast_nullable_to_non_nullable
              as int,
      opponent_rank: opponent_rank == freezed
          ? _value.opponent_rank
          : opponent_rank // ignore: cast_nullable_to_non_nullable
              as int,
      opponent_name: opponent_name == freezed
          ? _value.opponent_name
          : opponent_name // ignore: cast_nullable_to_non_nullable
              as String,
      opponent_win: opponent_win == freezed
          ? _value.opponent_win
          : opponent_win // ignore: cast_nullable_to_non_nullable
              as bool,
      opponent_score: opponent_score == freezed
          ? _value.opponent_score
          : opponent_score // ignore: cast_nullable_to_non_nullable
              as int?,
      opponent_round_history: opponent_round_history == freezed
          ? _value.opponent_round_history
          : opponent_round_history // ignore: cast_nullable_to_non_nullable
              as String,
      opponent_clutch_score: opponent_clutch_score == freezed
          ? _value.opponent_clutch_score
          : opponent_clutch_score // ignore: cast_nullable_to_non_nullable
              as int,
      opponent_pick: opponent_pick == freezed
          ? _value.opponent_pick
          : opponent_pick // ignore: cast_nullable_to_non_nullable
              as bool?,
      opponent_1st_half_winner: opponent_1st_half_winner == freezed
          ? _value.opponent_1st_half_winner
          : opponent_1st_half_winner // ignore: cast_nullable_to_non_nullable
              as bool,
      opponent_clutch_1st_half: opponent_clutch_1st_half == freezed
          ? _value.opponent_clutch_1st_half
          : opponent_clutch_1st_half // ignore: cast_nullable_to_non_nullable
              as int?,
      opponent_win_at_pistol_1: opponent_win_at_pistol_1 == freezed
          ? _value.opponent_win_at_pistol_1
          : opponent_win_at_pistol_1 // ignore: cast_nullable_to_non_nullable
              as bool,
      opponent_win_at_pistol_16: opponent_win_at_pistol_16 == freezed
          ? _value.opponent_win_at_pistol_16
          : opponent_win_at_pistol_16 // ignore: cast_nullable_to_non_nullable
              as bool,
      team_round_1: team_round_1 == freezed
          ? _value.team_round_1
          : team_round_1 // ignore: cast_nullable_to_non_nullable
              as int,
      team_round_16: team_round_16 == freezed
          ? _value.team_round_16
          : team_round_16 // ignore: cast_nullable_to_non_nullable
              as int,
      opponent_round_1: opponent_round_1 == freezed
          ? _value.opponent_round_1
          : opponent_round_1 // ignore: cast_nullable_to_non_nullable
              as int,
      opponent_round_16: opponent_round_16 == freezed
          ? _value.opponent_round_16
          : opponent_round_16 // ignore: cast_nullable_to_non_nullable
              as int,
      team_round_1_ct_side: team_round_1_ct_side == freezed
          ? _value.team_round_1_ct_side
          : team_round_1_ct_side // ignore: cast_nullable_to_non_nullable
              as bool,
      team_round_16_ct_side: team_round_16_ct_side == freezed
          ? _value.team_round_16_ct_side
          : team_round_16_ct_side // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_GameDetail extends _GameDetail {
  _$_GameDetail(
      {required this.match_date,
      required this.match_id,
      required this.map_stat_id,
      this.is_favourite,
      required this.map_round,
      required this.mapstat_name,
      required this.team_id,
      this.team_rank,
      required this.team_name,
      required this.team_win,
      this.team_score = 0,
      required this.team_round_history,
      this.team_clutch_score,
      this.team_pick,
      required this.team_1st_half_winner,
      this.team_clutch_1st_half,
      required this.team_win_at_pistol_1,
      required this.team_win_at_pistol_16,
      required this.opponent_id,
      required this.opponent_rank,
      required this.opponent_name,
      required this.opponent_win,
      this.opponent_score = 0,
      required this.opponent_round_history,
      required this.opponent_clutch_score,
      this.opponent_pick,
      required this.opponent_1st_half_winner,
      this.opponent_clutch_1st_half,
      required this.opponent_win_at_pistol_1,
      required this.opponent_win_at_pistol_16,
      required this.team_round_1,
      required this.team_round_16,
      required this.opponent_round_1,
      required this.opponent_round_16,
      required this.team_round_1_ct_side,
      required this.team_round_16_ct_side})
      : super._();

  factory _$_GameDetail.fromJson(Map<String, dynamic> json) =>
      _$_$_GameDetailFromJson(json);

  @override
  final DateTime match_date;
  @override
  final int match_id;
  @override
  final int map_stat_id;
  @override
  final bool? is_favourite;
  @override
  final int map_round;
  @override
  final String mapstat_name;
  @override
  final int team_id;
  @override
  final int? team_rank;
  @override
  final String team_name;
  @override
  final bool team_win;
  @JsonKey(defaultValue: 0)
  @override
  final int? team_score;
  @override
  final String team_round_history;
  @override
  final int? team_clutch_score;
  @override
  final bool? team_pick;
  @override
  final bool team_1st_half_winner;
  @override
  final int? team_clutch_1st_half;
  @override
  final bool team_win_at_pistol_1;
  @override
  final bool team_win_at_pistol_16;
  @override
  final int opponent_id;
  @override
  final int opponent_rank;
  @override
  final String opponent_name;
  @override
  final bool opponent_win;
  @JsonKey(defaultValue: 0)
  @override
  final int? opponent_score;
  @override
  final String opponent_round_history;
  @override
  final int opponent_clutch_score;
  @override
  final bool? opponent_pick;
  @override
  final bool opponent_1st_half_winner;
  @override
  final int? opponent_clutch_1st_half;
  @override
  final bool opponent_win_at_pistol_1;
  @override
  final bool opponent_win_at_pistol_16;
  @override
  final int team_round_1;
  @override
  final int team_round_16;
  @override
  final int opponent_round_1;
  @override
  final int opponent_round_16;
  @override
  final bool team_round_1_ct_side;
  @override
  final bool team_round_16_ct_side;

  @override
  String toString() {
    return 'GameDetail(match_date: $match_date, match_id: $match_id, map_stat_id: $map_stat_id, is_favourite: $is_favourite, map_round: $map_round, mapstat_name: $mapstat_name, team_id: $team_id, team_rank: $team_rank, team_name: $team_name, team_win: $team_win, team_score: $team_score, team_round_history: $team_round_history, team_clutch_score: $team_clutch_score, team_pick: $team_pick, team_1st_half_winner: $team_1st_half_winner, team_clutch_1st_half: $team_clutch_1st_half, team_win_at_pistol_1: $team_win_at_pistol_1, team_win_at_pistol_16: $team_win_at_pistol_16, opponent_id: $opponent_id, opponent_rank: $opponent_rank, opponent_name: $opponent_name, opponent_win: $opponent_win, opponent_score: $opponent_score, opponent_round_history: $opponent_round_history, opponent_clutch_score: $opponent_clutch_score, opponent_pick: $opponent_pick, opponent_1st_half_winner: $opponent_1st_half_winner, opponent_clutch_1st_half: $opponent_clutch_1st_half, opponent_win_at_pistol_1: $opponent_win_at_pistol_1, opponent_win_at_pistol_16: $opponent_win_at_pistol_16, team_round_1: $team_round_1, team_round_16: $team_round_16, opponent_round_1: $opponent_round_1, opponent_round_16: $opponent_round_16, team_round_1_ct_side: $team_round_1_ct_side, team_round_16_ct_side: $team_round_16_ct_side)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _GameDetail &&
            (identical(other.match_date, match_date) ||
                const DeepCollectionEquality()
                    .equals(other.match_date, match_date)) &&
            (identical(other.match_id, match_id) ||
                const DeepCollectionEquality()
                    .equals(other.match_id, match_id)) &&
            (identical(other.map_stat_id, map_stat_id) ||
                const DeepCollectionEquality()
                    .equals(other.map_stat_id, map_stat_id)) &&
            (identical(other.is_favourite, is_favourite) ||
                const DeepCollectionEquality()
                    .equals(other.is_favourite, is_favourite)) &&
            (identical(other.map_round, map_round) ||
                const DeepCollectionEquality()
                    .equals(other.map_round, map_round)) &&
            (identical(other.mapstat_name, mapstat_name) ||
                const DeepCollectionEquality()
                    .equals(other.mapstat_name, mapstat_name)) &&
            (identical(other.team_id, team_id) ||
                const DeepCollectionEquality()
                    .equals(other.team_id, team_id)) &&
            (identical(other.team_rank, team_rank) ||
                const DeepCollectionEquality()
                    .equals(other.team_rank, team_rank)) &&
            (identical(other.team_name, team_name) ||
                const DeepCollectionEquality()
                    .equals(other.team_name, team_name)) &&
            (identical(other.team_win, team_win) ||
                const DeepCollectionEquality()
                    .equals(other.team_win, team_win)) &&
            (identical(other.team_score, team_score) ||
                const DeepCollectionEquality()
                    .equals(other.team_score, team_score)) &&
            (identical(other.team_round_history, team_round_history) ||
                const DeepCollectionEquality()
                    .equals(other.team_round_history, team_round_history)) &&
            (identical(other.team_clutch_score, team_clutch_score) ||
                const DeepCollectionEquality()
                    .equals(other.team_clutch_score, team_clutch_score)) &&
            (identical(other.team_pick, team_pick) ||
                const DeepCollectionEquality()
                    .equals(other.team_pick, team_pick)) &&
            (identical(other.team_1st_half_winner, team_1st_half_winner) ||
                const DeepCollectionEquality().equals(
                    other.team_1st_half_winner, team_1st_half_winner)) &&
            (identical(other.team_clutch_1st_half, team_clutch_1st_half) ||
                const DeepCollectionEquality().equals(
                    other.team_clutch_1st_half, team_clutch_1st_half)) &&
            (identical(other.team_win_at_pistol_1, team_win_at_pistol_1) ||
                const DeepCollectionEquality().equals(
                    other.team_win_at_pistol_1, team_win_at_pistol_1)) &&
            (identical(other.team_win_at_pistol_16, team_win_at_pistol_16) ||
                const DeepCollectionEquality().equals(
                    other.team_win_at_pistol_16, team_win_at_pistol_16)) &&
            (identical(other.opponent_id, opponent_id) ||
                const DeepCollectionEquality()
                    .equals(other.opponent_id, opponent_id)) &&
            (identical(other.opponent_rank, opponent_rank) ||
                const DeepCollectionEquality()
                    .equals(other.opponent_rank, opponent_rank)) &&
            (identical(other.opponent_name, opponent_name) ||
                const DeepCollectionEquality()
                    .equals(other.opponent_name, opponent_name)) &&
            (identical(other.opponent_win, opponent_win) || const DeepCollectionEquality().equals(other.opponent_win, opponent_win)) &&
            (identical(other.opponent_score, opponent_score) || const DeepCollectionEquality().equals(other.opponent_score, opponent_score)) &&
            (identical(other.opponent_round_history, opponent_round_history) || const DeepCollectionEquality().equals(other.opponent_round_history, opponent_round_history)) &&
            (identical(other.opponent_clutch_score, opponent_clutch_score) || const DeepCollectionEquality().equals(other.opponent_clutch_score, opponent_clutch_score)) &&
            (identical(other.opponent_pick, opponent_pick) || const DeepCollectionEquality().equals(other.opponent_pick, opponent_pick)) &&
            (identical(other.opponent_1st_half_winner, opponent_1st_half_winner) || const DeepCollectionEquality().equals(other.opponent_1st_half_winner, opponent_1st_half_winner)) &&
            (identical(other.opponent_clutch_1st_half, opponent_clutch_1st_half) || const DeepCollectionEquality().equals(other.opponent_clutch_1st_half, opponent_clutch_1st_half)) &&
            (identical(other.opponent_win_at_pistol_1, opponent_win_at_pistol_1) || const DeepCollectionEquality().equals(other.opponent_win_at_pistol_1, opponent_win_at_pistol_1)) &&
            (identical(other.opponent_win_at_pistol_16, opponent_win_at_pistol_16) || const DeepCollectionEquality().equals(other.opponent_win_at_pistol_16, opponent_win_at_pistol_16)) &&
            (identical(other.team_round_1, team_round_1) || const DeepCollectionEquality().equals(other.team_round_1, team_round_1)) &&
            (identical(other.team_round_16, team_round_16) || const DeepCollectionEquality().equals(other.team_round_16, team_round_16)) &&
            (identical(other.opponent_round_1, opponent_round_1) || const DeepCollectionEquality().equals(other.opponent_round_1, opponent_round_1)) &&
            (identical(other.opponent_round_16, opponent_round_16) || const DeepCollectionEquality().equals(other.opponent_round_16, opponent_round_16)) &&
            (identical(other.team_round_1_ct_side, team_round_1_ct_side) || const DeepCollectionEquality().equals(other.team_round_1_ct_side, team_round_1_ct_side)) &&
            (identical(other.team_round_16_ct_side, team_round_16_ct_side) || const DeepCollectionEquality().equals(other.team_round_16_ct_side, team_round_16_ct_side)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(match_date) ^
      const DeepCollectionEquality().hash(match_id) ^
      const DeepCollectionEquality().hash(map_stat_id) ^
      const DeepCollectionEquality().hash(is_favourite) ^
      const DeepCollectionEquality().hash(map_round) ^
      const DeepCollectionEquality().hash(mapstat_name) ^
      const DeepCollectionEquality().hash(team_id) ^
      const DeepCollectionEquality().hash(team_rank) ^
      const DeepCollectionEquality().hash(team_name) ^
      const DeepCollectionEquality().hash(team_win) ^
      const DeepCollectionEquality().hash(team_score) ^
      const DeepCollectionEquality().hash(team_round_history) ^
      const DeepCollectionEquality().hash(team_clutch_score) ^
      const DeepCollectionEquality().hash(team_pick) ^
      const DeepCollectionEquality().hash(team_1st_half_winner) ^
      const DeepCollectionEquality().hash(team_clutch_1st_half) ^
      const DeepCollectionEquality().hash(team_win_at_pistol_1) ^
      const DeepCollectionEquality().hash(team_win_at_pistol_16) ^
      const DeepCollectionEquality().hash(opponent_id) ^
      const DeepCollectionEquality().hash(opponent_rank) ^
      const DeepCollectionEquality().hash(opponent_name) ^
      const DeepCollectionEquality().hash(opponent_win) ^
      const DeepCollectionEquality().hash(opponent_score) ^
      const DeepCollectionEquality().hash(opponent_round_history) ^
      const DeepCollectionEquality().hash(opponent_clutch_score) ^
      const DeepCollectionEquality().hash(opponent_pick) ^
      const DeepCollectionEquality().hash(opponent_1st_half_winner) ^
      const DeepCollectionEquality().hash(opponent_clutch_1st_half) ^
      const DeepCollectionEquality().hash(opponent_win_at_pistol_1) ^
      const DeepCollectionEquality().hash(opponent_win_at_pistol_16) ^
      const DeepCollectionEquality().hash(team_round_1) ^
      const DeepCollectionEquality().hash(team_round_16) ^
      const DeepCollectionEquality().hash(opponent_round_1) ^
      const DeepCollectionEquality().hash(opponent_round_16) ^
      const DeepCollectionEquality().hash(team_round_1_ct_side) ^
      const DeepCollectionEquality().hash(team_round_16_ct_side);

  @JsonKey(ignore: true)
  @override
  _$GameDetailCopyWith<_GameDetail> get copyWith =>
      __$GameDetailCopyWithImpl<_GameDetail>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_GameDetailToJson(this);
  }
}

abstract class _GameDetail extends GameDetail {
  factory _GameDetail(
      {required DateTime match_date,
      required int match_id,
      required int map_stat_id,
      bool? is_favourite,
      required int map_round,
      required String mapstat_name,
      required int team_id,
      int? team_rank,
      required String team_name,
      required bool team_win,
      int? team_score,
      required String team_round_history,
      int? team_clutch_score,
      bool? team_pick,
      required bool team_1st_half_winner,
      int? team_clutch_1st_half,
      required bool team_win_at_pistol_1,
      required bool team_win_at_pistol_16,
      required int opponent_id,
      required int opponent_rank,
      required String opponent_name,
      required bool opponent_win,
      int? opponent_score,
      required String opponent_round_history,
      required int opponent_clutch_score,
      bool? opponent_pick,
      required bool opponent_1st_half_winner,
      int? opponent_clutch_1st_half,
      required bool opponent_win_at_pistol_1,
      required bool opponent_win_at_pistol_16,
      required int team_round_1,
      required int team_round_16,
      required int opponent_round_1,
      required int opponent_round_16,
      required bool team_round_1_ct_side,
      required bool team_round_16_ct_side}) = _$_GameDetail;
  _GameDetail._() : super._();

  factory _GameDetail.fromJson(Map<String, dynamic> json) =
      _$_GameDetail.fromJson;

  @override
  DateTime get match_date => throw _privateConstructorUsedError;
  @override
  int get match_id => throw _privateConstructorUsedError;
  @override
  int get map_stat_id => throw _privateConstructorUsedError;
  @override
  bool? get is_favourite => throw _privateConstructorUsedError;
  @override
  int get map_round => throw _privateConstructorUsedError;
  @override
  String get mapstat_name => throw _privateConstructorUsedError;
  @override
  int get team_id => throw _privateConstructorUsedError;
  @override
  int? get team_rank => throw _privateConstructorUsedError;
  @override
  String get team_name => throw _privateConstructorUsedError;
  @override
  bool get team_win => throw _privateConstructorUsedError;
  @override
  int? get team_score => throw _privateConstructorUsedError;
  @override
  String get team_round_history => throw _privateConstructorUsedError;
  @override
  int? get team_clutch_score => throw _privateConstructorUsedError;
  @override
  bool? get team_pick => throw _privateConstructorUsedError;
  @override
  bool get team_1st_half_winner => throw _privateConstructorUsedError;
  @override
  int? get team_clutch_1st_half => throw _privateConstructorUsedError;
  @override
  bool get team_win_at_pistol_1 => throw _privateConstructorUsedError;
  @override
  bool get team_win_at_pistol_16 => throw _privateConstructorUsedError;
  @override
  int get opponent_id => throw _privateConstructorUsedError;
  @override
  int get opponent_rank => throw _privateConstructorUsedError;
  @override
  String get opponent_name => throw _privateConstructorUsedError;
  @override
  bool get opponent_win => throw _privateConstructorUsedError;
  @override
  int? get opponent_score => throw _privateConstructorUsedError;
  @override
  String get opponent_round_history => throw _privateConstructorUsedError;
  @override
  int get opponent_clutch_score => throw _privateConstructorUsedError;
  @override
  bool? get opponent_pick => throw _privateConstructorUsedError;
  @override
  bool get opponent_1st_half_winner => throw _privateConstructorUsedError;
  @override
  int? get opponent_clutch_1st_half => throw _privateConstructorUsedError;
  @override
  bool get opponent_win_at_pistol_1 => throw _privateConstructorUsedError;
  @override
  bool get opponent_win_at_pistol_16 => throw _privateConstructorUsedError;
  @override
  int get team_round_1 => throw _privateConstructorUsedError;
  @override
  int get team_round_16 => throw _privateConstructorUsedError;
  @override
  int get opponent_round_1 => throw _privateConstructorUsedError;
  @override
  int get opponent_round_16 => throw _privateConstructorUsedError;
  @override
  bool get team_round_1_ct_side => throw _privateConstructorUsedError;
  @override
  bool get team_round_16_ct_side => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$GameDetailCopyWith<_GameDetail> get copyWith =>
      throw _privateConstructorUsedError;
}
