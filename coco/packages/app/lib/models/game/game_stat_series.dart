part of 'game.dart';

class GameStatSeries {
  final String team_name;
  final List<GameDetail>? details;
  late int? wins = 0;

  GameStatSeries({required this.team_name, this.details}) : super() {
    wins = details?.map((e) => e.team_win ? 1 : 0).sum;
  }
}
