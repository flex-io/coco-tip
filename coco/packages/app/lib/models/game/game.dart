import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:supercharged/supercharged.dart';

part 'game.freezed.dart';
part 'game.g.dart';
part 'game_stat_pistol.dart';
part 'game_stat.dart';
part 'game_detail.dart';
part 'game_stat_series.dart';

class GameResultGroup {
  final String name;
  final List<GameResult>? results;
  GameResultGroup({required this.name, this.results});
}

class GameResult {
  final int match_id;
  final DateTime match_date;
  final int team_id;
  final int opponent_id;
  final String team_name;
  final String opponent_name;

  final List<GameDetail>? details;
  late int team_final_score;
  late int opponent_final_score;
  late bool team_win;
  GameResult({
    required this.match_id,
    required this.match_date,
    required this.team_id,
    required this.team_name,
    required this.opponent_name,
    required this.opponent_id,
    this.details,
  }) : super() {
    team_final_score = details?.map((e) => e.team_score! > e.opponent_score! ? 1 : 0).sum ?? 0;
    opponent_final_score = details?.map((e) => e.team_score! < e.opponent_score! ? 1 : 0).sum ?? 0;
    team_win = team_final_score > opponent_final_score;
  }
}

@freezed
class Game with _$Game {
  Game._();

  factory Game({
    required int id,
    required DateTime match_date,
    required int team1_id,
    required String team1_name,
    required int team2_id,
    required String team2_name,
    String? bo,
    String? match_event,
    List<GameDetail>? team1_details,
    List<GameDetail>? team2_details,
    List<GameDetail>? h2h,
    List<GameDetail>? same_opponent,
  }) = _Game;

  // late final List<GameSeries> team1_series = team1_details
  //         ?.groupBy((element) => element.match_id, valueTransform: (item) => item)
  //         .map((key, value) =>
  //             )
  //     as List<GameSeries>;

  GameResultGroup get team1_result {
    late List<GameResult> data = [];
    team1_details?.groupBy((e) => e.match_id, valueTransform: (item) => item).forEach((key, value) {
      data.add(GameResult(
        match_id: key,
        match_date: value.map((e) => e.match_date).first,
        team_id: team1_id,
        team_name: team1_name,
        opponent_id: value.map((e) => e.opponent_id).first,
        opponent_name: value.map((e) => e.opponent_name).first,
        details: value,
      ));
    });
    return GameResultGroup(name: team1_name, results: data);
  }

  GameResultGroup get team2_result {
    late List<GameResult> data = [];
    team2_details?.groupBy((e) => e.match_id, valueTransform: (item) => item).forEach((key, value) {
      data.add(GameResult(
        match_id: key,
        match_date: value.map((e) => e.match_date).first,
        team_id: team2_id,
        team_name: team2_name,
        opponent_id: value.map((e) => e.opponent_id).first,
        opponent_name: value.map((e) => e.opponent_name).first,
        details: value,
      ));
    });
    return GameResultGroup(name: team2_name, results: data);
  }

  GameResultGroup get h2h_result {
    late List<GameResult> data = [];
    h2h?.groupBy((e) => e.match_id, valueTransform: (item) => item).forEach((key, value) {
      data.add(GameResult(
        match_id: key,
        match_date: value.map((e) => e.match_date).first,
        team_id: team2_id,
        team_name: team2_name,
        opponent_id: value.map((e) => e.opponent_id).first,
        opponent_name: value.map((e) => e.opponent_name).first,
        details: value,
      ));
    });
    return GameResultGroup(name: 'h2h', results: data);
  }

  late final List<String> maps = ((team1_details ?? [])..addAll(team2_details ?? []))
      .map((e) => e.mapstat_name)
      .toSet()
      .toList();

  factory Game.fromJson(Map<String, dynamic> json) => _$GameFromJson(json);
}
