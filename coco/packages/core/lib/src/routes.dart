class AppRoutes {
  static final home = '/';
  static final game = '/game';
  static final preview = '/preview';
  static final template = '/template';
}
