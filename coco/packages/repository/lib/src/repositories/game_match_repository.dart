import 'dart:async';

import '../services/services.dart';
import '../entities/entities.dart';
// import 'package:meta/meta.dart';
// import 'package:http/http.dart' as http;

class GameMatchRepository {
  final WebClient webClient;

  const GameMatchRepository({this.webClient = const WebClient()});

  Future<List<GameMatchEntity>> getGameMatches() async {
    return await webClient.fetchGameMatches();
  }
}
