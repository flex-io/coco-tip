import 'dart:async';

import '../services/services.dart';
import '../entities/entities.dart';
// import 'package:meta/meta.dart';
// import 'package:http/http.dart' as http;

class GameDetailRepository {
  final WebClient webClient;

  const GameDetailRepository({this.webClient = const WebClient()});

  Future<GameDetailEntity> getGameDetail(int matchId) async {
    return await webClient.fetchGameDetail(matchId);
  }
}
