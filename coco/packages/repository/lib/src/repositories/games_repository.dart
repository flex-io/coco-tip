import 'dart:async';

import '../services/services.dart';
import '../entities/entities.dart';

class GamesRepository {
  final WebClient webClient;

  const GamesRepository({this.webClient = const WebClient()});

  Future<List<GameEntity>> getGames() async {
    return await webClient.fetchGames();
  }
}
