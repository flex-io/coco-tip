import 'dart:convert';
import '../../repository.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:http/http.dart' as http;

class WebClient {
  static const baseUrl =
      'https://e5grmu5o47.execute-api.ap-southeast-2.amazonaws.com/prod';
  final Duration delay;

  const WebClient([this.delay = const Duration(milliseconds: 3000)]);

  Future<List<GameMatchEntity>> fetchGameMatches() async {
    final url = '$baseUrl/csgo/matches';
    final resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw Exception('Error getting matches');
    }
    final t = jsonDecode(resp.body)['matches'] as List;
    return t.map((e) => GameMatchEntity.fromJson(e)).toList();

    // final dynamic data = await rootBundle.loadString(
    //     'packages/coco/coco_app_repo/assets/data/game_matches.json');
    // final t = jsonDecode(data)['matches'] as List;
    // return t.map((e) => GameMatchEntity.fromJson(e)).toList();
  }

  Future<List<GameEntity>> fetchGames() async {
    final response = await http.get('$baseUrl/games');

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      final t = jsonDecode(response.body) as List;
      return t.map((e) => GameEntity.fromJson(e)).toList();
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
    // final dynamic data = await rootBundle
    //     .loadString('packages/coco/coco_app_repo/assets/data/games.json');
    // final t = jsonDecode(data) as List;
    // return t.map((e) => GameEntity.fromJson(e)).toList();
  }

  Future<GameDetailEntity> fetchGameDetail(int matchId) async {
    final response = await http.get('$baseUrl/csgo/match/$matchId');

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      // return GameDetailEntity.fromJson(jsonDecode(response.body));
      final dynamic data = jsonDecode(response.body);
      // final t = data['game_detail'];
      return GameDetailEntity.fromJson(data['game_detail']);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }

    // final dynamic data =
    //     await rootBundle.loadString('packages/repository/assets/2346134.json');
    // final t = jsonDecode(data)['game_detail'];
    // return GameDetailEntity.fromJson(t);
  }
}
