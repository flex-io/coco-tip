class GameMatchEntity {
  final int id;
  final String url;
  final DateTime matchDate;
  final int team1Id;
  final int team2Id;
  final String team1Name;
  final String team2Name;
  final String matchEvent;
  final String bo;
  final String s3Url;

  GameMatchEntity(
      {required this.id,
      required this.url,
      required this.matchDate,
      required this.team1Id,
      required this.team2Id,
      required this.team1Name,
      required this.team2Name,
      required this.matchEvent,
      required this.bo,
      required this.s3Url});

  factory GameMatchEntity.fromJson(dynamic json) {
    return GameMatchEntity(
      id: json['id'] as int,
      url: json['url'] as String,
      matchDate: DateTime.parse(json['match_date']),
      team1Id: json['team1_id'] as int,
      team2Id: json['team2_id'] as int,
      team1Name: json['team1_name'],
      team2Name: json['team2_name'],
      matchEvent: json['match_event'],
      bo: json['bo'] as String,
      s3Url: json['s3_url'] as String,
    );
  }
}
