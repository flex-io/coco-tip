import 'map_stat_entity.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:equatable/equatable.dart';

class TeamEntity {
  final int? id;
  final List<MapStatEntity>? mapStats;

  TeamEntity({this.id, this.mapStats});
}

class GameDetailEntity {
  final int id;
  final DateTime matchDate;
  final int? bo;
  final int team1Id;
  final int team2Id;
  final List<MapStatEntity>? mapStats;
  final List<Insight>? insights;

  GameDetailEntity(
      {required this.id,
      required this.matchDate,
      this.bo,
      required this.team1Id,
      required this.team2Id,
      this.mapStats,
      this.insights});

// t.map((e) => GameEntity.fromJson(e)).toList();
  factory GameDetailEntity.fromJson(dynamic json) {
    return GameDetailEntity(
        id: json['id'] as int,
        matchDate: DateTime.parse(json['match_date']),
        team1Id: json['team1_id'] as int,
        team2Id: json['team2_id'] as int,
        mapStats: ((json['map_stats'] as List))
            .map((e) => MapStatEntity.fromJson(e))
            .toList(),
        insights: (json["insights"] as List)
            .map((e) => Insight.fromJson(e))
            .toList());
  }
}

class Insight extends Equatable {
  final String groupName;
  final InsightTile header;
  final List<InsightTile> details;

  Insight(
      {required this.groupName, required this.header, required this.details});

  factory Insight.fromJson(dynamic json) {
    return Insight(
        groupName: json['group_name'],
        header: InsightTile.fromJson(json["header"]),
        details: (json["details"] as List)
            .map((e) => InsightTile.fromJson(e))
            .toList());
  }

  @override
  List<Object> get props => [groupName, header, details];
}

class InsightTile {
  String leading;
  String title;
  String trailing;

  InsightTile(
      {required this.leading, required this.title, required this.trailing});

  factory InsightTile.fromJson(dynamic json) {
    return InsightTile(
        leading: json["leading"],
        title: json["title"],
        trailing: json["trailing"]);
  }
}
