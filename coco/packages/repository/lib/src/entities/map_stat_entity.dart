class MapStatEntity {
  final int id;
  final DateTime matchDate;
  final int mapRound;
  final int mapStatId;
  final String mapName;
  final int matchId;
  final int teamId;
  final String teamName;
  final List<int> teamRoundHistory;
  final int teamRank;
  final int teamScore;
  final int opponentId;
  final String opponentName;
  final List<int> opponentRoundHistory;
  final int opponentRank;
  final int opponentScore;
  final bool pistol1;
  final bool pistol16;
  final bool isWin;
  final int teamFirstHalfScore;
  final int opponentFirstHalfscore;
  final bool isCounterSide;

  MapStatEntity({
    required this.id,
    required this.matchDate,
    required this.mapRound,
    required this.mapStatId,
    required this.mapName,
    required this.matchId,
    required this.teamId,
    required this.teamName,
    required this.teamRoundHistory,
    required this.teamRank,
    required this.teamScore,
    required this.opponentId,
    required this.opponentName,
    required this.opponentRoundHistory,
    required this.opponentRank,
    required this.opponentScore,
    required this.pistol1,
    required this.pistol16,
    required this.isWin,
    required this.teamFirstHalfScore,
    required this.opponentFirstHalfscore,
  }) : isCounterSide = teamFirstHalfScore == 0
            ? opponentFirstHalfscore > 0
                ? true
                : false
            : teamFirstHalfScore > 0
                ? true
                : false;

  factory MapStatEntity.fromJson(dynamic json) {
    return MapStatEntity(
      id: json['id'] as int,
      matchDate: DateTime.parse(json['match_date']),
      mapRound: json['map_round'] as int,
      mapStatId: json['map_stat_id'] as int,
      mapName: json['map_name'],
      matchId: json['match_id'] as int,
      teamId: json['team_id'] as int,
      teamName: json['team_name'],
      teamRoundHistory: json['team_round_history']
          .toString()
          .replaceAll('|', ',')
          .split(',')
          .map(int.parse)
          .toList(),
      teamRank: json['team_rank'] as int,
      teamScore: json['team_score'] as int,
      opponentId: json['opponent_id'] as int,
      opponentName: json['opponent_name'],
      opponentRoundHistory: json['opponent_round_history']
          .toString()
          .replaceAll('|', ',')
          .split(',')
          .map(int.parse)
          .toList(),
      opponentRank: json['opponent_rank'] as int,
      opponentScore: json['opponent_score'] as int,
      pistol1: json['pistol_1'] as bool,
      pistol16: json['pistol_16'] as bool,
      isWin: json['is_win'] as bool,
      teamFirstHalfScore: json['team_first_half_score'],
      opponentFirstHalfscore: json['opponent_first_half_score'],
    );
  }
}
