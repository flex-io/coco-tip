// ignore: import_of_legacy_library_into_null_safe
import 'package:timeago/timeago.dart' as timeago;

class GameEntity {
  final int id;
  final DateTime matchDate;
  final String team1Name;
  final String team2Name;
  final String title;
  final String subTitle;
  final bool isLive;
  final String league;
  final int team1Id;
  final int team2Id;
  final int leagueId;
  final int gameType;
  final String timeAgo;

  GameEntity(
      {required this.id,
      required this.matchDate,
      required this.team1Name,
      required this.team2Name,
      required this.title,
      required this.subTitle,
      required this.isLive,
      required this.league,
      required this.team1Id,
      required this.team2Id,
      required this.leagueId,
      required this.gameType})
      : timeAgo = timeago.format(
            new DateTime.now().add(new DateTime.now().difference(matchDate)),
            locale: 'en_short');

  factory GameEntity.fromJson(dynamic json) {
    return GameEntity(
        id: json['id'] as int,
        matchDate: DateTime.parse(json['match_date']),
        team1Name: json['team1_name'],
        team2Name: json['team2_name'],
        title: json['title'],
        subTitle: json['sub_title'],
        isLive: json['isLive'] as bool,
        league: json['league'],
        team1Id: json['team1_id'],
        team2Id: json['team2_id'],
        leagueId: json['league_id'],
        gameType: json['game_type']);
  }
}
